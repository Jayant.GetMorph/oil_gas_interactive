﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class ConnectionManager : MonoBehaviour
{
    public GameObject connectionInitiatePanel;
    public GameObject connectingPanel;
    public GameObject connectionSucceededPanel;
    public GameObject connectionFailedPanel;

    public TMP_InputField ipInputField;
    public TMP_InputField portNumberInputField;

    public static ConnectionManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void tryNetworkConnection()
    {
        connectionInitiatePanel.SetActive(false);
        connectingPanel.SetActive(true);
        connectionSucceededPanel.SetActive(false);
        connectionFailedPanel.SetActive(false);

        ServerHandler.Instance.defaultHost = ipInputField.text;
        int.TryParse( portNumberInputField.text,out ServerHandler.Instance.defaultTcpPort);

        Debug.Log("User Entered IP : " + ServerHandler.Instance.defaultHost);
        Debug.Log("User Entered Port number : " + ServerHandler.Instance.defaultTcpPort);

        ServerHandler.Instance.ConnectToServer();
    }

    public void connectionResponse(bool networkResponse)
    {
        if(networkResponse)
        {
            connectionInitiatePanel.SetActive(false);
            connectingPanel.SetActive(false);
            connectionSucceededPanel.SetActive(true);
            connectionFailedPanel.SetActive(false);
            Debug.Log("Connection failed");
        }
        else
        {
            connectionInitiatePanel.SetActive(false);
            connectingPanel.SetActive(false);
            connectionSucceededPanel.SetActive(false);
            connectionFailedPanel.SetActive(true);
            Debug.Log("Connection succeeded");
        }
    }
    public void retryNetworkConnection()
    {
        connectionInitiatePanel.SetActive(true);
        connectingPanel.SetActive(false);
        connectionSucceededPanel.SetActive(false);
        connectionFailedPanel.SetActive(false);
    }
}
