﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ProblemFoundMechanics : MonoBehaviour
{
    public GameObject problemDetectedIndividualPanel;
    public GameObject problemDetectedPanel;
    public GameObject problemFixedPanel;
    public GameObject issueFixedPanel;

    void Start()
    {
        GetComponentInParent<Canvas>().worldCamera = Camera.main;
    }
    public void enableProblemDetectedPanel(bool network)
    {
        Manager.Instance.previousDemoState = Manager.DEMO_STATES.PROBLEM_DETECTED;
        Manager.Instance.currentDemoState = Manager.DEMO_STATES.MAINTAINENCE_BUTTON_HIGHLIGHTED;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(Manager.DEMO_STATES.MAINTAINENCE_BUTTON_HIGHLIGHTED));
        }

        problemDetectedIndividualPanel.SetActive(false);
        problemDetectedPanel.SetActive(true);
        problemFixedPanel.SetActive(false);
        issueFixedPanel.SetActive(false);
        Manager.Instance.highlightMaintainence();
    }

    public void enableProblemFixedPanel()
    {
        Manager.Instance.previousDemoState = Manager.DEMO_STATES.ENGINEER_DISPLAY_PANEL_AND_REPAIRING;
        Manager.Instance.currentDemoState = Manager.DEMO_STATES.PROBLEM_FIXED_PANEL;

        problemDetectedIndividualPanel.SetActive(false);
        problemDetectedPanel.SetActive(false);
        problemFixedPanel.SetActive(true);
        issueFixedPanel.SetActive(false);
    }

    public void enableIssueFixedPanel(bool network)
    {
        Manager.Instance.previousDemoState = Manager.DEMO_STATES.PROBLEM_FIXED_PANEL;
        Manager.Instance.currentDemoState = Manager.DEMO_STATES.ISSUE_FIXED_PANEL;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(Manager.DEMO_STATES.ISSUE_FIXED_PANEL));
        }

        problemDetectedIndividualPanel.SetActive(false);
        problemDetectedPanel.SetActive(false);
        problemFixedPanel.SetActive(false);
        issueFixedPanel.SetActive(true);
        Manager.Instance.engineerDisplayPanel.SetActive(false);
    }

    public void disableProblemFoundDashboard(bool network)
    {
        Manager.Instance.previousDemoState = Manager.DEMO_STATES.ISSUE_FIXED_PANEL;
        Manager.Instance.currentDemoState = Manager.DEMO_STATES.FINISH_DEMO;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(Manager.DEMO_STATES.FINISH_DEMO));
        }

        gameObject.SetActive(false);
        Camera.main.gameObject.GetComponent<CameraMovement>().moveCameraToOriginalPosition();
        Camera.main.transform.GetChild(0).gameObject.SetActive(false);

         foreach (GameObject oilRig in Manager.Instance.selectedOilRigsList)
        {
            if (oilRig.GetComponent<OilRigProperties>().currentOilRigState == OilRigProperties.OIL_RIG_STATE.RED)
            {
                oilRig.GetComponent<OilRigProperties>().tickAnimationCheck(oilRig.GetComponent<OilRigProperties>().RTG);
                oilRig.GetComponent<OilRigProperties>().currentOilRigState = OilRigProperties.OIL_RIG_STATE.GREEN;
            }
        }
    }
}