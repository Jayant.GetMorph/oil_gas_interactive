﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LongPress : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool isDown;
    public float downTime;

    public void OnPointerDown(PointerEventData eventData)
    {
        isDown = true;
        downTime = Time.realtimeSinceStartup;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isDown = false;
    }

    void Update()
    {
        if (!isDown) return;

        if (Time.realtimeSinceStartup - this.downTime > 0.5f)
        {
            print("Handle Long Tap");
            isDown = false;
            if (!Manager.Instance.areaSelection)
            {
                Manager.Instance.oilRigSelectionButtonsPanel.SetActive(true);
                this.downTime=0f;

                Manager.Instance.previousDemoState = Manager.DEMO_STATES.OIL_RIG_FIELD_SELECTED;
                Manager.Instance.currentDemoState = Manager.DEMO_STATES.OIL_RIG_SELECTION_PANEL;

                ServerHandler.Instance.SendData(0, 0, nameof(Manager.DEMO_STATES.OIL_RIG_SELECTION_PANEL));
            }
            else
            {
                Manager.Instance.previousDemoState = Manager.DEMO_STATES.LEVEL_1_ZOOM_IN;
                Manager.Instance.currentDemoState = Manager.DEMO_STATES.ZOOM_SELECTION_PANEL;
                Manager.Instance.oilRigSelectionZoomPanel.SetActive(true);

                ServerHandler.Instance.SendData(0, 0, nameof(Manager.DEMO_STATES.ZOOM_SELECTION_PANEL));
            }
        }

        if(Manager.Instance.zoomedIn)
        {
            GetComponent<Image>().raycastTarget = false;
        }
        else
        {
            GetComponent<Image>().raycastTarget = true;
        }
    }

}
