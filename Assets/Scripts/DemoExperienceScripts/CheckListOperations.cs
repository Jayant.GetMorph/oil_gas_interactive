﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CheckListOperations : MonoBehaviour
{
    TextMeshProUGUI textMeshProUGUI;
    Image toggleImage;
    public Sprite highlightImage;
    public Sprite deHighlightImage;
    public int index; 

    Color32 deHighlightColor = new Color32(166,166,166,255);
    Color32 HighlightColor = new Color32(255,255,255,255);

    void Awake()
    {
        textMeshProUGUI = GetComponent<TextMeshProUGUI>();
        toggleImage = GetComponentInChildren<Image>();
    }

    public void highlightCategory()
    {
        toggleImage.sprite = highlightImage;
        textMeshProUGUI.color = HighlightColor; 
    }

    public void deHighlightCategory()
    {
        toggleImage.sprite = deHighlightImage;
        textMeshProUGUI.color = deHighlightColor; 
    }
}
