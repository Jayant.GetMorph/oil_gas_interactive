﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using TMPro;

public class Manager : MonoBehaviour
{
    public static Manager Instance;

    [Header("Public Buttons References")]
    public GameObject viewDemoButton;
    public GameObject viewDemoClickAnimationObject;
    public GameObject workOrderHighlightImage;
    public GameObject workOrderSelectedImage;
    public Button workOrderButton;
    public GameObject inventoryHighlightImage;
    public GameObject inventorySelectedImage;
    public Button inventoryButton;
    public GameObject maintainenceHighlightImage;
    public GameObject maintainenceSelectedImage;
    public Button maintainenceButton;
    public GameObject operatorSelectionPanel;
    public Button operatorSelectionSubmitButton;
    public GameObject operatorsDisplayPanel;
    public GameObject operatorsDisplaySubmitButton;
    public GameObject engineerSelectionPanel;
    public GameObject engineerDisplayPanel;
    public Image chemicalTotesrefillBarImage;
    public GameObject chemicalTotesLevelImage;
    public GameObject chemicalTotesHealthReportPanel;
    public Image repairRefillBarImage;

    [Header("Public Text References")]
    public GameObject workOrderNotifyText;
    public GameObject inventoryNotifyText;
    public GameObject maintainenceNotifyText;

    [Header("Public Query Text References")]
    public TextMeshProUGUI query1;
    public TextMeshProUGUI query2;
    public TextMeshProUGUI query3;
    public TextMeshProUGUI query4;

    [Header("Public Screen References")]
    public GameObject connectionManagerScreen;
    public GameObject introVideoScreen;
    public GameObject viewDemoScreen;
    public GameObject oilFieldDashboardScreen;
    public GameObject oilRigFieldSelectionCanvas;
    public GameObject oilRigFieldSelectionScreen;
    public GameObject oilRigFieldLongPressSelectionScreen;
    public GameObject oilRigSelectionButtonsPanel;
    public GameObject oilRigSelectionZoomPanel;
    public GameObject inventoryDashboard;
    public GameObject orderPlacedDashboard;
    public GameObject petrolSearchDashboard;
    public GameObject operatorRouteMapDashboard;
    public GameObject pumpJackPerformanceDashboard;
    public GameObject oilFieldHealthStatusDashboard;

    [Header("Operator References")]
    public OperatorIconProperties operatorIcon1;
    public OperatorIconProperties operatorIcon2;
    public Button operator1AvailableButton;
    public Button operator2AvailableButton;
    public Button engineer1AvailableButton;
    public Button engineerSelectButton;

    public enum DEMO_STATES
    {
        INTRO_VIDEO,
        VIEW_DEMO,
        OIL_RIG_FIELD_SELECTION,
        OIL_RIG_FIELD_SELECTED,
        OIL_RIG_SELECTION_PANEL,
        OIL_RIG_SELECTION_START,
        WORK_ORDER_HIGHLIGHTED,
        OPERATOR_SELECTION_DISPLAY_PANEL,
        OPERATOR_DISPLAY_PANEL,
        WORK_ORDER_REFILLING,
        CHEMICAL_TOTES_HEALTH_REPORT,
        INVENTORY_BUTTON_HIGHLIGHTED,
        INVENTORY_DASHBOARD,
        ORDER_PLACED,
        REMAINING_CRITICAL_OIL_RIG_HIGHLIGHTED,
        LEVEL_1_ZOOM_IN,
        ZOOM_SELECTION_PANEL,
        LEVEL_2_ZOOM_IN,
        LEVEL_2_ZOOM_OUT,
        PROBLEM_DETECTED,
        MAINTAINENCE_BUTTON_HIGHLIGHTED,
        ENGINEER_SELECTION_DISPLAY_PANEL,
        ENGINEER_DISPLAY_PANEL_AND_REPAIRING,
        PROBLEM_FIXED_PANEL,
        ISSUE_FIXED_PANEL,
        FINISH_DEMO
    }

    public DEMO_STATES previousDemoState;
    public DEMO_STATES currentDemoState;
    public bool selectionProcess;
    public float operatorSelectionCount;
    public int selectedQueryIndex;
    public bool areaSelection;
    public bool zoomedIn;
    public Vector3 areaSelectionVector;
    public GameObject selectedRig;
    public GameObject zoomLongPressSelector;
    public GameObject oilRigZoomButtonsPanel;
    public int selectedLongPressScreenIndex;
    public float refillDuration;
    public float refillAmount;

    public List<GameObject> selectedOilRigsList;
    public List<GameObject> oilRigsField1List;
    public List<GameObject> oilRigsField2List;
    public List<GameObject> oilRigsField3List;

    [Header("NavMesh References")]
    public PathHandler redAgent;
    public PathHandler orangeAgent;

    [Header("CheckList References")]
    public List<GameObject> checkListGameObjects;

    private void Awake()
    {
        Instance = this;
        
        connectionManagerScreen.SetActive(true);
        introVideoScreen.SetActive(false);
        viewDemoScreen.SetActive(false);
        oilFieldDashboardScreen.SetActive(false);
        oilRigFieldSelectionCanvas.SetActive(false);
        oilRigFieldSelectionScreen.SetActive(false);
        oilRigFieldLongPressSelectionScreen.SetActive(false);
        oilRigSelectionButtonsPanel.SetActive(false);
        oilRigSelectionZoomPanel.SetActive(false);
        inventoryDashboard.SetActive(false);
        orderPlacedDashboard.SetActive(false);
        petrolSearchDashboard.SetActive(false);
        operatorRouteMapDashboard.SetActive(false);
        pumpJackPerformanceDashboard.SetActive(false);
        oilFieldHealthStatusDashboard.SetActive(false);
    }

    private void OnEnable()
    {
        PathHandler.OnOilRigRefilled += fillOilRig;
        ServerHandler.OnServerResponse += server;
    }

    private void OnDisable()
    {
        PathHandler.OnOilRigRefilled -= fillOilRig;
        ServerHandler.OnServerResponse -= server;
    }

    private void server(int groupId, int id, string name)
    {
        //Debug.Log("INT : " + id);
        //Debug.Log("STRING : " + name);
        //Debug.Log("STRING : " + DEMO_STATES.INTRO_VIDEO.ToString());

        switch (name)
        {
            case nameof(DEMO_STATES.VIEW_DEMO):
                {
                    if (currentDemoState == DEMO_STATES.INTRO_VIDEO)
                    {
                        viewDemo(false);
                    }
                        break;
                }
            case nameof(DEMO_STATES.OIL_RIG_FIELD_SELECTION):
                {
                    if (currentDemoState == DEMO_STATES.VIEW_DEMO)
                    {
                        initiateFieldSelection(false);
                    }
                    break;
                }
            case nameof(DEMO_STATES.OIL_RIG_FIELD_SELECTED):
                {
                    if (currentDemoState == DEMO_STATES.OIL_RIG_FIELD_SELECTION)
                    {
                        switch (id)
                        {
                            case 1:
                                {
                                    CameraMovement.Instance.MoveCameraToOilRig1();
                                    assignLongPressIndex(id, false);
                                    break;
                                }
                            case 2:
                                {
                                    CameraMovement.Instance.MoveCameraToOilRig2();
                                    assignLongPressIndex(id, false);
                                    break;
                                }
                            case 3:
                                {
                                    CameraMovement.Instance.MoveCameraToOilRig3();
                                    assignLongPressIndex(id, false);
                                    break;
                                }
                        }
                    }
                    break;
                }
            case nameof(DEMO_STATES.OIL_RIG_SELECTION_PANEL):
                {
                    if (currentDemoState == DEMO_STATES.OIL_RIG_FIELD_SELECTED)
                    {
                        oilRigSelectionButtonsPanel.SetActive(true);

                        previousDemoState = DEMO_STATES.OIL_RIG_FIELD_SELECTED;
                        currentDemoState = DEMO_STATES.OIL_RIG_SELECTION_PANEL;
                    }
                    break;
                }
            case nameof(DEMO_STATES.OIL_RIG_SELECTION_START):
                {
                    if (currentDemoState == DEMO_STATES.OIL_RIG_SELECTION_PANEL)
                    {
                        if (id == 0)
                        {
                            startOilRigSelection(false);
                        }
                        else if (id == 9)
                        {
                            disableOilRigSelectionButtonsPanel(false);
                        }
                        else
                        {
                            selectRigFromNetwork(groupId, id);
                        }
                    }
                    break;
                }
            case nameof(DEMO_STATES.WORK_ORDER_HIGHLIGHTED):
                {
                    if (currentDemoState == DEMO_STATES.OIL_RIG_SELECTION_START)
                    {
                        endSelectionOilRigSelectionButtonsPanel(false);
                    }
                    break;
                }
            case nameof(DEMO_STATES.OPERATOR_SELECTION_DISPLAY_PANEL):
                {
                    if (currentDemoState == DEMO_STATES.WORK_ORDER_HIGHLIGHTED)
                    {
                        switch (groupId)
                        {
                            case 0:
                                {
                                    startWorkOrderProcess(false);
                                    break;
                                }
                            case 1:
                                {
                                    switch (id)
                                    {
                                        case 1:
                                            {
                                                operator1AvailableButton.GetComponent<OperatorAssigningButtonProperties>().sendServerData(false);
                                                break;
                                            }
                                        case 2:
                                            {
                                                operator2AvailableButton.GetComponent<OperatorAssigningButtonProperties>().sendServerData(false);
                                                break;
                                            }
                                    }
                                    break;
                                }
                        }
                    }
                    break;
                }
            case nameof(DEMO_STATES.OPERATOR_DISPLAY_PANEL):
                {
                    if (currentDemoState == DEMO_STATES.OPERATOR_SELECTION_DISPLAY_PANEL)
                    {
                        assignOilRigsToOperators(false);
                    }
                    break;
                }
            case nameof(DEMO_STATES.WORK_ORDER_REFILLING):
                {
                    if (currentDemoState == DEMO_STATES.OPERATOR_DISPLAY_PANEL)
                    {
                        startRefilling(false);
                    }
                    break;
                }
            case nameof(DEMO_STATES.INVENTORY_BUTTON_HIGHLIGHTED):
                {
                    if (currentDemoState == DEMO_STATES.CHEMICAL_TOTES_HEALTH_REPORT)
                    {
                        finishWorkOrderProcess(false);
                    }
                    break;
                }
            case nameof(DEMO_STATES.INVENTORY_DASHBOARD):
                {
                    if (currentDemoState == DEMO_STATES.INVENTORY_BUTTON_HIGHLIGHTED)
                    {
                        startInventoryProcess(false);
                    }
                    break;
                }
            case nameof(DEMO_STATES.ORDER_PLACED):
                {
                    if (currentDemoState == DEMO_STATES.INVENTORY_DASHBOARD)
                    {
                        generateInventoryOrder(false);
                    }
                        break;
                }
            case nameof(DEMO_STATES.REMAINING_CRITICAL_OIL_RIG_HIGHLIGHTED):
                {
                    if (currentDemoState == DEMO_STATES.ORDER_PLACED)
                    {
                        finishInventoryProcess(false);
                    }
                    break;
                }
            case nameof(DEMO_STATES.LEVEL_1_ZOOM_IN):
                {
                    if (currentDemoState == DEMO_STATES.REMAINING_CRITICAL_OIL_RIG_HIGHLIGHTED)
                    {
                        selectFaultyRigFromNetwork(groupId, id);
                    }
                    break;
                }
            case nameof(DEMO_STATES.ZOOM_SELECTION_PANEL):
                {
                    if (currentDemoState == DEMO_STATES.LEVEL_1_ZOOM_IN)
                    {
                        if (id == 0)
                        {
                            oilRigSelectionZoomPanel.SetActive(true);
                        }
                        else if (id == 9)
                        {
                            disableOilRigZoomButtonsPanel(false);
                        }
                    }
                    break;
                }
            case nameof(DEMO_STATES.PROBLEM_DETECTED):
                {
                    if (currentDemoState == DEMO_STATES.LEVEL_2_ZOOM_OUT)
                    {
                        CameraMovement.Instance.zoomInOnRig(false);
                    }
                    break;
                }
            case nameof(DEMO_STATES.LEVEL_2_ZOOM_OUT):
                {
                    if (currentDemoState == DEMO_STATES.LEVEL_2_ZOOM_IN)
                    {
                        CameraMovement.Instance.zoomOutOnRig(false);
                    }
                    break;
                }
            case nameof(DEMO_STATES.MAINTAINENCE_BUTTON_HIGHLIGHTED):
                {
                    if (currentDemoState == DEMO_STATES.PROBLEM_DETECTED)
                    {
                        selectedRig.GetComponent<OilRigProperties>().problemDetectedDashboard.GetComponent<ProblemFoundMechanics>().enableProblemDetectedPanel(false);
                    }
                        break;
                }
            case nameof(DEMO_STATES.ENGINEER_SELECTION_DISPLAY_PANEL):
                {
                    if (currentDemoState == DEMO_STATES.MAINTAINENCE_BUTTON_HIGHLIGHTED)
                    {
                        if (groupId == 1)
                        {
                            startMaintainenceProcess(false);
                        }
                        else
                        {
                            engineer1AvailableButton.GetComponent<OperatorAssigningButtonProperties>().sendServerDataForEngineer(false);
                        }
                    }
                    break;
                }
            case nameof(DEMO_STATES.ENGINEER_DISPLAY_PANEL_AND_REPAIRING):
                {
                    if (currentDemoState == DEMO_STATES.ENGINEER_SELECTION_DISPLAY_PANEL)
                    {
                        assignEngineerForProblem(false);
                    }
                    break;
                }
            case nameof(DEMO_STATES.ISSUE_FIXED_PANEL):
                {
                    if (currentDemoState == DEMO_STATES.ENGINEER_DISPLAY_PANEL_AND_REPAIRING)
                    {
                        selectedRig.GetComponent<OilRigProperties>().problemDetectedDashboard.GetComponent<ProblemFoundMechanics>().enableIssueFixedPanel(false);
                    }
                        break;
                }
            case nameof(DEMO_STATES.FINISH_DEMO):
                {
                    if (currentDemoState == DEMO_STATES.ISSUE_FIXED_PANEL)
                    {
                        selectedRig.GetComponent<OilRigProperties>().problemDetectedDashboard.GetComponent<ProblemFoundMechanics>().disableProblemFoundDashboard(false);
                    }
                        break;
                }
        }
    }

    public void initiateFieldSelection()
    {
        initiateFieldSelection(true);
    }

    public void initiateFieldSelection(bool network)
    {
        previousDemoState = DEMO_STATES.VIEW_DEMO;
        currentDemoState = DEMO_STATES.OIL_RIG_FIELD_SELECTION;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(DEMO_STATES.OIL_RIG_FIELD_SELECTION));
        }
        oilRigFieldSelectionScreen.SetActive(true);
        StartCoroutine(initiateOilRigFieldSelection());
    }

    public void assignLongPressIndex(int index)
    {
        assignLongPressIndex(index, true);
    }

    public void assignLongPressIndex(int index, bool network)
    {
        previousDemoState = DEMO_STATES.OIL_RIG_FIELD_SELECTION;
        currentDemoState = DEMO_STATES.OIL_RIG_FIELD_SELECTED;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, index, nameof(DEMO_STATES.OIL_RIG_FIELD_SELECTED));
        }
        selectedLongPressScreenIndex = index;
    }

    public void disableOilRigSelectionButtonsPanel(bool network)
    {
        previousDemoState = DEMO_STATES.OIL_RIG_SELECTION_START;
        currentDemoState = DEMO_STATES.OIL_RIG_FIELD_SELECTED;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 9, nameof(DEMO_STATES.OIL_RIG_SELECTION_START));
        }
        oilRigSelectionButtonsPanel.SetActive(false);
    }

    public void disableOilRigZoomButtonsPanel(bool network)
    {
        previousDemoState = DEMO_STATES.ZOOM_SELECTION_PANEL;
        currentDemoState = DEMO_STATES.LEVEL_1_ZOOM_IN;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 9, nameof(DEMO_STATES.LEVEL_1_ZOOM_IN));
        }
        oilRigZoomButtonsPanel.SetActive(false);
    }



    public void fillOilRig(GameObject closestTarget, bool isLastTarget)
    {
        refillTheBar();

        if (!isLastTarget)
            tickTransition(closestTarget);
    }

    public void toggleChecklist(int index)
    {
        foreach (GameObject checkListGameObject in checkListGameObjects)
        {
            if (checkListGameObject.GetComponent<CheckListOperations>().index == index)
            {
                checkListGameObject.GetComponent<CheckListOperations>().highlightCategory();
            }
            else
            {
                checkListGameObject.GetComponent<CheckListOperations>().deHighlightCategory();
            }
        }
    }

    public void startIntroVideo()
    {
        previousDemoState = DEMO_STATES.INTRO_VIDEO;
        currentDemoState = DEMO_STATES.INTRO_VIDEO;
        
        connectionManagerScreen.SetActive(false);
        introVideoScreen.SetActive(true);
    }

    public void viewDemo(bool network)
    {
        previousDemoState = DEMO_STATES.INTRO_VIDEO;
        currentDemoState = DEMO_STATES.VIEW_DEMO;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(DEMO_STATES.VIEW_DEMO));
        }

        introVideoScreen.SetActive(false);
        viewDemoScreen.SetActive(true);
    }



    public IEnumerator initiateOilRigFieldSelection()
    {
        yield return new WaitForSeconds(0.2f);
        viewDemoButton.SetActive(false);
        viewDemoClickAnimationObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        viewDemoClickAnimationObject.SetActive(false);
        viewDemoScreen.SetActive(false);
        oilRigFieldSelectionCanvas.SetActive(true);

        toggleChecklist(1);
    }

    public void selectOilRigField()
    {
        oilRigFieldSelectionScreen.SetActive(false);
        oilRigFieldLongPressSelectionScreen.SetActive(true);
        oilFieldDashboardScreen.SetActive(true);
    }

    public void endSelectionOilRigSelectionButtonsPanel(bool network)
    {
        previousDemoState = DEMO_STATES.OIL_RIG_SELECTION_START;
        currentDemoState = DEMO_STATES.WORK_ORDER_HIGHLIGHTED;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(DEMO_STATES.WORK_ORDER_HIGHLIGHTED));
        }

        oilRigFieldLongPressSelectionScreen.SetActive(false);
        if (selectedOilRigsList.Count >= 1)
        {
            oilRigSelectionButtonsPanel.SetActive(false);
            oilRigFieldSelectionCanvas.SetActive(false);
            detectOilRigsSelection();

            workOrderButton.interactable = true;
            inventoryButton.interactable = false;
            maintainenceButton.interactable = false;
            workOrderHighlightImage.SetActive(true);
            selectionProcess = false;
        }
    }
    public void enableOilRigZoomPanel()
    {
        // oilRigSelectionZoomPanel.SetActive(true);
        zoomLongPressSelector.SetActive(true);

        //  previousDemoState = DEMO_STATES.OIL_RIG_IDLE;
        //  currentDemoState = DEMO_STATES.OIL_RIG_SELECTION_STATE;
    }
    public void startOilRigSelection()
    {
        startOilRigSelection(true);
    }


    public void startOilRigSelection(bool network)
    {
        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(DEMO_STATES.OIL_RIG_SELECTION_START));
        }

        previousDemoState = DEMO_STATES.OIL_RIG_SELECTION_PANEL;
        currentDemoState = DEMO_STATES.OIL_RIG_SELECTION_START;

        switch (selectedLongPressScreenIndex)
        {
            case 1:
                {
                    foreach (GameObject oilRig in oilRigsField1List)
                    {
                        oilRig.GetComponent<OilRigProperties>().startRigSelectionProcess();
                    }
                    break;
                }
            case 2:
                {
                    foreach (GameObject oilRig in oilRigsField2List)
                    {
                        oilRig.GetComponent<OilRigProperties>().startRigSelectionProcess();
                    }
                    break;
                }
            case 3:
                {
                    foreach (GameObject oilRig in oilRigsField3List)
                    {
                        oilRig.GetComponent<OilRigProperties>().startRigSelectionProcess();
                    }
                    break;
                }
        }
    }

    public void selectRigFromNetwork(int oilRigFieldIndex, int oilRigIndex)
    {
        switch (oilRigFieldIndex)
        {
            case 1:
                {
                    foreach (GameObject oilRig in oilRigsField1List)
                    {
                        if (oilRig.GetComponent<OilRigProperties>().oilRigIndex == oilRigIndex)
                        {
                            oilRigProcesses(oilRig);
                        }
                    }
                    break;
                }
            case 2:
                {
                    foreach (GameObject oilRig in oilRigsField2List)
                    {
                        if (oilRig.GetComponent<OilRigProperties>().oilRigIndex == oilRigIndex)
                        {
                            oilRigProcesses(oilRig);
                        }
                    }
                    break;
                }
            case 3:
                {
                    foreach (GameObject oilRig in oilRigsField3List)
                    {
                        if (oilRig.GetComponent<OilRigProperties>().oilRigIndex == oilRigIndex)
                        {
                            oilRigProcesses(oilRig);
                        }
                    }
                    break;
                }
        }
    }

    public void selectFaultyRigFromNetwork(int oilRigFieldIndex, int oilRigIndex)
    {
        switch (oilRigFieldIndex)
        {
            case 1:
                {
                    foreach (GameObject oilRig in oilRigsField1List)
                    {
                        if (oilRig.GetComponent<OilRigProperties>().oilRigIndex == oilRigIndex)
                        {
                            Camera.main.GetComponent<CameraMovement>().moveToSelectedRigArea(oilRig.transform);
                        }
                    }
                    break;
                }
            case 2:
                {
                    foreach (GameObject oilRig in oilRigsField2List)
                    {
                        if (oilRig.GetComponent<OilRigProperties>().oilRigIndex == oilRigIndex)
                        {
                            Camera.main.GetComponent<CameraMovement>().moveToSelectedRigArea(oilRig.transform);
                        }
                    }
                    break;
                }
            case 3:
                {
                    foreach (GameObject oilRig in oilRigsField3List)
                    {
                        if (oilRig.GetComponent<OilRigProperties>().oilRigIndex == oilRigIndex)
                        {
                            Camera.main.GetComponent<CameraMovement>().moveToSelectedRigArea(oilRig.transform);
                        }
                    }
                    break;
                }
        }
    }

    void oilRigProcesses(GameObject oilRig)
    {
        oilRig.GetComponent<OilRigProperties>().selectRig();
        oilRig.GetComponent<OilRigProperties>().selectableWaveHUDAnimator.gameObject.SetActive(true);
        oilRig.GetComponent<OilRigProperties>().selectableWaveHUDAnimator.SetBool("SELECTABLE", true);
    }

    public void endOilRigsSelection()
    {
        foreach (GameObject oilRig in oilRigsField1List)
        {
            oilRig.GetComponent<OilRigProperties>().endRigSelectionProcess();
        }

        foreach (GameObject oilRig in oilRigsField2List)
        {
            oilRig.GetComponent<OilRigProperties>().endRigSelectionProcess();
        }

        foreach (GameObject oilRig in oilRigsField3List)
        {
            oilRig.GetComponent<OilRigProperties>().endRigSelectionProcess();
        }

    }

    public void detectOilRigsSelection()
    {
        switch (selectedLongPressScreenIndex)
        {
            case 1:
                {
                    foreach (GameObject oilRig in oilRigsField1List)
                    {
                        if (!selectedOilRigsList.Contains(oilRig))
                        {
                            oilRig.SetActive(false);
                        }
                    }
                    break;
                }
            case 2:
                {
                    foreach (GameObject oilRig in oilRigsField2List)
                    {
                        if (!selectedOilRigsList.Contains(oilRig))
                        {
                            oilRig.SetActive(false);
                        }
                    }
                    break;
                }
            case 3:
                {
                    foreach (GameObject oilRig in oilRigsField3List)
                    {
                        if (!selectedOilRigsList.Contains(oilRig))
                        {
                            oilRig.SetActive(false);
                        }
                    }
                    break;
                }
        }
    }

    public void startWorkOrderProcess(bool network)
    {
        previousDemoState = DEMO_STATES.WORK_ORDER_HIGHLIGHTED;
        currentDemoState = DEMO_STATES.OPERATOR_SELECTION_DISPLAY_PANEL;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(DEMO_STATES.OPERATOR_SELECTION_DISPLAY_PANEL));
        }

        workOrderButton.interactable = false;
        inventoryButton.interactable = false;
        maintainenceButton.interactable = false;

        workOrderHighlightImage.SetActive(false);
        workOrderSelectedImage.SetActive(true);

        setAllNotifyTextsState(false);
        operatorSelectionPanel.SetActive(true);
    }

    public void finishWorkOrderProcess(bool network)
    {
        previousDemoState = DEMO_STATES.CHEMICAL_TOTES_HEALTH_REPORT;
        currentDemoState = DEMO_STATES.INVENTORY_BUTTON_HIGHLIGHTED;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(DEMO_STATES.INVENTORY_BUTTON_HIGHLIGHTED));
        }

        workOrderButton.interactable = false;
        inventoryButton.interactable = true;
        maintainenceButton.interactable = false;

        workOrderHighlightImage.SetActive(false);
        workOrderSelectedImage.SetActive(false);
        chemicalTotesHealthReportPanel.SetActive(false);

        inventoryHighlightImage.SetActive(true);

        setAllNotifyTextsState(true);
    }

    public void startInventoryProcess(bool network)
    {
        previousDemoState = DEMO_STATES.INVENTORY_BUTTON_HIGHLIGHTED;
        currentDemoState = DEMO_STATES.INVENTORY_DASHBOARD;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(DEMO_STATES.INVENTORY_DASHBOARD));
        }

        workOrderButton.interactable = false;
        inventoryButton.interactable = false;
        maintainenceButton.interactable = false;

        inventoryHighlightImage.SetActive(false);
        inventorySelectedImage.SetActive(true);

        setAllNotifyTextsState(false);
        inventoryDashboard.SetActive(true);
        toggleChecklist(5);
    }

    public void generateInventoryOrder(bool network)
    {
        previousDemoState = DEMO_STATES.INVENTORY_DASHBOARD;
        currentDemoState = DEMO_STATES.ORDER_PLACED;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(DEMO_STATES.ORDER_PLACED));
        }

        inventoryDashboard.SetActive(false);
        orderPlacedDashboard.SetActive(true);
        toggleChecklist(6);
    }

    public void finishInventoryProcess(bool network)
    {

        previousDemoState = DEMO_STATES.ORDER_PLACED;
        currentDemoState = DEMO_STATES.REMAINING_CRITICAL_OIL_RIG_HIGHLIGHTED;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(DEMO_STATES.REMAINING_CRITICAL_OIL_RIG_HIGHLIGHTED));
        }

        workOrderButton.interactable = false;
        inventoryButton.interactable = false;
        maintainenceButton.interactable = false;
        inventoryHighlightImage.SetActive(false);
        inventorySelectedImage.SetActive(false);

        orderPlacedDashboard.SetActive(false);
        // petrolSearchHighlightImage.SetActive(true);
        highlightRemainingOilRig();
    }

    public void highlightMaintainence()
    {
        maintainenceHighlightImage.SetActive(true);

        workOrderButton.interactable = false;
        inventoryButton.interactable = false;
        maintainenceButton.interactable = true;
    }

    public void startMaintainenceProcess(bool network)
    {
        previousDemoState = DEMO_STATES.MAINTAINENCE_BUTTON_HIGHLIGHTED;
        currentDemoState = DEMO_STATES.ENGINEER_SELECTION_DISPLAY_PANEL;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(1, 0, nameof(DEMO_STATES.ENGINEER_SELECTION_DISPLAY_PANEL));
        }

        workOrderButton.interactable = false;
        inventoryButton.interactable = false;
        maintainenceButton.interactable = false;

        maintainenceHighlightImage.SetActive(false);
        maintainenceSelectedImage.SetActive(true);

        setAllNotifyTextsState(false);
        engineerSelectionPanel.SetActive(true);
        toggleChecklist(9);
    }

    public void petrolSearchDashboardToggle()
    {
        if (petrolSearchDashboard.activeInHierarchy)
        {
            petrolSearchDashboard.SetActive(false);
        }
        else
        {
            petrolSearchDashboard.SetActive(true);
            toggleChecklist(7);
        }
    }

    void setAllNotifyTextsState(bool state)
    {
        workOrderNotifyText.SetActive(state);
        inventoryNotifyText.SetActive(state);
        maintainenceNotifyText.SetActive(state);
    }

    public void increaseOperatorSelectionCount(Button selectedButton)
    {
        selectedButton.interactable = false;
        operatorSelectionCount++;
        if (operatorSelectionCount >= 2)
        {
            operatorSelectionSubmitButton.interactable = true;
            operatorSelectionCount = 0;
        }
    }

    public void assignOilRigsToOperators(bool network)
    {
        previousDemoState = DEMO_STATES.OPERATOR_SELECTION_DISPLAY_PANEL;
        currentDemoState = DEMO_STATES.OPERATOR_DISPLAY_PANEL;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(DEMO_STATES.OPERATOR_DISPLAY_PANEL));
        }

        operatorSelectionPanel.SetActive(false);
        operatorsDisplayPanel.SetActive(true);
        toggleChecklist(2);

        foreach (GameObject oilRig in selectedOilRigsList)
        {
            if (oilRig.GetComponent<OilRigProperties>().currentOilRigState == OilRigProperties.OIL_RIG_STATE.ORANGE)
            {
                operatorIcon1.oilRigsList.Add(oilRig);
            }
            else if (oilRig.GetComponent<OilRigProperties>().currentOilRigState == OilRigProperties.OIL_RIG_STATE.RED)
            {
                operatorIcon2.oilRigsList.Add(oilRig);
            }
        }
    }
    public void assignEngineerForProblem(bool network)
    {
        previousDemoState = DEMO_STATES.ENGINEER_SELECTION_DISPLAY_PANEL;
        currentDemoState = DEMO_STATES.ENGINEER_DISPLAY_PANEL_AND_REPAIRING;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(DEMO_STATES.ENGINEER_DISPLAY_PANEL_AND_REPAIRING));
        }

        engineerSelectionPanel.SetActive(false);
        engineerDisplayPanel.SetActive(true);
        repairRefillBarImage.DOFillAmount(1, 1).OnComplete(fixTheLastRig);
    }

    public void fixTheLastRig()
    {
        selectedRig.GetComponent<OilRigProperties>().problemDetectedDashboard.GetComponent<ProblemFoundMechanics>().enableProblemFixedPanel();
        selectedRig.GetComponent<OilRigProperties>().tickPlaceHolder.SetActive(true);
        selectedRig.GetComponent<OilRigProperties>().tickAnimationCheck(selectedRig.GetComponent<OilRigProperties>().RTG);
    }

    public void startRefilling(bool network)
    {
        previousDemoState = DEMO_STATES.OPERATOR_DISPLAY_PANEL;
        currentDemoState = DEMO_STATES.WORK_ORDER_REFILLING;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(DEMO_STATES.WORK_ORDER_REFILLING));
        }

        // This function start refilling which will be done by a car
        redAgent.StartRefill(operatorIcon2.oilRigsList, selectedLongPressScreenIndex - 1);
        orangeAgent.StartRefill(operatorIcon1.oilRigsList, selectedLongPressScreenIndex - 1);

        // refillDuration += 1;

        refillAmount = 1 / refillDuration;

        operatorsDisplaySubmitButton.SetActive(false);
        chemicalTotesrefillBarImage.transform.parent.gameObject.SetActive(true);
        toggleChecklist(3);
    }

    public void refillTheBar()
    {
        // operatorsDisplaySubmitButton.SetActive(false);
        // chemicalTotesrefillBarImage.transform.parent.gameObject.SetActive(true);
        // StartCoroutine(changeTicks());
        chemicalTotesrefillBarImage.fillAmount += refillAmount;
        // chemicalTotesrefillBarImage.DOFillAmount(1, refillDuration).OnComplete(showChemicalTotesHealthReport);
        // chemicalTotesLevelImage.GetComponent<RectTransform>().DOAnchorPosY(110, refillDuration);
    }

    public void tickTransition(GameObject oilRig)
    {
        oilRig.GetComponent<OilRigProperties>().tickAnimator.enabled = true;
        if (oilRig.GetComponent<OilRigProperties>().currentOilRigState == OilRigProperties.OIL_RIG_STATE.ORANGE)
        {
            oilRig.GetComponent<OilRigProperties>().tickAnimationCheck(oilRig.GetComponent<OilRigProperties>().OTG);
        }
        else if (oilRig.GetComponent<OilRigProperties>().currentOilRigState == OilRigProperties.OIL_RIG_STATE.RED)
        {
            oilRig.GetComponent<OilRigProperties>().tickAnimationCheck(oilRig.GetComponent<OilRigProperties>().RTG);
        }

        oilRig.GetComponent<OilRigProperties>().currentOilRigState = OilRigProperties.OIL_RIG_STATE.GREEN;
        oilRig.GetComponent<OilRigProperties>().selectableWaveHUDAnimator.gameObject.SetActive(false);
    }
    public void showChemicalTotesHealthReport()
    {
        previousDemoState = DEMO_STATES.WORK_ORDER_REFILLING;
        currentDemoState = DEMO_STATES.CHEMICAL_TOTES_HEALTH_REPORT;

        chemicalTotesHealthReportPanel.SetActive(true);
        operatorsDisplayPanel.SetActive(false);
        toggleChecklist(4);
    }

    public IEnumerator changeTicks()
    {
        for (int i = 0; i < operatorIcon1.oilRigsList.Count; i++)
        {
            operatorIcon1.oilRigsList[i].GetComponent<OilRigProperties>().tickAnimator.enabled = true;
            if (operatorIcon1.oilRigsList[i].GetComponent<OilRigProperties>().currentOilRigState == OilRigProperties.OIL_RIG_STATE.ORANGE)
            {
                operatorIcon1.oilRigsList[i].GetComponent<OilRigProperties>().tickAnimationCheck(operatorIcon1.oilRigsList[i].GetComponent<OilRigProperties>().OTG);
            }
            else
            {
                operatorIcon1.oilRigsList[i].GetComponent<OilRigProperties>().tickAnimationCheck(operatorIcon1.oilRigsList[i].GetComponent<OilRigProperties>().RTG);
            }

            operatorIcon1.oilRigsList[i].GetComponent<OilRigProperties>().currentOilRigState = OilRigProperties.OIL_RIG_STATE.GREEN;
            yield return new WaitForSeconds(1f);
        }

        for (int i = 0; i < operatorIcon2.oilRigsList.Count - 1; i++)
        {
            operatorIcon2.oilRigsList[i].GetComponent<OilRigProperties>().tickAnimator.enabled = true;
            if (operatorIcon2.oilRigsList[i].GetComponent<OilRigProperties>().currentOilRigState == OilRigProperties.OIL_RIG_STATE.ORANGE)
            {
                operatorIcon2.oilRigsList[i].GetComponent<OilRigProperties>().tickAnimationCheck(operatorIcon2.oilRigsList[i].GetComponent<OilRigProperties>().OTG);
                operatorIcon2.oilRigsList[i].GetComponent<OilRigProperties>().currentOilRigState = OilRigProperties.OIL_RIG_STATE.GREEN;
            }
            else
            {
                operatorIcon2.oilRigsList[i].GetComponent<OilRigProperties>().tickAnimationCheck(operatorIcon2.oilRigsList[i].GetComponent<OilRigProperties>().RTG);
                operatorIcon2.oilRigsList[i].GetComponent<OilRigProperties>().currentOilRigState = OilRigProperties.OIL_RIG_STATE.GREEN;
            }

            yield return new WaitForSeconds(1f);
        }
    }

    public void highlightRemainingOilRig()
    {
        foreach (GameObject oilRig in selectedOilRigsList)
        {
            if (oilRig.GetComponent<OilRigProperties>().currentOilRigState == OilRigProperties.OIL_RIG_STATE.RED)
            {
                oilRig.tag = "FaultyOilRig";
                oilRig.GetComponent<OilRigProperties>().selectableWaveHUDAnimator.gameObject.SetActive(false);
                oilRig.GetComponent<OilRigProperties>().selectableWaveHUDAnimator.gameObject.SetActive(true);
                oilRig.GetComponent<OilRigProperties>().selectableWaveHUDAnimator.SetBool("SELECTABLE", true);
                oilRig.GetComponent<SphereCollider>().enabled = true;
            }
        }

        areaSelection = true;
    }

    public void playIntroVideo()
    {
        Debug.Log("Playing intro video");
    }

    public void refreshDemo()
    {
        Debug.Log("Refreshed Demo");
    }

    public void startPetrolSearch()
    {
        Debug.Log("Starting Petrol Search");
    }

    public void querySelector(int index)
    {
        selectedQueryIndex = index;
        switch (selectedQueryIndex)
        {
            case 1:
                {
                    query1.color = new Color32(255, 255, 255, 255);
                    query2.color = new Color32(166, 166, 166, 255);
                    query3.color = new Color32(166, 166, 166, 255);
                    query4.color = new Color32(166, 166, 166, 255);
                    break;
                }
            case 2:
                {
                    query1.color = new Color32(166, 166, 166, 255);
                    query2.color = new Color32(255, 255, 255, 255);
                    query3.color = new Color32(166, 166, 166, 255);
                    query4.color = new Color32(166, 166, 166, 255);
                    break;
                }
            case 3:
                {
                    query1.color = new Color32(166, 166, 166, 255);
                    query2.color = new Color32(166, 166, 166, 255);
                    query3.color = new Color32(255, 255, 255, 255);
                    query4.color = new Color32(166, 166, 166, 255);
                    break;
                }
            case 4:
                {
                    query1.color = new Color32(166, 166, 166, 255);
                    query2.color = new Color32(166, 166, 166, 255);
                    query3.color = new Color32(166, 166, 166, 255);
                    query4.color = new Color32(255, 255, 255, 255);
                    break;
                }
        }
    }

    public void selectedQueryAction()
    {
        Debug.Log("Query Selected Index : " + selectedQueryIndex);
        switch (selectedQueryIndex)
        {
            case 1:
                {
                    operatorRouteMapDashboard.SetActive(true);
                    pumpJackPerformanceDashboard.SetActive(false);
                    oilFieldHealthStatusDashboard.SetActive(false);
                    inventoryDashboard.SetActive(false);

                    petrolSearchDashboardToggle();
                    break;
                }
            case 2:
                {
                    operatorRouteMapDashboard.SetActive(false);
                    pumpJackPerformanceDashboard.SetActive(true);
                    oilFieldHealthStatusDashboard.SetActive(false);
                    inventoryDashboard.SetActive(false);

                    petrolSearchDashboardToggle();
                    break;
                }
            case 3:
                {
                    operatorRouteMapDashboard.SetActive(false);
                    pumpJackPerformanceDashboard.SetActive(false);
                    oilFieldHealthStatusDashboard.SetActive(true);
                    inventoryDashboard.SetActive(false);

                    petrolSearchDashboardToggle();
                    break;
                }
            case 4:
                {
                    operatorRouteMapDashboard.SetActive(false);
                    pumpJackPerformanceDashboard.SetActive(false);
                    oilFieldHealthStatusDashboard.SetActive(false);

                    petrolSearchDashboardToggle();
                    startInventoryProcess(true);
                    break;
                }
        }

    }
}
