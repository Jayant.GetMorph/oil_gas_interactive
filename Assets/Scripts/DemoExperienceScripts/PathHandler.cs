﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class PathHandler : MonoBehaviour
{
    public enum AgentType { Critical, NeedsAttention }

    NavMeshAgent myNav;

    public List<GameObject> targetList;
    private bool isTargetSet;
    public List<GameObject> exitTarget;
    private bool isExiting;

    public GameObject vehicle;
    public bool isDebugModeOn;

    private Transform placeHolders;

    public AgentType agentType;

    private MSVehicleController vehicleController;
    GameObject closestTargetObject = null;

    [Tooltip("0-OilField1, 1-OilField2, 2-OilField3")]
    public Transform[] placeHolderList;

    public static Action<GameObject, bool> OnOilRigRefilled;
    public static Action OnRefillOver;

    public Color trailColor;

    // Start is called before the first frame update
    void Start()
    {
        if (isDebugModeOn)
        {
            placeHolders = placeHolderList[0];
            StartCoroutine(SetUp());
        }
    }

    private void Update()
    {
        if (isTargetSet)
        {
            if (myNav.remainingDistance < 2f)
            {
                if (HasCarReachedTarget())
                {
                    if (isExiting)
                    {
                        //Debug.Log("Completed");
                        //vehicleWhellCollider.SetActive(false);

                        vehicleController.handBrakeTrue = true;
                        isTargetSet = false;
                        //OnRefillOver?.Invoke();
                        // vehicle.SetActive(false);

                        //dgtween fade out trail renderer (OnComplete callback)
                        //renderer.colo
                        //renderer.material.DOFade(0, 5f).OnComplete(OnTrailFaded);

                        StartCoroutine(FadeTrail());
                        return;
                    }

                    vehicleController.handBrakeTrue = true;
                    isTargetSet = false;
                    OnOilRigRefilled?.Invoke(closestTargetObject.transform.parent.gameObject, agentType == AgentType.Critical? targetList.Count==0:false);
                    Invoke("CalculatePath", 1f);
                    // myNav.speed = myNav.speed * 0.5f;
                }

            }
        }
    }

    private IEnumerator FadeTrail() {
        TrailRenderer renderer = vehicle.GetComponentInChildren<TrailRenderer>();
        Color color = renderer.material.GetColor("_TintColor");
        while (color.a > 0) {
            yield return null;
            color.a -= Time.deltaTime;
            renderer.material.SetColor("_TintColor", color);
        }
        OnTrailFaded();
    }

    private void OnTrailFaded()
    {
        vehicle.SetActive(false);
        if (gameObject.tag == "AgentOrange")
        {
            Manager.Instance.chemicalTotesrefillBarImage.fillAmount = 1;
            Manager.Instance.showChemicalTotesHealthReport();
        }
        myNav.isStopped = true;
        OnRefillOver?.Invoke();
    }

    /// <summary>
    /// Sets up the vehicle and agent
    /// </summary>
    private IEnumerator SetUp()
    {

        myNav = GetComponent<NavMeshAgent>();
        List<GameObject> targets = new List<GameObject>();
        foreach (GameObject target in targetList)
        {
            target.GetComponent<SphereCollider>().enabled = false;
            targets.Add(target.GetComponent<OilRigProperties>().navMeshTarget);
        }

        targetList.Sort(delegate (GameObject g1, GameObject g2)
        {
            OilRigProperties oilRig1 = g1.GetComponent<OilRigProperties>();
            OilRigProperties oilRig2 = g2.GetComponent<OilRigProperties>();
            if (oilRig1.targetId < oilRig2.targetId)
                return -1;
            else
                return 1;
        });

        if (agentType != AgentType.Critical)
            yield return new WaitForSeconds(5f);

        vehicle.SetActive(true);
        TrailRenderer renderer = vehicle.GetComponentInChildren<TrailRenderer>();
        renderer.material.SetColor("_TintColor", trailColor);
        vehicleController = vehicle.GetComponentInParent<MSVehicleController>();
        vehicleController.handBrakeTrue = true;

        Transform positionTransform = GetPositionHolderTransform(targetList[0].GetComponent<OilRigProperties>().targetId);
        transform.SetPositionAndRotation(positionTransform.GetChild(0).position, positionTransform.GetChild(0).rotation);
        vehicleController.transform.SetPositionAndRotation(positionTransform.GetChild(1).position, positionTransform.GetChild(1).rotation);

        targetList = targets;
        // myNav.speed = myNav.speed * 2f;

        CalculatePath();
    }

    /// <summary>
    /// Path Length
    /// </summary>
    /// <param name="target">Target Position</param>
    /// <returns>Path Length</returns>
    private float PathLength(Vector3 target)
    {
        NavMeshPath path = new NavMeshPath();
        myNav.CalculatePath(target, path);
        int i = 1;
        float currentPathLength = 0;
        Vector3 lastCorner;
        Vector3 currentCorner;

        lastCorner = path.corners[0];
        while (i < path.corners.Length)
        {
            currentCorner = path.corners[i];
            currentPathLength += Vector3.Distance(lastCorner, currentCorner);
            Debug.DrawLine(lastCorner, currentCorner, Color.red);
            lastCorner = currentCorner;
            i++;
        }

        return currentPathLength;
    }

    /// <summary>
    /// Calculate and setup the target for the agent
    /// </summary>
    private void CalculatePath()
    {
        if (targetList.Count == 0)
        {
            LoadExitData();
            targetList = exitTarget;
            SetClosestTarget();
            myNav.SetDestination(closestTargetObject.transform.position);
            isExiting = true;
            isTargetSet = true;
            vehicleController.handBrakeTrue = false;
            // myNav.speed = myNav.speed * 2f;

            return;
        }

        SetClosestTarget();

        myNav.SetDestination(closestTargetObject.transform.position);
        targetList.Remove(closestTargetObject);
        isTargetSet = true;
        vehicleController.handBrakeTrue = false;

    }

    /// <summary>
    /// Sets the Closet Target
    /// </summary>
    private void SetClosestTarget()
    {
        Vector3 closestTarget = Vector3.zero;
        float closestTargetLength = Mathf.Infinity;
        float lastPathLength = Mathf.Infinity;
        float currentPathLength = 0;

        foreach (GameObject player in targetList)
        {

            currentPathLength = PathLength(player.transform.position);

            if (currentPathLength < closestTargetLength)
            {
                closestTarget = player.transform.position;
                closestTargetLength = currentPathLength;
                closestTargetObject = player;
            }
            lastPathLength = currentPathLength;
        }
    }

    /// <summary>
    /// HasCarReachedTarget
    /// </summary>
    /// <returns>True if the car has reached the target, False otherwise</returns>
    private bool HasCarReachedTarget()
    {
        // Debug.Log(agentType + " - "+Vector3.Distance(vehicleController.transform.position, closestTargetObject.transform.position));
        return Vector3.Distance(vehicleController.transform.position, closestTargetObject.transform.position) < 2f;
    }

    /// <summary>
    /// Starts the refilling process
    /// </summary>
    /// <param name="targets">List of OilRigs to refill</param>
    public void StartRefill(List<GameObject> targets, int fieldId = 1)
    {
        if (targets.Count == 0)
            return;
        targetList = targets;
        placeHolders = placeHolderList[fieldId];
        //filter the target if required
        StartCoroutine(SetUp());
    }

    /// <summary>
    /// Returns the Position Holder Transform
    /// </summary>
    /// <param name="id">start point id</param>
    /// <returns>Place Holder Transform</returns>
    private Transform GetPositionHolderTransform(int id)
    {
        try
        {
            return placeHolders.GetChild(id);
        }
        catch (Exception ex)
        {
            return placeHolders.GetChild(0);
        }
    }

    /// <summary>
    /// Loads the exit Data(Vehicle will go to the nearest exit point)
    /// </summary>
    private void LoadExitData()
    {
        exitTarget = new List<GameObject>();
        foreach (Transform t in placeHolders)
        {
            exitTarget.Add(t.GetChild(0).gameObject);
        }
    }

}
