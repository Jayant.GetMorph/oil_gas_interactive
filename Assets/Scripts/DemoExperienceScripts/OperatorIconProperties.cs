﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OperatorIconProperties : MonoBehaviour
{
    public TextMeshProUGUI assignedOilRigsCount;
    public List<GameObject> oilRigsList;

    private void FixedUpdate()
    {
        assignedOilRigsCount.text = oilRigsList.Count.ToString();
    }
}
