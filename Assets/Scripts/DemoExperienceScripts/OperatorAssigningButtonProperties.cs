﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class OperatorAssigningButtonProperties : MonoBehaviour
{
    public TextMeshProUGUI availableText;
    public OperatorAssigningIconProperties operatorObject;
    public int index;

    public void sendServerData(bool network)
    {
        if (network)
        {
            if(index ==1)
            {
                ServerHandler.Instance.SendData(1, 1, nameof(Manager.DEMO_STATES.OPERATOR_SELECTION_DISPLAY_PANEL));
            }
            else
            {
                ServerHandler.Instance.SendData(1, 2, nameof(Manager.DEMO_STATES.OPERATOR_SELECTION_DISPLAY_PANEL));
            }
        }
        availableText.text = "ASSIGNED";
        availableText.color = Color.black;

        operatorObject.changeAssetsAfterSelection();
        Manager.Instance.increaseOperatorSelectionCount(GetComponent<Button>());
    }

    public void sendServerDataForEngineer(bool network)
    {
        if(network)
        {
            availableText.text = "ASSIGNED";
            availableText.color = Color.black;
            Manager.Instance.engineerSelectButton.interactable = true;
        }
    }
}
