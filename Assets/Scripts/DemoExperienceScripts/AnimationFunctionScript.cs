﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationFunctionScript : MonoBehaviour
{
    public void callPlayIntroVideo()
    {
        
        Manager.Instance.playIntroVideo();
        GetComponent<Animator>().SetBool("INIT", false);
    }

    public void callRefreshDemo()
    {
        Manager.Instance.refreshDemo();
        GetComponent<Animator>().SetBool("INIT", false);
    }

    public void callPetrolSearchFunction()
    {
        Manager.Instance.petrolSearchDashboardToggle();
        GetComponent<Animator>().SetBool("INIT", false);
    }

    public void initPlayIntroVideoAnimation()
    {
        GetComponent<Animator>().SetBool("INIT", true);
    }

    public void initRefreshDemoAnimation()
    {
        GetComponent<Animator>().SetBool("INIT", true);
    }

    public void initAnimation()
    {
        GetComponent<Animator>().SetBool("INIT", true);
    }

}
