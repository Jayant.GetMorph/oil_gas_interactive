﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public static EventManager Instance;
    public delegate IEnumerator WiproOil();
    public static event WiproOil viewDemoEvent;

    private void Awake()
    {
        Instance = this;
    }

    public void callViewDemoEvent()
    {
        StartCoroutine(viewDemo());
    }

    public IEnumerator viewDemo()
    {
        StartCoroutine(viewDemoEvent());
        yield return null;
    }
}
