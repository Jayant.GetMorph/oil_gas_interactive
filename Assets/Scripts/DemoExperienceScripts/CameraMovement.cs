﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraMovement : MonoBehaviour
{
    public Transform cameraBasePosition;
    public Transform oilRig1CameraPosition;
    public Transform oilRig2CameraPosition;
    public Transform oilRig3CameraPosition;
    public GameObject longPressSelection1Image;
    public GameObject longPressSelection2Image;
    public GameObject longPressSelection3Image;
    public int selectedOilRigFieldIndex;
    public static CameraMovement Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        transform.position = cameraBasePosition.position;
        transform.localEulerAngles = cameraBasePosition.localEulerAngles;
        transform.localScale = cameraBasePosition.localScale;
    }
    
    public void moveCameraToOriginalPosition()
    {
        transform.DOMove(cameraBasePosition.position, 2f).SetEase(Ease.OutSine);
        enableAllListObjects(Manager.Instance.oilRigsField1List);
        enableAllListObjects(Manager.Instance.oilRigsField2List);
        enableAllListObjects(Manager.Instance.oilRigsField3List);
        Manager.Instance.endOilRigsSelection();
    }

    public void MoveCameraToOilRig1()
    {
        transform.DOMove(oilRig1CameraPosition.position, 2f).SetEase(Ease.OutSine);
        Manager.Instance.selectOilRigField();

        selectedOilRigFieldIndex = 1;
        disableOilRigFields();
    }

    public void MoveCameraToOilRig2()
    {
        transform.DOMove(oilRig2CameraPosition.position, 2f).SetEase(Ease.OutSine);
        Manager.Instance.selectOilRigField();

        selectedOilRigFieldIndex = 2;
        disableOilRigFields();
    }

    public void MoveCameraToOilRig3()
    {
        transform.DOMove(oilRig3CameraPosition.position, 2f).SetEase(Ease.OutSine);
        Manager.Instance.selectOilRigField();

        selectedOilRigFieldIndex = 3;
        disableOilRigFields();
    }

    public void moveToSelectedRigArea(Transform selectedRig)
    {
        Manager.Instance.selectedRig = selectedRig.gameObject;
        Vector3 thresholdVector = new Vector3(55f, 35f, -60f);
        Manager.Instance.areaSelectionVector = selectedRig.position + thresholdVector;
        transform.DOMove((Manager.Instance.areaSelectionVector), 2f).SetEase(Ease.OutSine);
        Manager.Instance.enableOilRigZoomPanel();
    }

    public void zoomInOnRig(bool network)
    {
        Manager.Instance.previousDemoState = Manager.DEMO_STATES.ZOOM_SELECTION_PANEL;
        Manager.Instance.currentDemoState = Manager.DEMO_STATES.PROBLEM_DETECTED;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(Manager.DEMO_STATES.PROBLEM_DETECTED));
        }

        Vector3 thresholdVector = new Vector3(20f, 14f, -33f);
        transform.DOMove((Manager.Instance.areaSelectionVector - thresholdVector), 2f).SetEase(Ease.OutSine);
        Manager.Instance.selectedRig.GetComponent<OilRigProperties>().problemDetectedDashboard.SetActive(true);
        Manager.Instance.zoomedIn = true;
        Manager.Instance.toggleChecklist(8);
    }

    public void zoomOutOnRig(bool network)
    {
        Manager.Instance.previousDemoState = Manager.DEMO_STATES.PROBLEM_DETECTED;
        Manager.Instance.currentDemoState = Manager.DEMO_STATES.LEVEL_2_ZOOM_OUT;

        if (network)
        {
            // Server Call
            ServerHandler.Instance.SendData(0, 0, nameof(Manager.DEMO_STATES.LEVEL_2_ZOOM_OUT));
        }

        transform.DOMove((Manager.Instance.areaSelectionVector), 5).SetEase(Ease.OutSine);
        Manager.Instance.selectedRig.GetComponent<OilRigProperties>().problemDetectedDashboard.SetActive(false);
        Manager.Instance.zoomedIn = false;
        Manager.Instance.toggleChecklist(7);
    }

    public void disableOilRigFields()
    {
        switch (selectedOilRigFieldIndex)
        {
            case 1:
                {
                    longPressSelection1Image.SetActive(true);
                    longPressSelection2Image.SetActive(false);
                    longPressSelection3Image.SetActive(false);
                    disableAllListObjects(Manager.Instance.oilRigsField2List);
                    disableAllListObjects(Manager.Instance.oilRigsField3List);
                    break;
                }
            case 2:
                {
                    longPressSelection1Image.SetActive(false);
                    longPressSelection2Image.SetActive(true);
                    longPressSelection3Image.SetActive(false);
                    disableAllListObjects(Manager.Instance.oilRigsField1List);
                    disableAllListObjects(Manager.Instance.oilRigsField3List);
                    break;
                }
            case 3:
                {
                    longPressSelection1Image.SetActive(false);
                    longPressSelection2Image.SetActive(false);
                    longPressSelection3Image.SetActive(true);
                    disableAllListObjects(Manager.Instance.oilRigsField1List);
                    disableAllListObjects(Manager.Instance.oilRigsField2List);
                    break;
                }
        }
    }

    public void disableAllListObjects(List<GameObject> oilRigField)
    {
        foreach (GameObject oilRig in oilRigField)
        {
            oilRig.SetActive(false);
        }
    }

    public void enableAllListObjects(List<GameObject> oilRigField)
    {
        foreach (GameObject oilRig in oilRigField)
        {
            oilRig.SetActive(true);
        }
    }
}
