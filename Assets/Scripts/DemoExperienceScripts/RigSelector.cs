﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigSelector : MonoBehaviour
{
    const string oilRigTag = "OilRig";
    const string faultyOilRigTag = "FaultyOilRig";

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform != null)
                {
                    if (hit.transform.tag == oilRigTag)
                    {
                        if (Manager.Instance.selectionProcess)
                        {
                            hit.transform.GetComponent<OilRigProperties>().selectRig();
                            hit.transform.GetComponent<OilRigProperties>().selectableWaveHUDAnimator.gameObject.SetActive(true);
                            hit.transform.GetComponent<OilRigProperties>().selectableWaveHUDAnimator.SetBool("SELECTABLE", true);

                            sendData(hit.transform);
                        }
                    }
                    else if (hit.transform.tag == faultyOilRigTag)
                    {
                        if (Manager.Instance.areaSelection && !Manager.Instance.zoomedIn)
                        {
                            GetComponent<CameraMovement>().moveToSelectedRigArea(hit.transform);

                            sendFaultyRigData(hit.transform);
                        }
                    }
                }
            }
        }
    }

    public void sendData(Transform oilRig)
    {
        ServerHandler.Instance.SendData(oilRig.GetComponent<OilRigProperties>().oilRigFieldIndex, oilRig.GetComponent<OilRigProperties>().oilRigIndex, nameof(Manager.DEMO_STATES.OIL_RIG_SELECTION_START));
    }
    public void sendFaultyRigData(Transform oilRig)
    {
        ServerHandler.Instance.SendData(oilRig.GetComponent<OilRigProperties>().oilRigFieldIndex, oilRig.GetComponent<OilRigProperties>().oilRigIndex, nameof(Manager.DEMO_STATES.LEVEL_1_ZOOM_IN));
    }

}

