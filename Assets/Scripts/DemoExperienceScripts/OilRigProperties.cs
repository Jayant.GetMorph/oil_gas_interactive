﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class OilRigProperties : MonoBehaviour
{
    public enum OIL_RIG_STATE
    {
        GREEN,
        ORANGE,
        RED
    }

    public OIL_RIG_STATE currentOilRigState;
    public OIL_RIG_STATE previousOilRigState;

    [Header("Tick Image References")]
    public Sprite redTick;
    public Sprite orangeTick;
    public Sprite greenTick;
    public GameObject tickPlaceHolder;

    [Header("Animators References")]
    public Animator greenCircleHUDAnimator;
    public Animator orangeCircleHUDAnimator;
    public Animator redCircleHUDAnimator;
    public Animator circleHUDAnimatorPlacHolder;
    public Animator tickAnimator;
    public Animator selectableWaveHUDAnimator;

    [Header("Public References")]
    public bool selected;
    public GameObject problemDetectedDashboard;

    public string SELECTABLE = "SELECTABLE";
    public string INIT = "INIT";
    public string LOOP = "LOOP";
    public string OTG = "OTG";
    public string RTG = "RTG";

    [Header("NavMesh References")]
    public GameObject navMeshTarget;
    [Tooltip("(0-bottomleft, 1-bottomright, 2-topleft, 3-topright, 4-inside)")]
    [Range(0, 4)]
    public int targetId;    //used to setup start position for the vehicle 

    [Header("Oil Rig Indexes")]
    public int oilRigFieldIndex;
    public int oilRigIndex;

    void Start()
    {
        ConstraintSource source = new ConstraintSource();
        source.sourceTransform = Camera.main.transform;
        source.weight = 1f;

        tickPlaceHolder.GetComponent<LookAtConstraint>().AddSource(source);
        problemDetectedDashboard.GetComponent<LookAtConstraint>().AddSource(source);
        problemDetectedDashboard.SetActive(false);
        circleHUDCheck();
    }

    public void startRigSelectionProcess()
    {
        Manager.Instance.selectionProcess = true;
        tickSpriteCheck();
        // circleHUDCheck();
        // selectableWaveHUDAnimator.gameObject.SetActive(true);
        // selectableWaveHUDAnimator.SetBool(SELECTABLE, true);
    }

    public void endRigSelectionProcess()
    {
        Manager.Instance.selectionProcess = false;
        tickPlaceHolder.SetActive(false);
        selectableWaveHUDAnimator.SetBool(SELECTABLE, false);
        greenCircleHUDAnimator.gameObject.SetActive(true);
        redCircleHUDAnimator.gameObject.SetActive(true);
        orangeCircleHUDAnimator.gameObject.SetActive(true);
    }

    void tickSpriteCheck()
    {
        Debug.Log("currentOilRigState : " + currentOilRigState);
        tickPlaceHolder.gameObject.SetActive(true);
        switch (currentOilRigState)
        {
            case OIL_RIG_STATE.GREEN:
                {
                    tickPlaceHolder.GetComponent<SpriteRenderer>().sprite = greenTick;
                    break;
                }
            case OIL_RIG_STATE.ORANGE:
                {
                    tickPlaceHolder.GetComponent<SpriteRenderer>().sprite = orangeTick;
                    break;
                }

            case OIL_RIG_STATE.RED:
                {
                    tickPlaceHolder.GetComponent<SpriteRenderer>().sprite = redTick;
                    break;
                }
        }

        Debug.Log("tickPlaceHolder.GetComponent<SpriteRenderer>().sprite : " + tickPlaceHolder.GetComponent<SpriteRenderer>().sprite);
        //Invoke("enableTickAnimator", 1f);
    }

    void enableTickAnimator()
    {
        tickPlaceHolder.gameObject.GetComponent<Animator>().enabled = true;
    }

    void circleHUDCheck()
    {
        switch (currentOilRigState)
        {
            case OIL_RIG_STATE.GREEN:
                {
                    greenCircleHUDAnimator.gameObject.SetActive(true);
                    circleHUDAnimatorPlacHolder = greenCircleHUDAnimator;
                    break;
                }
            case OIL_RIG_STATE.ORANGE:
                {
                    orangeCircleHUDAnimator.gameObject.SetActive(true);
                    circleHUDAnimatorPlacHolder = orangeCircleHUDAnimator;
                    break;
                }
            case OIL_RIG_STATE.RED:
                {
                    redCircleHUDAnimator.gameObject.SetActive(true);
                    circleHUDAnimatorPlacHolder = redCircleHUDAnimator;
                    break;
                }
        }
        circleHUDAnimationBegin();
    }

    public void selectRig()
    {
        selected = true;
        circleHUDAnimationLoopBegin();
        

        //Need to add in the selected oil rigs list

        if (!Manager.Instance.selectedOilRigsList.Contains(gameObject))
        {
            if (gameObject.GetComponent<OilRigProperties>().currentOilRigState != OilRigProperties.OIL_RIG_STATE.GREEN)
            {
                Manager.Instance.selectedOilRigsList.Add(gameObject);
                Manager.Instance.refillDuration+=1;
            }
        }
    }

    public void circleHUDAnimationBegin()
    {
        circleHUDAnimatorPlacHolder.SetBool(INIT, true);
    }

    public void circleHUDAnimationLoopBegin()
    {
        circleHUDAnimatorPlacHolder.SetBool(LOOP, true);
    }

    public void tickAnimationCheck(string transitionBool)
    {
        if (!tickAnimator.GetBool(OTG) && !tickAnimator.GetBool(RTG))
        {
            tickAnimator.SetBool(transitionBool, true);
        }
        else
        {
            tickAnimator.SetBool(OTG, false);
            tickAnimator.SetBool(RTG, false);
            tickAnimator.SetBool(transitionBool, true);
        }
    }

}
