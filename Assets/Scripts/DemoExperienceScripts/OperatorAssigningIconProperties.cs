﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OperatorAssigningIconProperties : MonoBehaviour
{
    public TextMeshProUGUI operatorNameText;
    public TextMeshProUGUI fieldOperatorText;

    public GameObject selectedOperatorIcon;
    public GameObject deselectedOperatorIcon;

    private void Start()
    {
        operatorNameText.color = new Color32(166, 166, 166, 255);
        fieldOperatorText.color = new Color32(166, 166, 166, 255);

        selectedOperatorIcon.SetActive(false);
        deselectedOperatorIcon.SetActive(true);
    }

    public void changeAssetsAfterSelection()
    {
        //if (Manager.Instance.operator1AvailableButton.GetComponentInChildren<TextMeshProUGUI>().text != "ASSIGNED")
        //{
        //    if(gameObject.name.CompareTo("Operator1"))
        //    {
        //        ServerHandler.Instance.SendData(1, 1, nameof(Manager.DEMO_STATES.OPERATOR_SELECTION_DISPLAY_PANEL));
        //    }
        //    else
        //    {
        //        ServerHandler.Instance.SendData(1,2, nameof(Manager.DEMO_STATES.OPERATOR_SELECTION_DISPLAY_PANEL));
        //    }
            operatorNameText.color = new Color32(255, 255, 255, 255);
            fieldOperatorText.color = new Color32(255, 255, 255, 255);

            selectedOperatorIcon.SetActive(true);
            deselectedOperatorIcon.SetActive(false);
        //}
    }

    public void callEngineerSelectionNetworkFunction()
    {
        if(transform.GetChild(0).GetComponent<TextMeshProUGUI>().text != "ASSIGNED")
        {
                // Server Call
                ServerHandler.Instance.SendData(2, 0, nameof(Manager.DEMO_STATES.ENGINEER_SELECTION_DISPLAY_PANEL));
        }
    }
}
