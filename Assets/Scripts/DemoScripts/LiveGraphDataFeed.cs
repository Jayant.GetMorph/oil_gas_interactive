﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;

public class LiveGraphDataFeed : MonoBehaviour
{
    public GraphChart graph;
    public float dataFeedValue;
    float lastTime = 0f;

    //public void plotGraph()
    //{
    //    graph.DataSource.AddPointToCategoryRealtime("Tank", System.DateTime.Now, dataFeedValue); // each time we call AddPointToCategory
    //    //graph.DataSource.AddPointToCategoryRealtime("Tank", JsonDataParser.Instance.dateTimeStamp, dataFeedValue); // each time we call AddPointToCategory
    //}


    void Update()
    {
        if (JsonDataParser.Instance.startGraphPlotting)
        {
            float time = Time.time;
            if (lastTime + 5f < time)
            {
                Debug.Log("Update is called");
                lastTime = time;
                graph.DataSource.AddPointToCategoryRealtime("Tank", System.DateTime.Now, dataFeedValue); // each time we call AddPointToCategory
            }
        }
    }
}