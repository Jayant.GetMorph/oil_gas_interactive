﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;

public class TankLeveler : MonoBehaviour
{
    //public LiquidVolume liquidVolume;
    public GraphDataFeed volumetricGraphDataFeed;
    public GraphDataFeed flowRateGraphDataFeed;
    public float v1;
    public float v2;
    public float t1;
    public float t2;
    public float flowRate;

    [Header("Tank Fill Image")]
    public Image fillImage;
    public Slider interactiveSlider;

    [Header("Public Text References")]
    public TextMeshProUGUI tankId;
    public TextMeshProUGUI volumePercentage;
    public TextMeshProUGUI volumeInGallonsText;
    public TextMeshProUGUI temperature;
    public TextMeshProUGUI flowRateText;
    public TextMeshProUGUI tampered;
    public TextMeshProUGUI geoLocation;

    private void Start()
    {
        v1 = 0;
        t1 = DateTime.Now.Second;
        InvokeRepeating("checkCurrentData", 0f, 1f);
    }

    private void FixedUpdate()
    {
        tankId.text = "Tank ID : Field 03 | PumpJack -  00-80-00-00-00-01-21-06";
        volumePercentage.text = "Volume % : " + Math.Round(fillImage.fillAmount * 100, 2);
        volumeInGallonsText.text = "Volume : 10 G";
        temperature.text = "Temperature : 73.4 F";
        flowRateText.text = flowRate.ToString() + " G/s";

        if (flowRate < 0)
        {
            flowRateText.color = Color.red;
        }
        else
        {
            flowRateText.color = Color.green;
        }
        tampered.text = "False";
        tampered.color = Color.green;
        geoLocation.text = "Geo Location : Wipro SVIC, Mountain View";
    }

    void checkCurrentData()
    {
        v2 = interactiveSlider.value;
        t2 = DateTime.Now.Second;
        flowRate = (v1 - v2) / (t2 - t1);
    }

    public void changeWaterLevel(float newWaterLevel)
    {
        fillImage.fillAmount = interactiveSlider.value / 10;
        volumetricGraphDataFeed.dataFeedValue = newWaterLevel;
        flowRateGraphDataFeed.dataFeedValue = flowRate;
    }

}

