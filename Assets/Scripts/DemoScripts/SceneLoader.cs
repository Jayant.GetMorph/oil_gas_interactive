﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            loadMainMenuScene();
        }
    }

    public void loadMainMenuScene()
    {
        SceneManager.LoadScene(0);
    }

    public void loadInteractiveDemoScene()
    {
        SceneManager.LoadScene(1);
    }

    public void loadLiveDemoScene()
    {
        SceneManager.LoadScene(2);
    }
}
