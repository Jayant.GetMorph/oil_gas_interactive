﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Globalization;
using TMPro;
using UnityEngine.UI;

public class JsonDataParser : MonoBehaviour
{
    public static JsonDataParser Instance;

    public LiveGraphDataFeed volumetricGraphDataFeed;
    public LiveGraphDataFeed flowRateGraphDataFeed;

    [Header("Public Text References")]
    public TextMeshProUGUI tankId;
    public TextMeshProUGUI volumePercentage;
    public TextMeshProUGUI volumeInGallonsText;
    public TextMeshProUGUI temperature;
    public TextMeshProUGUI flowRateText;
    public TextMeshProUGUI tampered;
    public TextMeshProUGUI geoLocation;

    [Header("Tank Fill Image")]
    public Image fillImage;

    string TimeStamp;
    string TankID;
    string GeoHASH;
    float Volume_Gallon;
    string Volume_percent;
    string Temperature_Fahrenheit;
    int Tamper_Alert;
    int height;
    public DateTime dateTimeStamp;

    float v1;
    float v2;
    int t1;
    int t2;
    float flowRate;

    public bool init;
    public bool startGraphPlotting;

    private void Awake()
    {
        Instance = this;
    }

    private void FixedUpdate()
    {
        tankId.text = "Tank ID : Field 03 | PumpJack - " + TankID;
        volumePercentage.text = "Volume % : " + Math.Round(Convert.ToDecimal(Volume_percent), 2);
        volumeInGallonsText.text = "Volume : " + Volume_Gallon.ToString() + " G";
        temperature.text = "Temperature : " + Math.Round(Convert.ToDecimal(Temperature_Fahrenheit), 2) + " F";
        flowRateText.text = flowRate.ToString() + " G/s";

        if (flowRate < 0)
        {
            flowRateText.color = Color.red;
        }
        else
        {
            flowRateText.color = Color.green;
        }

        if (Tamper_Alert == 0)
        {
            tampered.text = "False";
            tampered.color = Color.green;
        }
        else
        {
            tampered.text = "True";
            tampered.color = Color.red;
        }

        geoLocation.text = "Geo Location : Wipro SVIC, Mountain View";
    }

    public void webRequest(string jsonString)
    {
        RootObject rootObject = RootObject.CreateFromJson(jsonString);
        TankID = rootObject.TankID;
        GeoHASH = rootObject.GeoHASH;
        Volume_Gallon = rootObject.Volume_Gallon;
        Volume_percent = rootObject.Volume_percent;
        Temperature_Fahrenheit = rootObject.Temperature_Fahrenheit;
        Tamper_Alert = rootObject.Tamper_Alert;
        height = rootObject.height;
        fillImage.fillAmount = (float.Parse(Volume_percent, CultureInfo.InvariantCulture)) / 100;

        if (string.IsNullOrEmpty(TimeStamp))
        {
            TimeStamp = rootObject.TimeStamp;
            init = true;
        }
        else
        {
            init = false;
            TimeStamp = rootObject.TimeStamp;
        }

        validateTimeStamp();
    }

    void validateTimeStamp()
    {
        string str1 = TimeStamp.Substring(0, 23);
        string[] str2 = str1.Split(new string[] { "T" }, StringSplitOptions.None);

        DateTime combined = DateTime.Parse(str2[0] + ' ' + str2[1], new CultureInfo("UK"));
        dateTimeStamp = combined;

        if (init)
        {
            t1 = returnCurrentTime(dateTimeStamp);
            v1 = Volume_Gallon;
            //volumetricGraphDataFeed.plotGraph();
        }
        else
        {
            t2 = returnCurrentTime(dateTimeStamp);
            v2 = Volume_Gallon;
            checkCurrentData();
        }
    }

    void intiatePopulatingData()
    {
        t2 = returnCurrentTime(dateTimeStamp);
        v2 = Volume_Gallon;
        checkCurrentData();
    }

    void checkCurrentData()
    {
        flowRate = (v1 - v2) / (t2 - t1);
        volumetricGraphDataFeed.dataFeedValue = Volume_Gallon;
        flowRateGraphDataFeed.dataFeedValue = flowRate;
        startGraphPlotting = true;
        //InvokeRepeating("initiateGraphPlot", 0f, 5f);
        //initiateGraphPlot();
    }

    int returnCurrentTime(DateTime timeStamp)
    {
        DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
        int cur_time = (int)(timeStamp - epochStart).TotalSeconds;
        return cur_time;
    }

    //void initiateGraphPlot()
    //{
    //    //Debug.Log("initiateGraphPlot");
    //    //volumetricGraphDataFeed.plotGraph();
    //    //flowRateGraphDataFeed.plotGraph();
    //}
}

[Serializable]
public class RootObject
{
    public string TimeStamp;
    public string TankID;
    public string GeoHASH;
    public float Volume_Gallon;
    public string Volume_percent;
    public string Temperature_Fahrenheit;
    public int Tamper_Alert;
    public int height;

    public static RootObject CreateFromJson(string jsonString)
    {
        return JsonUtility.FromJson<RootObject>(jsonString);
    }
}
