﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;

public class GraphDataFeed : MonoBehaviour
{
    public GraphChart graph;
    float lastTime = 0f;
    public float dataFeedValue;

    void Update()
    {
        float time = Time.time;
        if (lastTime + 1f < time)
        {
            Debug.Log("Update is called");
            lastTime = time;
            graph.DataSource.AddPointToCategoryRealtime("Tank", System.DateTime.Now, dataFeedValue); // each time we call AddPointToCategory
        }
    }
}