﻿using UnityEngine;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Util;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using UnityEngine.UI;
using System;

public class ServerHandler : MonoBehaviour
{
    public string defaultHost = "192.168.1.128";   // Default host
    public int defaultTcpPort = 9933;          // Default TCP port
    private const string defaultZone = "WiproIOT";
    private const string defaultRoom = "Lobby";
    private const string defaultRequestCMD = "Data";

    private SmartFox sfs;

    //public Text debugText;

    public static Action<int,int,string> OnServerResponse;
    public static ServerHandler Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
       // ConnectToServer();
    }

    void Update()
    {
        // As Unity is not thread safe, we process the queued up callbacks on every frame
        if (sfs != null)
            sfs.ProcessEvents();

        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    ReconnectSFS();
        //}
    }

    void OnApplicationQuit()
    {
        // Always disconnect before quitting
        if (sfs != null && sfs.IsConnected)
            sfs.Disconnect();
    }

    public void ConnectToServer() {
        // Initialize SFS2X client and add listeners
        sfs = new SmartFox();


        sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
        sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
        sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
        sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);
        sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnRoomJoin);
        sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnRoomJoinError);

        // Set connection parameters
        ConfigData cfg = new ConfigData();
        cfg.Host = defaultHost;
        cfg.Port = defaultTcpPort;
        cfg.Zone = defaultZone;
        cfg.Debug = false;

        // Connect to SFS2X
        sfs.Connect(cfg);

    }

    private void OnConnectionLost(BaseEvent evt)
    {
        Debug.Log("OnConnectionLost");
        Debug.Log("Connection was lost; reason is: " + (string)evt.Params["reason"]);

        ResetSFS();
    }

    private void OnConnection(BaseEvent evt)
    {
        Debug.Log("OnConnection");
        if ((bool)evt.Params["success"])
        {
            Debug.Log("Connection established successfully");
            Debug.Log("SFS2X API version: " + sfs.Version);
            Debug.Log("Connection mode is: " + sfs.ConnectionMode);

            SmartFoxConnection.Connection = sfs;

            sfs.Send(new LoginRequest(""));
            ConnectionManager.Instance.connectionResponse(true);

        }
        else
        {
            Debug.Log("Connection failed; is the server running at all?");
            ConnectionManager.Instance.connectionResponse(false);

            // Remove SFS2X listeners and re-enable interface
            //ResetSFS();
        }
    }

    private void ResetSFS()
    {
        // Remove SFS2X listeners
        sfs.RemoveEventListener(SFSEvent.CONNECTION, OnConnection);
        sfs.RemoveEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        sfs.RemoveEventListener(SFSEvent.LOGIN, OnLogin);
        sfs.RemoveEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
        sfs.RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);
        sfs.RemoveEventListener(SFSEvent.ROOM_JOIN, OnRoomJoin);
        sfs.RemoveEventListener(SFSEvent.ROOM_JOIN_ERROR, OnRoomJoinError);

        sfs = null;

        ConnectToServer();
    }

    private void OnLogin(BaseEvent evt)
    {
        Debug.Log("Login success");
        sfs.Send(new JoinRoomRequest("Lobby"));
    }

    private void OnLoginError(BaseEvent evt)
    {
        // Disconnect
        sfs.Disconnect();
        // Show error message
        Debug.Log("Login failed: " + (string)evt.Params["errorMessage"]);
    }

    private void OnExtensionResponse(BaseEvent evt)
    {
        string cmd = (string)evt.Params["cmd"];
        SFSObject dataObject = (SFSObject)evt.Params["params"];
       // Debug.Log(cmd);
        switch (cmd) {
            case defaultRequestCMD:
                //Debug.Log(dataObject.GetInt("id"));
                //debugText.text = dataObject.GetInt("id").ToString();
                OnServerResponse?.Invoke(dataObject.GetInt("groupId"),dataObject.GetInt("id"),dataObject.GetUtfString("name"));
                break;
        }
    }

    private void OnRoomJoin(BaseEvent evt)
    {
        Debug.Log("OnRoomJoin");
    }

    private void OnRoomJoinError(BaseEvent evt)
    {
        // Show error message
        Debug.Log("OnRoomJoinError");
    }

    public void SendData(int groupId,int id, string name) {
        ISFSObject obj = new SFSObject();
        obj.PutInt("groupId", groupId);
        obj.PutInt("id", id);
        obj.PutUtfString("name", name);

        Debug.Log("Sent state name : " + name);
        Debug.Log("groupId : " + groupId);
        Debug.Log("id : " + id);

        sfs.Send(new ExtensionRequest(defaultRequestCMD, obj, sfs.LastJoinedRoom));
    }

    public void ReconnectSFS() {

        //Debug.Log(sfs.IsConnected);

        //ResetSFS();

        if (sfs.IsConnected)
        {
            sfs.Send(new LoginRequest(""));
        }
        else {
            ResetSFS();
        }


        //ConnectToServer();
    }

}
