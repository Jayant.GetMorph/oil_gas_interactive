﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionAvoider : MonoBehaviour
{

    MSVehicleController vehicleController;

    void Awake() {
        vehicleController = GetComponentInChildren<MSVehicleController>();
    }

    void OnTriggerStay(Collider other) {
         Debug.Log(other.name);
        if(other.name.Equals("Red")) {
            // vehicleController.handBrakeTrue = true;
            vehicleController._AISettings._AIHorizontalInput = -1f;
            other.GetComponentInChildren<MSVehicleController>()._AISettings._AIHorizontalInput = 1f;
        }
    }

/* 
    void OnTriggerExit(Collider other) {
        // Debug.Log(other.name);
        if(other.name.Equals("Red")) {
            vehicleController.handBrakeTrue = false;
        }
    }
*/

}
