<Q                         DIRECTIONAL    LIQUID_VOLUME_CUBE     LIQUID_VOLUME_DEPTH_AWARE      LIQUID_VOLUME_DEPTH_AWARE_PASS     LIQUID_VOLUME_IGNORE_GRAVITY    m0  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 _ZBufferParams;
    float4 hlslcc_mtx4x4unity_CameraProjection[4];
    float4 hlslcc_mtx4x4unity_CameraToWorld[4];
    half4 _WorldSpaceLightPos0;
    half4 _LightColor0;
    float _DepthAwareOffset;
    half4 _Color1;
    float _FoamBottom;
    float _LevelPos;
    float4 _Size;
    float3 _Center;
    float4 _Turbulence;
    float _DeepAtten;
    half4 _SmokeColor;
    float _SmokeAtten;
    int _SmokeRaySteps;
    int _LiquidRaySteps;
    half3 _GlossinessInt;
    float4 hlslcc_mtx4x4_Rot[4];
    float _UpperLimit;
    float _LowerLimit;
    half3 _EmissionColor;
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float4 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    half3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_NoiseTex2D [[ sampler (0) ]],
    sampler sampler_CameraDepthTexture [[ sampler (1) ]],
    sampler sampler_VLFrontBufferTexture [[ sampler (2) ]],
    texture2d<float, access::sample > _CameraDepthTexture [[ texture(0) ]] ,
    texture2d<half, access::sample > _VLFrontBufferTexture [[ texture(1) ]] ,
    texture2d<half, access::sample > _NoiseTex2D [[ texture(2) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    half3 u_xlat16_0;
    float3 u_xlat1;
    float4 u_xlat2;
    half4 u_xlat16_2;
    bool u_xlatb2;
    float4 u_xlat3;
    half4 u_xlat16_3;
    float3 u_xlat4;
    half4 u_xlat16_4;
    int u_xlati4;
    bool u_xlatb4;
    float4 u_xlat5;
    half4 u_xlat16_5;
    float4 u_xlat6;
    half4 u_xlat16_6;
    float3 u_xlat7;
    half4 u_xlat16_7;
    half3 u_xlat16_8;
    half3 u_xlat16_9;
    float2 u_xlat12;
    bool u_xlatb12;
    half u_xlat16_18;
    float2 u_xlat22;
    bool u_xlatb22;
    float u_xlat24;
    bool u_xlatb24;
    float u_xlat30;
    float u_xlat31;
    bool u_xlatb31;
    float u_xlat32;
    int u_xlati32;
    half u_xlat16_38;
    u_xlat0.xyz = (-input.TEXCOORD1.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat30 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat30 = rsqrt(u_xlat30);
    u_xlat1.xyz = float3(u_xlat30) * u_xlat0.xyz;
    u_xlatb31 = FGlobals._UpperLimit<input.TEXCOORD4.y;
    u_xlatb2 = input.TEXCOORD4.y<FGlobals._LowerLimit;
    u_xlatb31 = u_xlatb31 || u_xlatb2;
    if(!u_xlatb31){
        u_xlat2.xyz = FGlobals._WorldSpaceCameraPos.xyzx.xyz + (-FGlobals._Center.xyzx.xyz);
        u_xlat3.xyz = u_xlat2.yyy * FGlobals.hlslcc_mtx4x4_Rot[1].xyz;
        u_xlat2.xyw = fma(FGlobals.hlslcc_mtx4x4_Rot[0].xyz, u_xlat2.xxx, u_xlat3.xyz);
        u_xlat2.xyz = fma(FGlobals.hlslcc_mtx4x4_Rot[2].xyz, u_xlat2.zzz, u_xlat2.xyw);
        u_xlat3.xyz = u_xlat2.xyz + FGlobals._Center.xyzx.xyz;
        u_xlat4.xyz = (-u_xlat3.xyz) + input.TEXCOORD3.xyz;
        u_xlat31 = dot(u_xlat4.xyz, u_xlat4.xyz);
        u_xlat31 = sqrt(u_xlat31);
        u_xlat4.xyz = u_xlat4.xyz / float3(u_xlat31);
        u_xlat5.xyz = float3(1.0, 1.0, 1.0) / u_xlat4.xyz;
        u_xlat6.xyz = (-u_xlat2.xyz) + (-FGlobals._Size.www);
        u_xlat6.xyz = u_xlat5.xyz * u_xlat6.xyz;
        u_xlat2.xyz = (-u_xlat2.xyz) + FGlobals._Size.www;
        u_xlat2.xyz = u_xlat2.xyz * u_xlat5.xyz;
        u_xlat5.xyz = min(u_xlat6.xyz, u_xlat2.xyz);
        u_xlat2.xyz = max(u_xlat6.xyz, u_xlat2.xyz);
        u_xlat5.xy = max(u_xlat5.yz, u_xlat5.xx);
        u_xlat31 = max(u_xlat5.y, u_xlat5.x);
        u_xlat2.xy = min(u_xlat2.yz, u_xlat2.xx);
        u_xlat2.x = min(u_xlat2.y, u_xlat2.x);
        u_xlat5.x = max(u_xlat31, 0.0);
        u_xlat12.xy = input.TEXCOORD2.xy / input.TEXCOORD2.ww;
        u_xlat31 = _CameraDepthTexture.sample(sampler_CameraDepthTexture, u_xlat12.xy).x;
        u_xlat31 = fma(FGlobals._ZBufferParams.z, u_xlat31, FGlobals._ZBufferParams.w);
        u_xlat31 = float(1.0) / u_xlat31;
        u_xlat6.xy = fma(u_xlat12.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
        u_xlat7.x = u_xlat6.x / FGlobals.hlslcc_mtx4x4unity_CameraProjection[0].x;
        u_xlat7.y = u_xlat6.y / FGlobals.hlslcc_mtx4x4unity_CameraProjection[1].y;
        u_xlat7.z = -1.0;
        u_xlat6.xyz = float3(u_xlat31) * u_xlat7.xyz;
        u_xlat7.xyz = u_xlat6.yyy * FGlobals.hlslcc_mtx4x4unity_CameraToWorld[1].xyz;
        u_xlat6.xyw = fma(FGlobals.hlslcc_mtx4x4unity_CameraToWorld[0].xyz, u_xlat6.xxx, u_xlat7.xyz);
        u_xlat6.xyz = fma(FGlobals.hlslcc_mtx4x4unity_CameraToWorld[2].xyz, u_xlat6.zzz, u_xlat6.xyw);
        u_xlat6.xyz = u_xlat6.xyz + FGlobals.hlslcc_mtx4x4unity_CameraToWorld[3].xyz;
        u_xlat3.xzw = u_xlat3.xyz + (-u_xlat6.xyz);
        u_xlat31 = dot(u_xlat3.xzw, u_xlat3.xzw);
        u_xlat31 = sqrt(u_xlat31);
        u_xlat31 = u_xlat31 + FGlobals._DepthAwareOffset;
        u_xlat31 = min(u_xlat31, u_xlat2.x);
        u_xlat16_2.x = _VLFrontBufferTexture.sample(sampler_VLFrontBufferTexture, u_xlat12.xy).x;
        u_xlat5.w = min(u_xlat31, float(u_xlat16_2.x));
        u_xlat31 = fma(u_xlat4.y, u_xlat5.x, u_xlat3.y);
        u_xlat16_2.x = _NoiseTex2D.sample(sampler_NoiseTex2D, input.TEXCOORD4.xz).y;
        u_xlat16_2.x = u_xlat16_2.x + half(-0.5);
        u_xlat12.x = sin(input.TEXCOORD4.w);
        u_xlat12.x = u_xlat12.x * FGlobals._Turbulence.y;
        u_xlat2.x = fma(float(u_xlat16_2.x), FGlobals._Turbulence.x, u_xlat12.x);
        u_xlat2.x = u_xlat2.x * FGlobals._Size.y;
        u_xlat2.x = fma(u_xlat2.x, 0.0500000007, FGlobals._LevelPos);
        u_xlat12.x = dot(u_xlat4.xz, u_xlat4.xz);
        u_xlat12.x = sqrt(u_xlat12.x);
        u_xlat3.y = u_xlat12.x / u_xlat4.y;
        u_xlat12.x = u_xlat31 + (-u_xlat2.x);
        u_xlat3.x = 1.0;
        u_xlat12.xy = u_xlat12.xx * u_xlat3.xy;
        u_xlat12.x = dot(u_xlat12.xy, u_xlat12.xy);
        u_xlat12.x = sqrt(u_xlat12.x);
        u_xlat5.z = u_xlat12.x + u_xlat5.x;
        u_xlatb12 = u_xlat2.x<u_xlat31;
        if(u_xlatb12){
            u_xlatb22 = u_xlat4.y<0.0;
            u_xlat32 = min(u_xlat5.w, u_xlat5.z);
            u_xlat22.x = (u_xlatb22) ? u_xlat32 : u_xlat5.w;
            u_xlat22.x = (-u_xlat5.x) + u_xlat22.x;
            u_xlat32 = float(FGlobals._SmokeRaySteps);
            u_xlat22.x = u_xlat22.x / u_xlat32;
            u_xlat16_3.xyz = FGlobals._SmokeColor.www * FGlobals._SmokeColor.xyz;
            u_xlat16_3.w = FGlobals._SmokeColor.w;
            u_xlat16_6.x = half(0.0);
            u_xlat16_6.y = half(0.0);
            u_xlat16_6.z = half(0.0);
            u_xlat16_6.w = half(0.0);
            u_xlat32 = u_xlat31;
            u_xlati4 = 0x0;
            while(true){
                u_xlatb24 = u_xlati4>=FGlobals._SmokeRaySteps;
                if(u_xlatb24){break;}
                u_xlat24 = (-u_xlat32) + u_xlat2.x;
                u_xlat24 = u_xlat24 / FGlobals._Size.y;
                u_xlat24 = u_xlat24 * FGlobals._SmokeAtten;
                u_xlat24 = u_xlat24 * 1.44269502;
                u_xlat24 = exp2(u_xlat24);
                u_xlat16_7 = half4(float4(u_xlat16_3) * float4(u_xlat24));
                u_xlat16_8.x = (-u_xlat16_6.w) + half(1.0);
                u_xlat16_6 = fma(u_xlat16_7, u_xlat16_8.xxxx, u_xlat16_6);
                u_xlat32 = fma(u_xlat4.y, u_xlat22.x, u_xlat32);
                u_xlati4 = u_xlati4 + 0x1;
            }
        } else {
            u_xlat16_6.x = half(0.0);
            u_xlat16_6.y = half(0.0);
            u_xlat16_6.z = half(0.0);
            u_xlat16_6.w = half(0.0);
        }
        u_xlatb31 = u_xlat4.y<0.0;
        u_xlatb22 = u_xlat5.z<u_xlat5.w;
        u_xlat16_8.x = (u_xlatb22) ? half(0.100000001) : half(0.0);
        u_xlat5.y = -99999.0;
        u_xlat4.xz = (bool(u_xlatb31)) ? u_xlat5.zw : u_xlat5.xy;
        u_xlat16_8.x = (u_xlatb31) ? u_xlat16_8.x : half(0.0);
        u_xlatb31 = 0.0<u_xlat4.y;
        u_xlatb31 = u_xlatb22 && u_xlatb31;
        u_xlat22.x = FGlobals._FoamBottom * 0.100000001;
        u_xlat16_18 = (u_xlatb31) ? half(u_xlat22.x) : half(0.0);
        u_xlat22.xy = (bool(u_xlatb12)) ? u_xlat4.xz : u_xlat5.xw;
        u_xlat16_3 = (bool(u_xlatb12)) ? u_xlat16_8.xxxx : half4(u_xlat16_18);
        u_xlatb31 = u_xlat22.x<u_xlat22.y;
        if(u_xlatb31){
            u_xlat31 = (-u_xlat22.x) + u_xlat22.y;
            u_xlat32 = float(FGlobals._LiquidRaySteps);
            u_xlat31 = u_xlat31 / u_xlat32;
            u_xlat22.x = fma(u_xlat4.y, u_xlat22.x, FGlobals._WorldSpaceCameraPos.xyzx.y);
            u_xlat2.x = (-u_xlat2.x) + u_xlat22.x;
            u_xlat16_5.w = FGlobals._Color1.w * FGlobals._Color1.w;
            u_xlat16_8.xyz = u_xlat16_5.www * FGlobals._Color1.xyz;
            u_xlat16_3.xyz = u_xlat16_3.www;
            u_xlat16_7.w = u_xlat16_3.w;
            u_xlat22.x = u_xlat2.x;
            u_xlati32 = 0x0;
            while(true){
                u_xlatb4 = u_xlati32>=FGlobals._LiquidRaySteps;
                if(u_xlatb4){break;}
                u_xlat4.x = u_xlat22.x / FGlobals._Size.y;
                u_xlat4.x = u_xlat4.x * FGlobals._DeepAtten;
                u_xlat4.x = u_xlat4.x * 1.44269502;
                u_xlat4.x = exp2(u_xlat4.x);
                u_xlat16_5.xyz = half3(u_xlat4.xxx * float3(u_xlat16_8.xyz));
                u_xlat16_38 = (-u_xlat16_7.w) + half(1.0);
                u_xlat16_7.xyz = u_xlat16_3.xyz;
                u_xlat16_7 = fma(u_xlat16_5, half4(u_xlat16_38), u_xlat16_7);
                u_xlat22.x = fma(u_xlat4.y, u_xlat31, u_xlat22.x);
                u_xlati32 = u_xlati32 + 0x1;
                u_xlat16_3.xyz = u_xlat16_7.xyz;
            }
            u_xlat16_3.w = u_xlat16_7.w;
        }
        u_xlat16_8.x = (-u_xlat16_6.w) + half(1.0);
        u_xlat16_8.x = clamp(u_xlat16_8.x, 0.0h, 1.0h);
        u_xlat16_4 = fma(u_xlat16_3, u_xlat16_8.xxxx, u_xlat16_6);
        u_xlat16_8.x = (-u_xlat16_3.w) + half(1.0);
        u_xlat16_8.x = clamp(u_xlat16_8.x, 0.0h, 1.0h);
        u_xlat16_3 = fma(u_xlat16_6, u_xlat16_8.xxxx, u_xlat16_3);
        u_xlat16_2 = (bool(u_xlatb12)) ? u_xlat16_4 : u_xlat16_3;
        u_xlat16_8.xyz = u_xlat16_2.xyz * FGlobals._EmissionColor.xyzx.xyz;
    } else {
        u_xlat16_8.x = half(0.0);
        u_xlat16_8.y = half(0.0);
        u_xlat16_8.z = half(0.0);
        u_xlat16_2.x = half(0.0);
        u_xlat16_2.y = half(0.0);
        u_xlat16_2.z = half(0.0);
        u_xlat16_2.w = half(0.0);
    }
    u_xlat16_9.xyz = half3(fma(u_xlat0.xyz, float3(u_xlat30), float3(FGlobals._WorldSpaceLightPos0.xyz)));
    u_xlat16_38 = dot(u_xlat16_9.xyz, u_xlat16_9.xyz);
    u_xlat16_38 = rsqrt(u_xlat16_38);
    u_xlat16_9.xyz = half3(u_xlat16_38) * u_xlat16_9.xyz;
    u_xlat16_38 = dot(input.TEXCOORD0.xyz, float3(FGlobals._WorldSpaceLightPos0.xyz));
    u_xlat16_38 = fma(u_xlat16_38, half(0.5), half(0.5));
    u_xlat16_38 = max(u_xlat16_38, half(0.0));
    u_xlat16_9.x = dot(input.TEXCOORD0.xyz, float3(u_xlat16_9.xyz));
    u_xlat16_9.x = max(u_xlat16_9.x, half(0.0));
    u_xlat16_0.x = log2(u_xlat16_9.x);
    u_xlat16_0.x = u_xlat16_0.x * FGlobals._GlossinessInt.xyzx.x;
    u_xlat16_0.x = exp2(u_xlat16_0.x);
    u_xlat16_9.x = dot(u_xlat1.xyz, (-float3(FGlobals._WorldSpaceLightPos0.xyz)));
    u_xlat16_9.x = max(u_xlat16_9.x, half(0.0));
    u_xlat16_9.x = log2(u_xlat16_9.x);
    u_xlat16_9.x = u_xlat16_9.x * FGlobals._GlossinessInt.xyzx.y;
    u_xlat16_9.x = exp2(u_xlat16_9.x);
    u_xlat16_38 = fma(u_xlat16_9.x, FGlobals._GlossinessInt.xyzx.z, u_xlat16_38);
    u_xlat16_9.xyz = u_xlat16_2.xyz * FGlobals._LightColor0.xyz;
    u_xlat16_0.xyz = u_xlat16_0.xxx * FGlobals._LightColor0.xyz;
    u_xlat16_0.xyz = fma(u_xlat16_9.xyz, half3(u_xlat16_38), u_xlat16_0.xyz);
    u_xlat16_9.xyz = fma(u_xlat16_2.xyz, input.TEXCOORD5.xyz, u_xlat16_0.xyz);
    output.SV_Target0.w = u_xlat16_2.w;
    output.SV_Target0.xyz = u_xlat16_8.xyz + u_xlat16_9.xyz;
    return output;
}
                                 FGlobals�        _WorldSpaceCameraPos                         _ZBufferParams                          _WorldSpaceLightPos0                 �      _LightColor0                 �      _DepthAwareOffset                     �      _Color1                  �      _FoamBottom                   �   	   _LevelPos                     �      _Size                     �      _Center                   �      _Turbulence                   �   
   _DeepAtten                          _SmokeColor                       _SmokeAtten                        _SmokeRaySteps                        _LiquidRaySteps                       _GlossinessInt                         _UpperLimit                   p     _LowerLimit                   t     _EmissionColor                   x     unity_CameraProjection                          unity_CameraToWorld                  `      _Rot                 0            _CameraDepthTexture                  _VLFrontBufferTexture                   _NoiseTex2D                  FGlobals           