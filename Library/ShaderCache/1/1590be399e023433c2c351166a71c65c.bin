<Q                         DIRECTIONAL    LIQUID_VOLUME_CUBE     LIQUID_VOLUME_DEPTH_AWARE      LIQUID_VOLUME_NON_AABB      P"  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 _Time;
    float3 _WorldSpaceCameraPos;
    float4 _ZBufferParams;
    float4 hlslcc_mtx4x4unity_CameraProjection[4];
    float4 hlslcc_mtx4x4unity_CameraToWorld[4];
    half4 _WorldSpaceLightPos0;
    half4 _LightColor0;
    float _DepthAwareOffset;
    float _LevelPos;
    float4 _Size;
    float3 _Center;
    float4 _Turbulence;
    half4 _SmokeColor;
    float _SmokeAtten;
    int _SmokeRaySteps;
    float _SmokeSpeed;
    half3 _GlossinessInt;
    float4 _Scale;
    float4 hlslcc_mtx4x4_Rot[4];
    float _UpperLimit;
    float _LowerLimit;
    half3 _EmissionColor;
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float4 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    half3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_CameraDepthTexture [[ sampler (0) ]],
    sampler sampler_NoiseTex [[ sampler (1) ]],
    texture2d<float, access::sample > _CameraDepthTexture [[ texture(0) ]] ,
    texture3d<half, access::sample > _NoiseTex [[ texture(1) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    half3 u_xlat16_0;
    float3 u_xlat1;
    float3 u_xlat2;
    bool u_xlatb2;
    float4 u_xlat3;
    float4 u_xlat4;
    float3 u_xlat5;
    half4 u_xlat16_5;
    float3 u_xlat6;
    float3 u_xlat7;
    half4 u_xlat16_7;
    half3 u_xlat16_8;
    half3 u_xlat16_9;
    float u_xlat23;
    float u_xlat30;
    float u_xlat31;
    int u_xlati31;
    bool u_xlatb31;
    float u_xlat32;
    float u_xlat33;
    bool u_xlatb33;
    half u_xlat16_34;
    half u_xlat16_38;
    u_xlat0.xyz = (-input.TEXCOORD1.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat30 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat30 = rsqrt(u_xlat30);
    u_xlat1.xyz = float3(u_xlat30) * u_xlat0.xyz;
    u_xlatb31 = FGlobals._UpperLimit<input.TEXCOORD4.y;
    u_xlatb2 = input.TEXCOORD4.y<FGlobals._LowerLimit;
    u_xlatb31 = u_xlatb31 || u_xlatb2;
    if(!u_xlatb31){
        u_xlat2.xyz = input.TEXCOORD3.xyz + (-FGlobals._WorldSpaceCameraPos.xyzx.xyz);
        u_xlat31 = dot(u_xlat2.xyz, u_xlat2.xyz);
        u_xlat31 = sqrt(u_xlat31);
        u_xlat2.xyz = u_xlat2.xyz / float3(u_xlat31);
        u_xlat3.xyz = u_xlat2.yyy * FGlobals.hlslcc_mtx4x4_Rot[1].xyz;
        u_xlat3.xyz = fma(FGlobals.hlslcc_mtx4x4_Rot[0].xyz, u_xlat2.xxx, u_xlat3.xyz);
        u_xlat3.xyz = fma(FGlobals.hlslcc_mtx4x4_Rot[2].xyz, u_xlat2.zzz, u_xlat3.xyz);
        u_xlat4.xyz = FGlobals._WorldSpaceCameraPos.xyzx.xyz + (-FGlobals._Center.xyzx.xyz);
        u_xlat5.xyz = u_xlat4.yyy * FGlobals.hlslcc_mtx4x4_Rot[1].xyz;
        u_xlat4.xyw = fma(FGlobals.hlslcc_mtx4x4_Rot[0].xyz, u_xlat4.xxx, u_xlat5.xyz);
        u_xlat4.xyz = fma(FGlobals.hlslcc_mtx4x4_Rot[2].xyz, u_xlat4.zzz, u_xlat4.xyw);
        u_xlat3.xyz = float3(1.0, 1.0, 1.0) / u_xlat3.xyz;
        u_xlat5.xyz = (-u_xlat4.xyz) + (-FGlobals._Size.www);
        u_xlat5.xyz = u_xlat3.xyz * u_xlat5.xyz;
        u_xlat4.xyz = (-u_xlat4.xyz) + FGlobals._Size.www;
        u_xlat3.xyz = u_xlat3.xyz * u_xlat4.xyz;
        u_xlat4.xyz = min(u_xlat5.xyz, u_xlat3.xyz);
        u_xlat3.xyz = max(u_xlat5.xyz, u_xlat3.xyz);
        u_xlat4.xy = max(u_xlat4.yz, u_xlat4.xx);
        u_xlat31 = max(u_xlat4.y, u_xlat4.x);
        u_xlat3.xy = min(u_xlat3.yz, u_xlat3.xx);
        u_xlat32 = min(u_xlat3.y, u_xlat3.x);
        u_xlat31 = max(u_xlat31, 0.0);
        u_xlat3.xy = input.TEXCOORD2.xy / input.TEXCOORD2.ww;
        u_xlat23 = _CameraDepthTexture.sample(sampler_CameraDepthTexture, u_xlat3.xy).x;
        u_xlat23 = fma(FGlobals._ZBufferParams.z, u_xlat23, FGlobals._ZBufferParams.w);
        u_xlat23 = float(1.0) / u_xlat23;
        u_xlat3.xy = fma(u_xlat3.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
        u_xlat4.x = u_xlat3.x / FGlobals.hlslcc_mtx4x4unity_CameraProjection[0].x;
        u_xlat4.y = u_xlat3.y / FGlobals.hlslcc_mtx4x4unity_CameraProjection[1].y;
        u_xlat4.z = -1.0;
        u_xlat3.xyz = float3(u_xlat23) * u_xlat4.xyz;
        u_xlat4.xyz = u_xlat3.yyy * FGlobals.hlslcc_mtx4x4unity_CameraToWorld[1].xyz;
        u_xlat3.xyw = fma(FGlobals.hlslcc_mtx4x4unity_CameraToWorld[0].xyz, u_xlat3.xxx, u_xlat4.xyz);
        u_xlat3.xyz = fma(FGlobals.hlslcc_mtx4x4unity_CameraToWorld[2].xyz, u_xlat3.zzz, u_xlat3.xyw);
        u_xlat3.xyz = u_xlat3.xyz + FGlobals.hlslcc_mtx4x4unity_CameraToWorld[3].xyz;
        u_xlat3.xyz = (-u_xlat3.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
        u_xlat3.x = dot(u_xlat3.xyz, u_xlat3.xyz);
        u_xlat3.x = sqrt(u_xlat3.x);
        u_xlat3.x = u_xlat3.x + FGlobals._DepthAwareOffset;
        u_xlat32 = min(u_xlat32, u_xlat3.x);
        u_xlat32 = (-u_xlat31) + u_xlat32;
        u_xlat3.x = float(FGlobals._SmokeRaySteps);
        u_xlat32 = u_xlat32 / u_xlat3.x;
        u_xlat3.xyz = fma(u_xlat2.xyz, float3(u_xlat31), FGlobals._WorldSpaceCameraPos.xyzx.xyz);
        u_xlat31 = FGlobals._Time.x * FGlobals._Turbulence.x;
        u_xlat31 = u_xlat31 * FGlobals._Size.y;
        u_xlat4.y = u_xlat31 * FGlobals._SmokeSpeed;
        u_xlat4.x = float(0.0);
        u_xlat4.z = float(0.0);
        u_xlat16_5.x = half(0.0);
        u_xlat16_5.y = half(0.0);
        u_xlat16_5.z = half(0.0);
        u_xlat16_5.w = half(0.0);
        u_xlat6.xyz = u_xlat3.xyz;
        u_xlati31 = FGlobals._SmokeRaySteps;
        while(true){
            u_xlatb33 = 0x0>=u_xlati31;
            if(u_xlatb33){break;}
            u_xlat33 = (-u_xlat6.y) + FGlobals._LevelPos;
            u_xlat33 = u_xlat33 / FGlobals._Size.y;
            u_xlat33 = u_xlat33 * FGlobals._SmokeAtten;
            u_xlat33 = u_xlat33 * 1.44269502;
            u_xlat33 = exp2(u_xlat33);
            u_xlat7.xyz = (-u_xlat4.xyz) + u_xlat6.xyz;
            u_xlat7.xyz = u_xlat7.xyz * FGlobals._Scale.xxx;
            u_xlat16_34 = _NoiseTex.sample(sampler_NoiseTex, u_xlat7.xyz, level(0.0)).x;
            u_xlat16_7.w = u_xlat16_34 * FGlobals._SmokeColor.w;
            u_xlat16_7.xyz = u_xlat16_7.www * FGlobals._SmokeColor.xyz;
            u_xlat16_7 = half4(float4(u_xlat33) * float4(u_xlat16_7));
            u_xlat16_8.x = (-u_xlat16_5.w) + half(1.0);
            u_xlat16_5 = fma(u_xlat16_7, u_xlat16_8.xxxx, u_xlat16_5);
            u_xlati31 = u_xlati31 + int(0xffffffffu);
            u_xlat6.xyz = fma(u_xlat2.xyz, float3(u_xlat32), u_xlat6.xyz);
        }
        u_xlat16_8.xyz = u_xlat16_5.xyz * FGlobals._EmissionColor.xyzx.xyz;
    } else {
        u_xlat16_8.x = half(0.0);
        u_xlat16_8.y = half(0.0);
        u_xlat16_8.z = half(0.0);
        u_xlat16_5.x = half(0.0);
        u_xlat16_5.y = half(0.0);
        u_xlat16_5.z = half(0.0);
        u_xlat16_5.w = half(0.0);
    }
    u_xlat16_9.xyz = half3(fma(u_xlat0.xyz, float3(u_xlat30), float3(FGlobals._WorldSpaceLightPos0.xyz)));
    u_xlat16_38 = dot(u_xlat16_9.xyz, u_xlat16_9.xyz);
    u_xlat16_38 = rsqrt(u_xlat16_38);
    u_xlat16_9.xyz = half3(u_xlat16_38) * u_xlat16_9.xyz;
    u_xlat16_38 = dot(input.TEXCOORD0.xyz, float3(FGlobals._WorldSpaceLightPos0.xyz));
    u_xlat16_38 = fma(u_xlat16_38, half(0.5), half(0.5));
    u_xlat16_38 = max(u_xlat16_38, half(0.0));
    u_xlat16_9.x = dot(input.TEXCOORD0.xyz, float3(u_xlat16_9.xyz));
    u_xlat16_9.x = max(u_xlat16_9.x, half(0.0));
    u_xlat16_0.x = log2(u_xlat16_9.x);
    u_xlat16_0.x = u_xlat16_0.x * FGlobals._GlossinessInt.xyzx.x;
    u_xlat16_0.x = exp2(u_xlat16_0.x);
    u_xlat16_9.x = dot(u_xlat1.xyz, (-float3(FGlobals._WorldSpaceLightPos0.xyz)));
    u_xlat16_9.x = max(u_xlat16_9.x, half(0.0));
    u_xlat16_9.x = log2(u_xlat16_9.x);
    u_xlat16_9.x = u_xlat16_9.x * FGlobals._GlossinessInt.xyzx.y;
    u_xlat16_9.x = exp2(u_xlat16_9.x);
    u_xlat16_38 = fma(u_xlat16_9.x, FGlobals._GlossinessInt.xyzx.z, u_xlat16_38);
    u_xlat16_9.xyz = u_xlat16_5.xyz * FGlobals._LightColor0.xyz;
    u_xlat16_0.xyz = u_xlat16_0.xxx * FGlobals._LightColor0.xyz;
    u_xlat16_0.xyz = fma(u_xlat16_9.xyz, half3(u_xlat16_38), u_xlat16_0.xyz);
    u_xlat16_9.xyz = fma(u_xlat16_5.xyz, input.TEXCOORD5.xyz, u_xlat16_0.xyz);
    output.SV_Target0.w = u_xlat16_5.w;
    output.SV_Target0.xyz = u_xlat16_8.xyz + u_xlat16_9.xyz;
    return output;
}
                              FGlobals�        _Time                            _WorldSpaceCameraPos                        _ZBufferParams                           _WorldSpaceLightPos0                 �      _LightColor0                 �      _DepthAwareOffset                     �   	   _LevelPos                     �      _Size                     �      _Center                   �      _Turbulence                   �      _SmokeColor                        _SmokeAtten                        _SmokeRaySteps                        _SmokeSpeed                        _GlossinessInt                        _Scale                          _UpperLimit                   p     _LowerLimit                   t     _EmissionColor                   x     unity_CameraProjection                   0      unity_CameraToWorld                  p      _Rot                 0            _CameraDepthTexture                	   _NoiseTex                   FGlobals           