<Q                         DIRECTIONAL    LIQUID_VOLUME_CYLINDER     LIQUID_VOLUME_DEPTH_AWARE       QI  #ifdef VERTEX
#version 100

uniform 	vec4 _Time;
uniform 	vec4 _ProjectionParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec3 _FlaskThickness;
uniform 	vec4 _Turbulence;
uniform 	float _TurbulenceSpeed;
attribute highp vec4 in_POSITION0;
attribute highp vec3 in_NORMAL0;
varying highp vec3 vs_TEXCOORD0;
varying highp vec3 vs_TEXCOORD1;
varying highp vec4 vs_TEXCOORD2;
varying highp vec3 vs_TEXCOORD3;
varying highp vec4 vs_TEXCOORD4;
varying mediump vec3 vs_TEXCOORD5;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
float u_xlat9;
void main()
{
    u_xlat0.xyz = in_POSITION0.xyz * _FlaskThickness.xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    gl_Position = u_xlat1;
    u_xlat2.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat2.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat2.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat9 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    vs_TEXCOORD0.xyz = vec3(u_xlat9) * u_xlat2.xyz;
    u_xlat2.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat0.xxx + u_xlat2.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    vs_TEXCOORD3.xyz = u_xlat0.xyz;
    u_xlat0.x = u_xlat1.y * _ProjectionParams.x;
    u_xlat0.w = u_xlat0.x * 0.5;
    u_xlat0.xz = u_xlat1.xw * vec2(0.5, 0.5);
    vs_TEXCOORD2.zw = u_xlat1.zw;
    vs_TEXCOORD2.xy = u_xlat0.zz + u_xlat0.xw;
    u_xlat0.x = dot(in_POSITION0.xz, _Turbulence.zw);
    vs_TEXCOORD4.w = u_xlat0.x + _TurbulenceSpeed;
    u_xlat0.x = _Turbulence.x * 0.100000001;
    vs_TEXCOORD4.xz = in_POSITION0.xz * u_xlat0.xx + _Time.xx;
    vs_TEXCOORD4.y = in_POSITION0.y;
    vs_TEXCOORD5.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 100
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif
#if !defined(GL_EXT_shader_texture_lod)
#define texture1DLodEXT texture1D
#define texture2DLodEXT texture2D
#define texture2DProjLodEXT texture2DProj
#define texture3DLodEXT texture3D
#define textureCubeLodEXT textureCube
#endif

#ifdef GL_FRAGMENT_PRECISION_HIGH
    precision highp float;
#else
    precision mediump float;
#endif
precision highp int;
uniform 	vec4 _Time;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ZBufferParams;
uniform 	vec4 hlslcc_mtx4x4unity_CameraProjection[4];
uniform 	vec4 hlslcc_mtx4x4unity_CameraToWorld[4];
uniform 	mediump vec4 _LightColor0;
uniform 	float _DepthAwareOffset;
uniform 	mediump vec4 _Color1;
uniform 	mediump vec4 _Color2;
uniform 	mediump vec4 _FoamColor;
uniform 	float _FoamMaxPos;
uniform 	int _FoamRaySteps;
uniform 	float _FoamDensity;
uniform 	float _FoamBottom;
uniform 	float _FoamTurbulence;
uniform 	float _LevelPos;
uniform 	vec3 _FlaskThickness;
uniform 	vec4 _Size;
uniform 	vec3 _Center;
uniform 	float _Muddy;
uniform 	vec4 _Turbulence;
uniform 	float _DeepAtten;
uniform 	mediump vec4 _SmokeColor;
uniform 	float _SmokeAtten;
uniform 	int _SmokeRaySteps;
uniform 	float _SmokeSpeed;
uniform 	int _LiquidRaySteps;
uniform 	float _FoamWeight;
uniform 	vec4 _Scale;
uniform 	float _UpperLimit;
uniform 	float _LowerLimit;
uniform 	mediump vec3 _EmissionColor;
uniform 	float _SparklingIntensity;
uniform 	float _SparklingThreshold;
uniform highp sampler2D _CameraDepthTexture;
uniform lowp sampler2D _NoiseTex2D;
uniform lowp sampler3D _NoiseTex;
varying highp vec4 vs_TEXCOORD2;
varying highp vec3 vs_TEXCOORD3;
varying highp vec4 vs_TEXCOORD4;
varying mediump vec3 vs_TEXCOORD5;
#define SV_Target0 gl_FragData[0]
vec3 u_xlat0;
mediump vec4 u_xlat16_0;
bool u_xlatb0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
lowp float u_xlat10_1;
vec4 u_xlat2;
int u_xlati2;
bool u_xlatb2;
vec3 u_xlat3;
vec3 u_xlat4;
vec4 u_xlat5;
mediump vec4 u_xlat16_5;
vec3 u_xlat6;
mediump vec4 u_xlat16_6;
mediump vec4 u_xlat16_7;
vec3 u_xlat8;
mediump vec4 u_xlat16_8;
vec3 u_xlat9;
mediump vec4 u_xlat16_9;
mediump vec3 u_xlat16_10;
vec3 u_xlat11;
vec3 u_xlat12;
bool u_xlatb13;
float u_xlat14;
vec3 u_xlat15;
bool u_xlatb15;
float u_xlat16;
int u_xlati16;
bool u_xlatb16;
mediump float u_xlat16_23;
float u_xlat27;
int u_xlati27;
float u_xlat28;
int u_xlati28;
float u_xlat29;
bool u_xlatb29;
bool u_xlatb30;
float u_xlat39;
bool u_xlatb39;
float u_xlat40;
bool u_xlatb40;
float u_xlat41;
int u_xlati41;
float u_xlat42;
lowp float u_xlat10_42;
bool u_xlatb42;
float u_xlat43;
void main()
{
    u_xlatb0 = _UpperLimit<vs_TEXCOORD4.y;
    u_xlatb13 = vs_TEXCOORD4.y<_LowerLimit;
    u_xlatb0 = u_xlatb13 || u_xlatb0;
    if(!u_xlatb0){
        u_xlat0.xyz = vs_TEXCOORD3.xyz + (-_WorldSpaceCameraPos.xyz);
        u_xlat39 = dot(u_xlat0.xyz, u_xlat0.xyz);
        u_xlat39 = sqrt(u_xlat39);
        u_xlat0.xyz = u_xlat0.xyz / vec3(u_xlat39);
        u_xlat1.xyz = _WorldSpaceCameraPos.xyz + (-_Center.xyz);
        u_xlat39 = dot(u_xlat0.xz, u_xlat0.xz);
        u_xlat40 = dot(u_xlat0.xz, u_xlat1.xz);
        u_xlat1.x = dot(u_xlat1.xz, u_xlat1.xz);
        u_xlat1.x = (-_Size.w) * _Size.w + u_xlat1.x;
        u_xlat1.x = u_xlat39 * u_xlat1.x;
        u_xlat1.x = u_xlat40 * u_xlat40 + (-u_xlat1.x);
        u_xlat1.x = max(u_xlat1.x, 0.0);
        u_xlat1.x = sqrt(u_xlat1.x);
        u_xlat27 = (-u_xlat1.x) + (-u_xlat40);
        u_xlat27 = u_xlat27 / u_xlat39;
        u_xlat1.x = u_xlat1.x + (-u_xlat40);
        u_xlat39 = u_xlat1.x / u_xlat39;
        u_xlat1.x = _FlaskThickness.y * _Size.y;
        u_xlat40 = u_xlat1.x * 0.5;
        u_xlat1.x = (-u_xlat1.x) * 0.5 + abs(u_xlat1.y);
        u_xlatb2 = 0.0<u_xlat1.x;
        u_xlat15.xy = u_xlat0.xz / u_xlat0.yy;
        u_xlat15.x = dot(u_xlat15.xy, u_xlat15.xy);
        u_xlat15.x = u_xlat15.x + 1.0;
        u_xlat15.x = sqrt(u_xlat15.x);
        u_xlat1.x = u_xlat1.x * u_xlat15.x;
        u_xlat1.x = max(u_xlat1.x, u_xlat27);
        u_xlat1.x = (u_xlatb2) ? u_xlat1.x : u_xlat27;
        u_xlati27 = int((0.0<u_xlat0.y) ? -1 : 0);
        u_xlati2 = int((u_xlat0.y<0.0) ? -1 : 0);
        u_xlati27 = (-u_xlati27) + u_xlati2;
        u_xlat27 = float(u_xlati27);
        u_xlat14 = u_xlat27 * (-u_xlat1.y) + u_xlat40;
        u_xlatb40 = 0.0<u_xlat14;
        u_xlat14 = u_xlat15.x * u_xlat14;
        u_xlat14 = min(u_xlat39, u_xlat14);
        u_xlat39 = (u_xlatb40) ? u_xlat14 : u_xlat39;
        u_xlat3.x = max(u_xlat1.x, 0.0);
        u_xlat1.xy = vs_TEXCOORD2.xy / vs_TEXCOORD2.ww;
        u_xlat40 = texture2D(_CameraDepthTexture, u_xlat1.xy).x;
        u_xlat40 = _ZBufferParams.z * u_xlat40 + _ZBufferParams.w;
        u_xlat40 = float(1.0) / u_xlat40;
        u_xlat1.xy = u_xlat1.xy * vec2(2.0, 2.0) + vec2(-1.0, -1.0);
        u_xlat4.x = u_xlat1.x / hlslcc_mtx4x4unity_CameraProjection[0].x;
        u_xlat4.y = u_xlat1.y / hlslcc_mtx4x4unity_CameraProjection[1].y;
        u_xlat4.z = -1.0;
        u_xlat1.xyw = vec3(u_xlat40) * u_xlat4.xyz;
        u_xlat15.xyz = u_xlat1.yyy * hlslcc_mtx4x4unity_CameraToWorld[1].xyz;
        u_xlat15.xyz = hlslcc_mtx4x4unity_CameraToWorld[0].xyz * u_xlat1.xxx + u_xlat15.xyz;
        u_xlat1.xyw = hlslcc_mtx4x4unity_CameraToWorld[2].xyz * u_xlat1.www + u_xlat15.xyz;
        u_xlat1.xyw = u_xlat1.xyw + hlslcc_mtx4x4unity_CameraToWorld[3].xyz;
        u_xlat1.xyw = (-u_xlat1.xyw) + _WorldSpaceCameraPos.xyz;
        u_xlat1.x = dot(u_xlat1.xyw, u_xlat1.xyw);
        u_xlat1.x = sqrt(u_xlat1.x);
        u_xlat1.x = u_xlat1.x + _DepthAwareOffset;
        u_xlat3.z = min(u_xlat39, u_xlat1.x);
        u_xlat39 = u_xlat0.y * u_xlat3.x + _WorldSpaceCameraPos.y;
        u_xlat10_1 = texture2D(_NoiseTex2D, vs_TEXCOORD4.xz).y;
        u_xlat16_1.x = u_xlat10_1 + -0.5;
        u_xlat14 = sin(vs_TEXCOORD4.w);
        u_xlat14 = u_xlat14 * _Turbulence.y;
        u_xlat1.x = u_xlat16_1.x * _Turbulence.x + u_xlat14;
        u_xlat14 = _FoamTurbulence * _Size.y;
        u_xlat1.x = u_xlat1.x * u_xlat14;
        u_xlat14 = u_xlat1.x * 0.0500000007 + _LevelPos;
        u_xlat1.x = u_xlat1.x * 0.0500000007 + _FoamMaxPos;
        u_xlat40 = u_xlat14 + (-_WorldSpaceCameraPos.y);
        u_xlat40 = u_xlat40 / u_xlat0.y;
        u_xlat40 = max(u_xlat3.x, u_xlat40);
        u_xlat4.x = min(u_xlat3.z, u_xlat40);
        u_xlat40 = u_xlat1.x + (-_WorldSpaceCameraPos.y);
        u_xlat40 = u_xlat40 / u_xlat0.y;
        u_xlat40 = max(u_xlat3.x, u_xlat40);
        u_xlat40 = min(u_xlat3.z, u_xlat40);
        u_xlatb15 = u_xlat14<u_xlat39;
        if(u_xlatb15){
            u_xlat28 = min(u_xlat3.z, u_xlat4.x);
            u_xlat28 = (u_xlati2 != 0) ? u_xlat28 : u_xlat3.z;
            u_xlat28 = (-u_xlat3.x) + u_xlat28;
            u_xlat41 = float(_SmokeRaySteps);
            u_xlat28 = u_xlat28 / u_xlat41;
            u_xlat5.xyz = u_xlat0.xyz * u_xlat3.xxx + _WorldSpaceCameraPos.xyz;
            u_xlat41 = _Time.x * _Turbulence.x;
            u_xlat41 = u_xlat41 * _Size.y;
            u_xlat6.y = u_xlat41 * _SmokeSpeed;
            u_xlat6.x = float(0.0);
            u_xlat6.z = float(0.0);
            u_xlat16_7.x = float(0.0);
            u_xlat16_7.y = float(0.0);
            u_xlat16_7.z = float(0.0);
            u_xlat16_7.w = float(0.0);
            u_xlat8.xyz = u_xlat5.xyz;
            u_xlati41 = _SmokeRaySteps;
            for(int u_xlati_while_true_0 = 0 ; u_xlati_while_true_0 < 0x7FFF ; u_xlati_while_true_0++){
                u_xlatb42 = 0>=u_xlati41;
                if(u_xlatb42){break;}
                u_xlat9.xyz = (-u_xlat6.xyz) + u_xlat8.xyz;
                u_xlat9.xyz = u_xlat9.xyz * _Scale.xxx;
                u_xlat10_42 = texture3DLodEXT(_NoiseTex, u_xlat9.xyz, 0.0).x;
                u_xlat16_9.w = u_xlat10_42 * _SmokeColor.w;
                u_xlat16_9.xyz = u_xlat16_9.www * _SmokeColor.xyz;
                u_xlat42 = u_xlat14 + (-u_xlat8.y);
                u_xlat42 = u_xlat42 / _Size.y;
                u_xlat42 = u_xlat42 * _SmokeAtten;
                u_xlat42 = u_xlat42 * 1.44269502;
                u_xlat42 = exp2(u_xlat42);
                u_xlat16_9 = vec4(u_xlat42) * u_xlat16_9;
                u_xlat16_10.x = (-u_xlat16_7.w) + 1.0;
                u_xlat16_7 = u_xlat16_9 * u_xlat16_10.xxxx + u_xlat16_7;
                u_xlati41 = u_xlati41 + -1;
                u_xlat8.xyz = u_xlat0.xyz * vec3(u_xlat28) + u_xlat8.xyz;
            }
        } else {
            u_xlat16_7.x = float(0.0);
            u_xlat16_7.y = float(0.0);
            u_xlat16_7.z = float(0.0);
            u_xlat16_7.w = float(0.0);
        }
        u_xlat5.x = min(u_xlat3.z, u_xlat40);
        u_xlatb40 = u_xlat1.x<u_xlat39;
        u_xlat6.x = min(u_xlat3.z, u_xlat4.x);
        u_xlat5.y = (-u_xlat27) * u_xlat6.x;
        u_xlatb39 = u_xlat39<u_xlat14;
        u_xlat28 = u_xlat27 * _FoamBottom;
        u_xlat6.y = u_xlat28 * u_xlat5.x;
        u_xlat3.y = (u_xlati2 != 0) ? u_xlat6.x : u_xlat5.x;
        u_xlat2.xz = (bool(u_xlatb39)) ? u_xlat6.xy : u_xlat3.xy;
        u_xlat2.xz = (bool(u_xlatb40)) ? u_xlat5.xy : u_xlat2.xz;
        u_xlatb39 = u_xlat2.x<u_xlat2.z;
        if(u_xlatb39){
            u_xlat39 = (-u_xlat2.x) + u_xlat2.z;
            u_xlat40 = float(_FoamRaySteps);
            u_xlat39 = u_xlat39 / u_xlat40;
            u_xlat2.xzw = u_xlat0.xyz * u_xlat2.xxx + _WorldSpaceCameraPos.xyz;
            u_xlat40 = (-u_xlat14) + u_xlat2.z;
            u_xlat1.x = (-u_xlat14) + u_xlat1.x;
            u_xlat5.xz = _Time.xx;
            u_xlat5.y = _Size.w;
            u_xlat5.xyz = u_xlat5.xyz * _Turbulence.xxx;
            u_xlat6.xz = _Size.ww;
            u_xlat6.y = _FoamTurbulence;
            u_xlat5.xyz = u_xlat5.xyz * u_xlat6.xyz;
            u_xlat6.xz = vec2(_FoamTurbulence);
            u_xlat6.y = 0.0;
            u_xlat16_8.x = float(0.0);
            u_xlat16_8.y = float(0.0);
            u_xlat16_8.z = float(0.0);
            u_xlat16_8.w = float(0.0);
            u_xlat11.xz = u_xlat2.xw;
            u_xlat11.y = u_xlat40;
            u_xlati28 = _FoamRaySteps;
            for(int u_xlati_while_true_1 = 0 ; u_xlati_while_true_1 < 0x7FFF ; u_xlati_while_true_1++){
                u_xlatb16 = 0>=u_xlati28;
                if(u_xlatb16){break;}
                u_xlat16 = u_xlat11.y / u_xlat1.x;
                u_xlat16 = clamp(u_xlat16, 0.0, 1.0);
                u_xlat12.xyz = (-u_xlat5.xyz) * u_xlat6.xyz + u_xlat11.xyz;
                u_xlat12.xyz = u_xlat12.xyz * _Scale.yyy;
                u_xlat10_42 = texture3DLodEXT(_NoiseTex, u_xlat12.xyz, 0.0).x;
                u_xlat42 = u_xlat10_42 + _FoamDensity;
                u_xlat42 = clamp(u_xlat42, 0.0, 1.0);
                u_xlatb30 = u_xlat16<u_xlat42;
                u_xlat16 = (-u_xlat16) + u_xlat42;
                u_xlat16_9.w = u_xlat16 * _FoamColor.w;
                u_xlat16_9.xyz = u_xlat16_9.www * _FoamColor.xyz;
                u_xlat16 = u_xlat11.y * _FoamWeight;
                u_xlat16 = u_xlat16 / u_xlat1.x;
                u_xlat16 = clamp(u_xlat16, 0.0, 1.0);
                u_xlat16_9 = vec4(u_xlat16) * u_xlat16_9;
                u_xlat16_10.x = (-u_xlat16_8.w) + 1.0;
                u_xlat16_9 = u_xlat16_9 * u_xlat16_10.xxxx + u_xlat16_8;
                u_xlat16_8 = (bool(u_xlatb30)) ? u_xlat16_9 : u_xlat16_8;
                u_xlati28 = u_xlati28 + -1;
                u_xlat11.xyz = u_xlat0.xyz * vec3(u_xlat39) + u_xlat11.xyz;
            }
            u_xlat39 = _FoamDensity + 1.0;
            u_xlat5 = vec4(u_xlat39) * u_xlat16_8;
            u_xlat16_5 = u_xlat5;
        } else {
            u_xlat16_5.x = float(0.0);
            u_xlat16_5.y = float(0.0);
            u_xlat16_5.z = float(0.0);
            u_xlat16_5.w = float(0.0);
        }
        u_xlat4.y = (-u_xlat27) * u_xlat3.z;
        u_xlat1.xz = (bool(u_xlatb15)) ? u_xlat4.xy : u_xlat3.xz;
        u_xlatb39 = u_xlat1.x<u_xlat1.z;
        if(u_xlatb39){
            u_xlat39 = (-u_xlat1.x) + u_xlat1.z;
            u_xlat27 = float(_LiquidRaySteps);
            u_xlat39 = u_xlat39 / u_xlat27;
            u_xlat1.xzw = u_xlat0.xyz * u_xlat1.xxx + _WorldSpaceCameraPos.xyz;
            u_xlat1.y = (-u_xlat14) + u_xlat1.z;
            u_xlat3.xz = _Time.xx * _Turbulence.yy;
            u_xlat27 = _Turbulence.x + _Turbulence.y;
            u_xlat3.y = _Time.x * 1.5;
            u_xlat2.xzw = vec3(u_xlat27) * u_xlat3.xyz;
            u_xlat3.x = _Size.y;
            u_xlat27 = u_xlat27 * _Time.x;
            u_xlat27 = u_xlat27 * _Size.y;
            u_xlat4.y = u_xlat27 * 2.5;
            u_xlat27 = (-_Muddy) + 1.0;
            u_xlat4.x = float(0.0);
            u_xlat4.z = float(0.0);
            u_xlat16_6.x = float(0.0);
            u_xlat16_6.y = float(0.0);
            u_xlat16_6.z = float(0.0);
            u_xlat16_6.w = float(0.0);
            u_xlat11.xyz = u_xlat1.xyw;
            u_xlati16 = _LiquidRaySteps;
            for(int u_xlati_while_true_2 = 0 ; u_xlati_while_true_2 < 0x7FFF ; u_xlati_while_true_2++){
                u_xlatb29 = 0>=u_xlati16;
                if(u_xlatb29){break;}
                u_xlat29 = u_xlat11.y / _Size.y;
                u_xlat29 = u_xlat29 * _DeepAtten;
                u_xlat29 = u_xlat29 * 1.44269502;
                u_xlat29 = exp2(u_xlat29);
                u_xlat12.xyz = (-u_xlat2.xzw) * u_xlat3.xxx + u_xlat11.xyz;
                u_xlat12.xyz = u_xlat12.xyz * _Scale.zzz;
                u_xlat10_42 = texture3DLodEXT(_NoiseTex, u_xlat12.xyz, 0.0).x;
                u_xlat42 = u_xlat10_42 * _Muddy + u_xlat27;
                u_xlat16_8.w = u_xlat42 * _Color1.w;
                u_xlat16_10.xyz = u_xlat16_8.www * _Color1.xyz;
                u_xlat16_8.xyz = vec3(u_xlat29) * u_xlat16_10.xyz;
                u_xlat16_10.x = (-u_xlat16_6.w) + 1.0;
                u_xlat16_8 = u_xlat16_8 * u_xlat16_10.xxxx + u_xlat16_6;
                u_xlat12.xyz = (-u_xlat4.xyz) + u_xlat11.xyz;
                u_xlat12.xyz = u_xlat12.xyz * _Scale.www;
                u_xlat10_42 = texture3DLodEXT(_NoiseTex, u_xlat12.xyz, 0.0).x;
                u_xlat43 = u_xlat10_42 + (-_SparklingThreshold);
                u_xlat43 = max(u_xlat43, 0.0);
                u_xlat12.xyz = vec3(u_xlat43) * vec3(vec3(_SparklingIntensity, _SparklingIntensity, _SparklingIntensity)) + _Color2.xyz;
                u_xlat42 = u_xlat10_42 * _Muddy + u_xlat27;
                u_xlat16_9.w = u_xlat42 * _Color2.w;
                u_xlat16_10.xyz = u_xlat16_9.www * u_xlat12.xyz;
                u_xlat16_9.xyz = vec3(u_xlat29) * u_xlat16_10.xyz;
                u_xlat16_10.x = (-u_xlat16_8.w) + 1.0;
                u_xlat16_6 = u_xlat16_9 * u_xlat16_10.xxxx + u_xlat16_8;
                u_xlati16 = u_xlati16 + -1;
                u_xlat11.xyz = u_xlat0.xyz * vec3(u_xlat39) + u_xlat11.xyz;
            }
        } else {
            u_xlat16_6.x = float(0.0);
            u_xlat16_6.y = float(0.0);
            u_xlat16_6.z = float(0.0);
            u_xlat16_6.w = float(0.0);
        }
        u_xlat16_10.x = (-u_xlat16_7.w) + 1.0;
        u_xlat16_23 = (-u_xlat16_5.w) * u_xlat16_10.x + 1.0;
        u_xlat16_0 = u_xlat16_6 * vec4(u_xlat16_23);
        u_xlat16_1 = u_xlat16_5 * u_xlat16_10.xxxx + u_xlat16_7;
        u_xlat16_0 = u_xlat16_0 * u_xlat16_10.xxxx + u_xlat16_1;
        u_xlat16_7.x = (-u_xlat16_6.w) + 1.0;
        u_xlat16_1 = u_xlat16_5 * u_xlat16_7.xxxx + u_xlat16_6;
        u_xlat16_0 = (bool(u_xlatb15)) ? u_xlat16_0 : u_xlat16_1;
        u_xlat16_7.xyz = u_xlat16_0.xyz * _EmissionColor.xyz;
    } else {
        u_xlat16_0.x = float(0.0);
        u_xlat16_0.y = float(0.0);
        u_xlat16_0.z = float(0.0);
        u_xlat16_0.w = float(0.0);
        u_xlat16_7.x = float(0.0);
        u_xlat16_7.y = float(0.0);
        u_xlat16_7.z = float(0.0);
    }
    u_xlat16_1.xyz = u_xlat16_0.xyz * vs_TEXCOORD5.xyz;
    u_xlat16_0.xyz = u_xlat16_0.xyz * _LightColor0.xyz;
    u_xlat16_1.w = 0.0;
    u_xlat16_0 = u_xlat16_0 + u_xlat16_1;
    SV_Target0.xyz = u_xlat16_7.xyz + u_xlat16_0.xyz;
    SV_Target0.w = u_xlat16_0.w;
    return;
}

#endif
                                 