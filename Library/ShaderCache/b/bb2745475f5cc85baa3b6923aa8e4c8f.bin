<Q                         DIRECTIONAL    LIQUID_VOLUME_DEPTH_AWARE      LIQUID_VOLUME_IRREGULAR    LIQUID_VOLUME_NON_AABB      *  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 _Time;
    float3 _WorldSpaceCameraPos;
    float4 _ZBufferParams;
    float4 hlslcc_mtx4x4unity_CameraProjection[4];
    float4 hlslcc_mtx4x4unity_CameraToWorld[4];
    half4 _WorldSpaceLightPos0;
    half4 _LightColor0;
    float _DepthAwareOffset;
    float _LevelPos;
    float4 _Size;
    float4 _Turbulence;
    half4 _SmokeColor;
    float _SmokeAtten;
    int _SmokeRaySteps;
    float _SmokeSpeed;
    half3 _GlossinessInt;
    float4 _Scale;
    float _UpperLimit;
    float _LowerLimit;
    half3 _EmissionColor;
    float _DoubleSidedBias;
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float4 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    half3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_CameraDepthTexture [[ sampler (0) ]],
    sampler sampler_VLBackBufferTexture [[ sampler (1) ]],
    sampler sampler_NoiseTex [[ sampler (2) ]],
    texture2d<float, access::sample > _CameraDepthTexture [[ texture(0) ]] ,
    texture2d<half, access::sample > _VLBackBufferTexture [[ texture(1) ]] ,
    texture3d<half, access::sample > _NoiseTex [[ texture(2) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    half3 u_xlat16_0;
    float3 u_xlat1;
    float3 u_xlat2;
    bool u_xlatb2;
    float3 u_xlat3;
    half u_xlat16_3;
    float4 u_xlat4;
    half4 u_xlat16_4;
    float3 u_xlat5;
    float3 u_xlat6;
    half4 u_xlat16_6;
    half3 u_xlat16_7;
    half3 u_xlat16_8;
    float u_xlat21;
    float u_xlat27;
    float u_xlat28;
    bool u_xlatb28;
    float u_xlat29;
    int u_xlati29;
    bool u_xlatb29;
    float u_xlat30;
    bool u_xlatb30;
    half u_xlat16_32;
    half u_xlat16_34;
    u_xlat0.xyz = (-input.TEXCOORD1.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat27 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat27 = rsqrt(u_xlat27);
    u_xlat1.xyz = float3(u_xlat27) * u_xlat0.xyz;
    u_xlatb28 = FGlobals._UpperLimit<input.TEXCOORD4.y;
    u_xlatb2 = input.TEXCOORD4.y<FGlobals._LowerLimit;
    u_xlatb28 = u_xlatb28 || u_xlatb2;
    if(!u_xlatb28){
        u_xlat2.xyz = input.TEXCOORD3.xyz + (-FGlobals._WorldSpaceCameraPos.xyzx.xyz);
        u_xlat28 = dot(u_xlat2.xyz, u_xlat2.xyz);
        u_xlat28 = sqrt(u_xlat28);
        u_xlat2.xyz = u_xlat2.xyz / float3(u_xlat28);
        u_xlat29 = u_xlat28 + FGlobals._Size.x;
        u_xlat3.xy = input.TEXCOORD2.xy / input.TEXCOORD2.ww;
        u_xlat21 = _CameraDepthTexture.sample(sampler_CameraDepthTexture, u_xlat3.xy).x;
        u_xlat21 = fma(FGlobals._ZBufferParams.z, u_xlat21, FGlobals._ZBufferParams.w);
        u_xlat21 = float(1.0) / u_xlat21;
        u_xlat4.xy = fma(u_xlat3.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
        u_xlat5.x = u_xlat4.x / FGlobals.hlslcc_mtx4x4unity_CameraProjection[0].x;
        u_xlat5.y = u_xlat4.y / FGlobals.hlslcc_mtx4x4unity_CameraProjection[1].y;
        u_xlat5.z = -1.0;
        u_xlat4.xyz = float3(u_xlat21) * u_xlat5.xyz;
        u_xlat5.xyz = u_xlat4.yyy * FGlobals.hlslcc_mtx4x4unity_CameraToWorld[1].xyz;
        u_xlat4.xyw = fma(FGlobals.hlslcc_mtx4x4unity_CameraToWorld[0].xyz, u_xlat4.xxx, u_xlat5.xyz);
        u_xlat4.xyz = fma(FGlobals.hlslcc_mtx4x4unity_CameraToWorld[2].xyz, u_xlat4.zzz, u_xlat4.xyw);
        u_xlat4.xyz = u_xlat4.xyz + FGlobals.hlslcc_mtx4x4unity_CameraToWorld[3].xyz;
        u_xlat4.xyz = (-u_xlat4.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
        u_xlat21 = dot(u_xlat4.xyz, u_xlat4.xyz);
        u_xlat21 = sqrt(u_xlat21);
        u_xlat21 = u_xlat21 + FGlobals._DepthAwareOffset;
        u_xlat29 = min(u_xlat29, u_xlat21);
        u_xlat16_3 = _VLBackBufferTexture.sample(sampler_VLBackBufferTexture, u_xlat3.xy).x;
        u_xlat29 = min(u_xlat29, float(u_xlat16_3));
        u_xlat28 = (-u_xlat28) + u_xlat29;
        u_xlat29 = u_xlat28 + (-FGlobals._DoubleSidedBias);
        u_xlatb29 = u_xlat29<0.0;
        if(((int(u_xlatb29) * int(0xffffffffu)))!=0){discard_fragment();}
        u_xlat29 = float(FGlobals._SmokeRaySteps);
        u_xlat28 = u_xlat28 / u_xlat29;
        u_xlat29 = FGlobals._Time.x * FGlobals._Turbulence.x;
        u_xlat29 = u_xlat29 * FGlobals._Size.y;
        u_xlat3.y = u_xlat29 * FGlobals._SmokeSpeed;
        u_xlat3.x = float(0.0);
        u_xlat3.z = float(0.0);
        u_xlat16_4.x = half(0.0);
        u_xlat16_4.y = half(0.0);
        u_xlat16_4.z = half(0.0);
        u_xlat16_4.w = half(0.0);
        u_xlat5.xyz = input.TEXCOORD3.xyz;
        u_xlati29 = FGlobals._SmokeRaySteps;
        while(true){
            u_xlatb30 = 0x0>=u_xlati29;
            if(u_xlatb30){break;}
            u_xlat30 = (-u_xlat5.y) + FGlobals._LevelPos;
            u_xlat30 = u_xlat30 / FGlobals._Size.y;
            u_xlat30 = u_xlat30 * FGlobals._SmokeAtten;
            u_xlat30 = u_xlat30 * 1.44269502;
            u_xlat30 = exp2(u_xlat30);
            u_xlat6.xyz = (-u_xlat3.xyz) + u_xlat5.xyz;
            u_xlat6.xyz = u_xlat6.xyz * FGlobals._Scale.xxx;
            u_xlat16_32 = _NoiseTex.sample(sampler_NoiseTex, u_xlat6.xyz, level(0.0)).x;
            u_xlat16_6.w = u_xlat16_32 * FGlobals._SmokeColor.w;
            u_xlat16_6.xyz = u_xlat16_6.www * FGlobals._SmokeColor.xyz;
            u_xlat16_6 = half4(float4(u_xlat30) * float4(u_xlat16_6));
            u_xlat16_7.x = (-u_xlat16_4.w) + half(1.0);
            u_xlat16_4 = fma(u_xlat16_6, u_xlat16_7.xxxx, u_xlat16_4);
            u_xlati29 = u_xlati29 + int(0xffffffffu);
            u_xlat5.xyz = fma(u_xlat2.xyz, float3(u_xlat28), u_xlat5.xyz);
        }
        u_xlat16_7.xyz = u_xlat16_4.xyz * FGlobals._EmissionColor.xyzx.xyz;
    } else {
        u_xlat16_7.x = half(0.0);
        u_xlat16_7.y = half(0.0);
        u_xlat16_7.z = half(0.0);
        u_xlat16_4.x = half(0.0);
        u_xlat16_4.y = half(0.0);
        u_xlat16_4.z = half(0.0);
        u_xlat16_4.w = half(0.0);
    }
    u_xlat16_8.xyz = half3(fma(u_xlat0.xyz, float3(u_xlat27), float3(FGlobals._WorldSpaceLightPos0.xyz)));
    u_xlat16_34 = dot(u_xlat16_8.xyz, u_xlat16_8.xyz);
    u_xlat16_34 = rsqrt(u_xlat16_34);
    u_xlat16_8.xyz = half3(u_xlat16_34) * u_xlat16_8.xyz;
    u_xlat16_34 = dot(input.TEXCOORD0.xyz, float3(FGlobals._WorldSpaceLightPos0.xyz));
    u_xlat16_34 = fma(u_xlat16_34, half(0.5), half(0.5));
    u_xlat16_34 = max(u_xlat16_34, half(0.0));
    u_xlat16_8.x = dot(input.TEXCOORD0.xyz, float3(u_xlat16_8.xyz));
    u_xlat16_8.x = max(u_xlat16_8.x, half(0.0));
    u_xlat16_0.x = log2(u_xlat16_8.x);
    u_xlat16_0.x = u_xlat16_0.x * FGlobals._GlossinessInt.xyzx.x;
    u_xlat16_0.x = exp2(u_xlat16_0.x);
    u_xlat16_8.x = dot(u_xlat1.xyz, (-float3(FGlobals._WorldSpaceLightPos0.xyz)));
    u_xlat16_8.x = max(u_xlat16_8.x, half(0.0));
    u_xlat16_8.x = log2(u_xlat16_8.x);
    u_xlat16_8.x = u_xlat16_8.x * FGlobals._GlossinessInt.xyzx.y;
    u_xlat16_8.x = exp2(u_xlat16_8.x);
    u_xlat16_34 = fma(u_xlat16_8.x, FGlobals._GlossinessInt.xyzx.z, u_xlat16_34);
    u_xlat16_8.xyz = u_xlat16_4.xyz * FGlobals._LightColor0.xyz;
    u_xlat16_0.xyz = u_xlat16_0.xxx * FGlobals._LightColor0.xyz;
    u_xlat16_0.xyz = fma(u_xlat16_8.xyz, half3(u_xlat16_34), u_xlat16_0.xyz);
    u_xlat16_8.xyz = fma(u_xlat16_4.xyz, input.TEXCOORD5.xyz, u_xlat16_0.xyz);
    output.SV_Target0.w = u_xlat16_4.w;
    output.SV_Target0.xyz = u_xlat16_7.xyz + u_xlat16_8.xyz;
    return output;
}
                                FGlobals4        _Time                            _WorldSpaceCameraPos                        _ZBufferParams                           _WorldSpaceLightPos0                 �      _LightColor0                 �      _DepthAwareOffset                     �   	   _LevelPos                     �      _Size                     �      _Turbulence                   �      _SmokeColor                  �      _SmokeAtten                   �      _SmokeRaySteps                   �      _SmokeSpeed                         _GlossinessInt                        _Scale                         _UpperLimit                         _LowerLimit                   $     _EmissionColor                   (     _DoubleSidedBias                  0     unity_CameraProjection                   0      unity_CameraToWorld                  p             _CameraDepthTexture                   _VLBackBufferTexture             	   _NoiseTex                   FGlobals           