<Q                         DIRECTIONAL    LIGHTPROBE_SH      LIQUID_VOLUME_CUBE     LIQUID_VOLUME_IGNORE_GRAVITY    bB  #ifdef VERTEX
#version 100

uniform 	vec4 _Time;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec3 _FlaskThickness;
uniform 	vec3 _Center;
uniform 	vec4 _Turbulence;
uniform 	vec4 hlslcc_mtx4x4_Rot[4];
uniform 	float _TurbulenceSpeed;
attribute highp vec4 in_POSITION0;
attribute highp vec3 in_NORMAL0;
varying highp vec3 vs_TEXCOORD0;
varying highp vec3 vs_TEXCOORD1;
varying highp vec3 vs_TEXCOORD2;
varying highp vec4 vs_TEXCOORD3;
varying mediump vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
float u_xlat9;
void main()
{
    u_xlat0.xyz = in_POSITION0.xyz * _FlaskThickness.xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat9 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    vs_TEXCOORD0.xyz = vec3(u_xlat9) * u_xlat1.xyz;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + (-_Center.xyz);
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4_Rot[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4_Rot[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4_Rot[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    vs_TEXCOORD2.xyz = u_xlat0.xyz + _Center.xyz;
    u_xlat0.x = dot(in_POSITION0.xz, _Turbulence.zw);
    vs_TEXCOORD3.w = u_xlat0.x + _TurbulenceSpeed;
    u_xlat0.x = _Turbulence.x * 0.100000001;
    vs_TEXCOORD3.xz = in_POSITION0.xz * u_xlat0.xx + _Time.xx;
    vs_TEXCOORD3.y = in_POSITION0.y;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 100
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif
#if !defined(GL_EXT_shader_texture_lod)
#define texture1DLodEXT texture1D
#define texture2DLodEXT texture2D
#define texture2DProjLodEXT texture2DProj
#define texture3DLodEXT texture3D
#define textureCubeLodEXT textureCube
#endif

#ifdef GL_FRAGMENT_PRECISION_HIGH
    precision highp float;
#else
    precision mediump float;
#endif
precision highp int;
uniform 	vec4 _Time;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump vec4 _Color1;
uniform 	mediump vec4 _Color2;
uniform 	mediump vec4 _FoamColor;
uniform 	float _FoamMaxPos;
uniform 	int _FoamRaySteps;
uniform 	float _FoamDensity;
uniform 	float _FoamBottom;
uniform 	float _FoamTurbulence;
uniform 	float _LevelPos;
uniform 	vec4 _Size;
uniform 	vec3 _Center;
uniform 	float _Muddy;
uniform 	vec4 _Turbulence;
uniform 	float _DeepAtten;
uniform 	mediump vec4 _SmokeColor;
uniform 	float _SmokeAtten;
uniform 	int _SmokeRaySteps;
uniform 	float _SmokeSpeed;
uniform 	int _LiquidRaySteps;
uniform 	float _FoamWeight;
uniform 	vec4 _Scale;
uniform 	vec4 hlslcc_mtx4x4_Rot[4];
uniform 	float _UpperLimit;
uniform 	float _LowerLimit;
uniform 	mediump vec3 _EmissionColor;
uniform 	float _SparklingIntensity;
uniform 	float _SparklingThreshold;
uniform lowp sampler2D _NoiseTex2D;
uniform lowp sampler3D _NoiseTex;
varying highp vec3 vs_TEXCOORD2;
varying highp vec4 vs_TEXCOORD3;
varying mediump vec3 vs_TEXCOORD4;
#define SV_Target0 gl_FragData[0]
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
bool u_xlatb0;
vec3 u_xlat1;
mediump vec4 u_xlat16_1;
vec3 u_xlat2;
vec3 u_xlat3;
vec4 u_xlat4;
mediump vec4 u_xlat16_4;
bool u_xlatb4;
vec3 u_xlat5;
mediump vec4 u_xlat16_5;
mediump vec4 u_xlat16_6;
vec3 u_xlat7;
vec3 u_xlat8;
mediump vec4 u_xlat16_8;
mediump vec4 u_xlat16_9;
vec3 u_xlat10;
bool u_xlatb10;
vec3 u_xlat11;
mediump vec3 u_xlat16_12;
vec3 u_xlat13;
bool u_xlatb13;
vec3 u_xlat17;
int u_xlati17;
mediump float u_xlat16_22;
vec3 u_xlat23;
lowp float u_xlat10_23;
float u_xlat26;
bool u_xlatb26;
float u_xlat29;
int u_xlati29;
int u_xlati30;
float u_xlat31;
float u_xlat36;
float u_xlat39;
bool u_xlatb39;
float u_xlat40;
mediump float u_xlat16_40;
lowp float u_xlat10_40;
float u_xlat41;
bool u_xlatb42;
float u_xlat43;
bool u_xlatb43;
float u_xlat44;
lowp float u_xlat10_44;
int u_xlati44;
float u_xlat46;
lowp float u_xlat10_46;
int u_xlati46;
bool u_xlatb46;
void main()
{
    u_xlatb0 = _UpperLimit<vs_TEXCOORD3.y;
    u_xlatb13 = vs_TEXCOORD3.y<_LowerLimit;
    u_xlatb0 = u_xlatb13 || u_xlatb0;
    if(!u_xlatb0){
        u_xlat0.xyz = _WorldSpaceCameraPos.xyz + (-_Center.xyz);
        u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4_Rot[1].xyz;
        u_xlat0.xyw = hlslcc_mtx4x4_Rot[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
        u_xlat0.xyz = hlslcc_mtx4x4_Rot[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
        u_xlat1.xyz = u_xlat0.xyz + _Center.xyz;
        u_xlat2.xyz = (-u_xlat1.xyz) + vs_TEXCOORD2.xyz;
        u_xlat39 = dot(u_xlat2.xyz, u_xlat2.xyz);
        u_xlat39 = sqrt(u_xlat39);
        u_xlat2.xyz = u_xlat2.xyz / vec3(u_xlat39);
        u_xlat3.xyz = vec3(1.0, 1.0, 1.0) / u_xlat2.xyz;
        u_xlat4.xyz = (-u_xlat0.xyz) + (-_Size.www);
        u_xlat4.xyz = u_xlat3.xyz * u_xlat4.xyz;
        u_xlat0.xyz = (-u_xlat0.xyz) + _Size.www;
        u_xlat0.xyz = u_xlat0.xyz * u_xlat3.xyz;
        u_xlat3.xyz = min(u_xlat4.xyz, u_xlat0.xyz);
        u_xlat0.xyz = max(u_xlat4.xyz, u_xlat0.xyz);
        u_xlat3.xy = max(u_xlat3.yz, u_xlat3.xx);
        u_xlat39 = max(u_xlat3.y, u_xlat3.x);
        u_xlat0.xy = min(u_xlat0.yz, u_xlat0.xx);
        u_xlat0.z = min(u_xlat0.y, u_xlat0.x);
        u_xlat0.x = max(u_xlat39, 0.0);
        u_xlat39 = u_xlat2.y * u_xlat0.x + u_xlat1.y;
        u_xlat10_40 = texture2D(_NoiseTex2D, vs_TEXCOORD3.xz).y;
        u_xlat16_40 = u_xlat10_40 + -0.5;
        u_xlat41 = sin(vs_TEXCOORD3.w);
        u_xlat41 = u_xlat41 * _Turbulence.y;
        u_xlat40 = u_xlat16_40 * _Turbulence.x + u_xlat41;
        u_xlat41 = _FoamTurbulence * _Size.y;
        u_xlat40 = u_xlat40 * u_xlat41;
        u_xlat41 = u_xlat40 * 0.0500000007 + _LevelPos;
        u_xlat40 = u_xlat40 * 0.0500000007 + _FoamMaxPos;
        u_xlat3.x = (-u_xlat1.y) + u_xlat41;
        u_xlat3.x = u_xlat3.x / u_xlat2.y;
        u_xlat3.x = max(u_xlat0.x, u_xlat3.x);
        u_xlat29 = (-u_xlat1.y) + u_xlat40;
        u_xlat29 = u_xlat29 / u_xlat2.y;
        u_xlat3.z = max(u_xlat0.x, u_xlat29);
        u_xlat3.xz = min(u_xlat0.zz, u_xlat3.xz);
        u_xlatb42 = u_xlat41<u_xlat39;
        if(u_xlatb42){
            u_xlatb4 = u_xlat2.y<0.0;
            u_xlat17.x = min(u_xlat0.z, u_xlat3.x);
            u_xlat4.x = (u_xlatb4) ? u_xlat17.x : u_xlat0.z;
            u_xlat4.x = (-u_xlat0.x) + u_xlat4.x;
            u_xlat17.x = float(_SmokeRaySteps);
            u_xlat4.x = u_xlat4.x / u_xlat17.x;
            u_xlat17.xyz = u_xlat2.xyz * u_xlat0.xxx + u_xlat1.xyz;
            u_xlat5.x = _Time.x * _Turbulence.x;
            u_xlat5.x = u_xlat5.x * _Size.y;
            u_xlat5.y = u_xlat5.x * _SmokeSpeed;
            u_xlat5.x = float(0.0);
            u_xlat5.z = float(0.0);
            u_xlat16_6.x = float(0.0);
            u_xlat16_6.y = float(0.0);
            u_xlat16_6.z = float(0.0);
            u_xlat16_6.w = float(0.0);
            u_xlat7.xyz = u_xlat17.xyz;
            u_xlati44 = _SmokeRaySteps;
            for(int u_xlati_while_true_0 = 0 ; u_xlati_while_true_0 < 0x7FFF ; u_xlati_while_true_0++){
                u_xlatb46 = 0>=u_xlati44;
                if(u_xlatb46){break;}
                u_xlat8.xyz = (-u_xlat5.xyz) + u_xlat7.xyz;
                u_xlat8.xyz = u_xlat8.xyz * _Scale.xxx;
                u_xlat10_46 = texture3DLodEXT(_NoiseTex, u_xlat8.xyz, 0.0).x;
                u_xlat16_8.w = u_xlat10_46 * _SmokeColor.w;
                u_xlat16_8.xyz = u_xlat16_8.www * _SmokeColor.xyz;
                u_xlat46 = u_xlat41 + (-u_xlat7.y);
                u_xlat46 = u_xlat46 / _Size.y;
                u_xlat46 = u_xlat46 * _SmokeAtten;
                u_xlat46 = u_xlat46 * 1.44269502;
                u_xlat46 = exp2(u_xlat46);
                u_xlat16_8 = vec4(u_xlat46) * u_xlat16_8;
                u_xlat16_9.x = (-u_xlat16_6.w) + 1.0;
                u_xlat16_6 = u_xlat16_8 * u_xlat16_9.xxxx + u_xlat16_6;
                u_xlati44 = u_xlati44 + -1;
                u_xlat7.xyz = u_xlat2.xyz * u_xlat4.xxx + u_xlat7.xyz;
            }
        } else {
            u_xlat16_6.x = float(0.0);
            u_xlat16_6.y = float(0.0);
            u_xlat16_6.z = float(0.0);
            u_xlat16_6.w = float(0.0);
        }
        u_xlat4.x = min(u_xlat0.z, u_xlat3.z);
        u_xlati29 = int((0.0<u_xlat2.y) ? -1 : 0);
        u_xlati30 = int((u_xlat2.y<0.0) ? -1 : 0);
        u_xlati29 = (-u_xlati29) + u_xlati30;
        u_xlat29 = float(u_xlati29);
        u_xlatb43 = u_xlat40<u_xlat39;
        u_xlat5.x = min(u_xlat0.z, u_xlat3.x);
        u_xlat4.y = (-u_xlat29) * u_xlat5.x;
        u_xlatb39 = u_xlat39<u_xlat41;
        u_xlat31 = u_xlat29 * _FoamBottom;
        u_xlat5.y = u_xlat4.x * u_xlat31;
        u_xlat0.y = (u_xlati30 != 0) ? u_xlat5.x : u_xlat4.x;
        u_xlat13.xz = (bool(u_xlatb39)) ? u_xlat5.xy : u_xlat0.xy;
        u_xlat13.xz = (bool(u_xlatb43)) ? u_xlat4.xy : u_xlat13.xz;
        u_xlatb4 = u_xlat13.x<u_xlat13.z;
        if(u_xlatb4){
            u_xlat39 = (-u_xlat13.x) + u_xlat13.z;
            u_xlat4.x = float(_FoamRaySteps);
            u_xlat39 = u_xlat39 / u_xlat4.x;
            u_xlat4.xyz = u_xlat2.xyz * u_xlat13.xxx + u_xlat1.xyz;
            u_xlat13.x = (-u_xlat41) + u_xlat4.y;
            u_xlat40 = (-u_xlat41) + u_xlat40;
            u_xlat5.xz = _Time.xx;
            u_xlat5.y = _Size.w;
            u_xlat5.xyz = u_xlat5.xyz * _Turbulence.xxx;
            u_xlat7.xz = _Size.ww;
            u_xlat7.y = _FoamTurbulence;
            u_xlat5.xyz = u_xlat5.xyz * u_xlat7.xyz;
            u_xlat7.xz = vec2(_FoamTurbulence);
            u_xlat7.y = 0.0;
            u_xlat16_8.x = float(0.0);
            u_xlat16_8.y = float(0.0);
            u_xlat16_8.z = float(0.0);
            u_xlat16_8.w = float(0.0);
            u_xlat10.xz = u_xlat4.xz;
            u_xlat10.y = u_xlat13.x;
            u_xlati17 = _FoamRaySteps;
            for(int u_xlati_while_true_1 = 0 ; u_xlati_while_true_1 < 0x7FFF ; u_xlati_while_true_1++){
                u_xlatb43 = 0>=u_xlati17;
                if(u_xlatb43){break;}
                u_xlat43 = u_xlat10.y / u_xlat40;
                u_xlat43 = clamp(u_xlat43, 0.0, 1.0);
                u_xlat11.xyz = (-u_xlat5.xyz) * u_xlat7.xyz + u_xlat10.xyz;
                u_xlat11.xyz = u_xlat11.xyz * _Scale.yyy;
                u_xlat10_44 = texture3DLodEXT(_NoiseTex, u_xlat11.xyz, 0.0).x;
                u_xlat44 = u_xlat10_44 + _FoamDensity;
                u_xlat44 = clamp(u_xlat44, 0.0, 1.0);
                u_xlatb46 = u_xlat43<u_xlat44;
                u_xlat43 = (-u_xlat43) + u_xlat44;
                u_xlat16_9.w = u_xlat43 * _FoamColor.w;
                u_xlat16_9.xyz = u_xlat16_9.www * _FoamColor.xyz;
                u_xlat43 = u_xlat10.y * _FoamWeight;
                u_xlat43 = u_xlat43 / u_xlat40;
                u_xlat43 = clamp(u_xlat43, 0.0, 1.0);
                u_xlat16_9 = vec4(u_xlat43) * u_xlat16_9;
                u_xlat16_12.x = (-u_xlat16_8.w) + 1.0;
                u_xlat16_9 = u_xlat16_9 * u_xlat16_12.xxxx + u_xlat16_8;
                u_xlat16_8 = (bool(u_xlatb46)) ? u_xlat16_9 : u_xlat16_8;
                u_xlati17 = u_xlati17 + -1;
                u_xlat10.xyz = u_xlat2.xyz * vec3(u_xlat39) + u_xlat10.xyz;
            }
            u_xlat13.x = _FoamDensity + 1.0;
            u_xlat4 = u_xlat13.xxxx * u_xlat16_8;
            u_xlat16_4 = u_xlat4;
        } else {
            u_xlat16_4.x = float(0.0);
            u_xlat16_4.y = float(0.0);
            u_xlat16_4.z = float(0.0);
            u_xlat16_4.w = float(0.0);
        }
        u_xlat3.y = u_xlat0.z * (-u_xlat29);
        u_xlat0.xy = (bool(u_xlatb42)) ? u_xlat3.xy : u_xlat0.xz;
        u_xlatb26 = u_xlat0.x<u_xlat0.y;
        if(u_xlatb26){
            u_xlat13.x = (-u_xlat0.x) + u_xlat0.y;
            u_xlat26 = float(_LiquidRaySteps);
            u_xlat13.x = u_xlat13.x / u_xlat26;
            u_xlat0.xzw = u_xlat2.xyz * u_xlat0.xxx + u_xlat1.xyz;
            u_xlat0.z = (-u_xlat41) + u_xlat0.z;
            u_xlat1.xz = _Time.xx * _Turbulence.yy;
            u_xlat40 = _Turbulence.x + _Turbulence.y;
            u_xlat1.y = _Time.x * 1.5;
            u_xlat1.xyz = vec3(u_xlat40) * u_xlat1.xyz;
            u_xlat41 = _Size.y;
            u_xlat40 = u_xlat40 * _Time.x;
            u_xlat40 = u_xlat40 * _Size.y;
            u_xlat3.y = u_xlat40 * 2.5;
            u_xlat40 = (-_Muddy) + 1.0;
            u_xlat3.x = float(0.0);
            u_xlat3.z = float(0.0);
            u_xlat16_5.x = float(0.0);
            u_xlat16_5.y = float(0.0);
            u_xlat16_5.z = float(0.0);
            u_xlat16_5.w = float(0.0);
            u_xlat7.xyz = u_xlat0.xzw;
            u_xlati46 = _LiquidRaySteps;
            for(int u_xlati_while_true_2 = 0 ; u_xlati_while_true_2 < 0x7FFF ; u_xlati_while_true_2++){
                u_xlatb10 = 0>=u_xlati46;
                if(u_xlatb10){break;}
                u_xlat10.x = u_xlat7.y / _Size.y;
                u_xlat10.x = u_xlat10.x * _DeepAtten;
                u_xlat10.x = u_xlat10.x * 1.44269502;
                u_xlat10.x = exp2(u_xlat10.x);
                u_xlat23.xyz = (-u_xlat1.xyz) * vec3(u_xlat41) + u_xlat7.xyz;
                u_xlat23.xyz = u_xlat23.xyz * _Scale.zzz;
                u_xlat10_23 = texture3DLodEXT(_NoiseTex, u_xlat23.xyz, 0.0).x;
                u_xlat23.x = u_xlat10_23 * _Muddy + u_xlat40;
                u_xlat16_8.w = u_xlat23.x * _Color1.w;
                u_xlat16_9.xyz = u_xlat16_8.www * _Color1.xyz;
                u_xlat16_8.xyz = u_xlat10.xxx * u_xlat16_9.xyz;
                u_xlat16_9.x = (-u_xlat16_5.w) + 1.0;
                u_xlat16_8 = u_xlat16_8 * u_xlat16_9.xxxx + u_xlat16_5;
                u_xlat23.xyz = (-u_xlat3.xyz) + u_xlat7.xyz;
                u_xlat23.xyz = u_xlat23.xyz * _Scale.www;
                u_xlat10_23 = texture3DLodEXT(_NoiseTex, u_xlat23.xyz, 0.0).x;
                u_xlat36 = u_xlat10_23 + (-_SparklingThreshold);
                u_xlat36 = max(u_xlat36, 0.0);
                u_xlat11.xyz = vec3(u_xlat36) * vec3(vec3(_SparklingIntensity, _SparklingIntensity, _SparklingIntensity)) + _Color2.xyz;
                u_xlat23.x = u_xlat10_23 * _Muddy + u_xlat40;
                u_xlat16_9.w = u_xlat23.x * _Color2.w;
                u_xlat16_12.xyz = u_xlat16_9.www * u_xlat11.xyz;
                u_xlat16_9.xyz = u_xlat10.xxx * u_xlat16_12.xyz;
                u_xlat16_12.x = (-u_xlat16_8.w) + 1.0;
                u_xlat16_5 = u_xlat16_9 * u_xlat16_12.xxxx + u_xlat16_8;
                u_xlati46 = u_xlati46 + -1;
                u_xlat7.xyz = u_xlat2.xyz * u_xlat13.xxx + u_xlat7.xyz;
            }
        } else {
            u_xlat16_5.x = float(0.0);
            u_xlat16_5.y = float(0.0);
            u_xlat16_5.z = float(0.0);
            u_xlat16_5.w = float(0.0);
        }
        u_xlat16_9.x = (-u_xlat16_6.w) + 1.0;
        u_xlat16_22 = (-u_xlat16_4.w) * u_xlat16_9.x + 1.0;
        u_xlat16_0 = u_xlat16_5 * vec4(u_xlat16_22);
        u_xlat16_1 = u_xlat16_4 * u_xlat16_9.xxxx + u_xlat16_6;
        u_xlat16_0 = u_xlat16_0 * u_xlat16_9.xxxx + u_xlat16_1;
        u_xlat16_6.x = (-u_xlat16_5.w) + 1.0;
        u_xlat16_1 = u_xlat16_4 * u_xlat16_6.xxxx + u_xlat16_5;
        u_xlat16_0 = (bool(u_xlatb42)) ? u_xlat16_0 : u_xlat16_1;
        u_xlat16_6.xyz = u_xlat16_0.xyz * _EmissionColor.xyz;
    } else {
        u_xlat16_0.x = float(0.0);
        u_xlat16_0.y = float(0.0);
        u_xlat16_0.z = float(0.0);
        u_xlat16_0.w = float(0.0);
        u_xlat16_6.x = float(0.0);
        u_xlat16_6.y = float(0.0);
        u_xlat16_6.z = float(0.0);
    }
    u_xlat16_1.xyz = u_xlat16_0.xyz * vs_TEXCOORD4.xyz;
    u_xlat16_0.xyz = u_xlat16_0.xyz * _LightColor0.xyz;
    u_xlat16_1.w = 0.0;
    u_xlat16_0 = u_xlat16_0 + u_xlat16_1;
    SV_Target0.xyz = u_xlat16_6.xyz + u_xlat16_0.xyz;
    SV_Target0.w = u_xlat16_0.w;
    return;
}

#endif
                                