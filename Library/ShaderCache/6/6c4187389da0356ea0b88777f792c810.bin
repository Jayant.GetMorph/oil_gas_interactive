<Q                         DIRECTIONAL    LIQUID_VOLUME_CYLINDER     LIQUID_VOLUME_DEPTH_AWARE_PASS     LIQUID_VOLUME_IGNORE_GRAVITY    .  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float3 _WorldSpaceCameraPos;
    half4 _WorldSpaceLightPos0;
    half4 _LightColor0;
    half4 _Color1;
    float _FoamBottom;
    float _LevelPos;
    float3 _FlaskThickness;
    float4 _Size;
    float3 _Center;
    float4 _Turbulence;
    float _DeepAtten;
    half4 _SmokeColor;
    float _SmokeAtten;
    int _SmokeRaySteps;
    int _LiquidRaySteps;
    half3 _GlossinessInt;
    float4 hlslcc_mtx4x4_Rot[4];
    float _UpperLimit;
    float _LowerLimit;
    half3 _EmissionColor;
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float4 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    half3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_NoiseTex2D [[ sampler (0) ]],
    sampler sampler_VLFrontBufferTexture [[ sampler (1) ]],
    texture2d<half, access::sample > _VLFrontBufferTexture [[ texture(0) ]] ,
    texture2d<half, access::sample > _NoiseTex2D [[ texture(1) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    half3 u_xlat16_0;
    float3 u_xlat1;
    float4 u_xlat2;
    half4 u_xlat16_2;
    int u_xlati2;
    bool u_xlatb2;
    float4 u_xlat3;
    half4 u_xlat16_3;
    bool u_xlatb3;
    float4 u_xlat4;
    half4 u_xlat16_4;
    bool u_xlatb4;
    half4 u_xlat16_5;
    half4 u_xlat16_6;
    float u_xlat7;
    half4 u_xlat16_7;
    bool u_xlatb7;
    half3 u_xlat16_8;
    float2 u_xlat11;
    half u_xlat16_11;
    int u_xlati12;
    float u_xlat13;
    int u_xlati13;
    bool u_xlatb13;
    half u_xlat16_14;
    float u_xlat20;
    float u_xlat27;
    float u_xlat28;
    bool u_xlatb28;
    float u_xlat29;
    int u_xlati29;
    bool u_xlatb29;
    int u_xlati30;
    half u_xlat16_32;
    half u_xlat16_35;
    u_xlat0.xyz = (-input.TEXCOORD1.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat27 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat27 = rsqrt(u_xlat27);
    u_xlat1.xyz = float3(u_xlat27) * u_xlat0.xyz;
    u_xlatb28 = FGlobals._UpperLimit<input.TEXCOORD4.y;
    u_xlatb2 = input.TEXCOORD4.y<FGlobals._LowerLimit;
    u_xlatb28 = u_xlatb28 || u_xlatb2;
    if(!u_xlatb28){
        u_xlat2.xyz = FGlobals._WorldSpaceCameraPos.xyzx.xyz + (-FGlobals._Center.xyzx.xyz);
        u_xlat3.xyz = u_xlat2.yyy * FGlobals.hlslcc_mtx4x4_Rot[1].xyz;
        u_xlat2.xyw = fma(FGlobals.hlslcc_mtx4x4_Rot[0].xyz, u_xlat2.xxx, u_xlat3.xyz);
        u_xlat2.xyz = fma(FGlobals.hlslcc_mtx4x4_Rot[2].xyz, u_xlat2.zzz, u_xlat2.xyw);
        u_xlat3.xyz = u_xlat2.xyz + FGlobals._Center.xyzx.xyz;
        u_xlat3.xzw = (-u_xlat3.xyz) + input.TEXCOORD3.xyz;
        u_xlat28 = dot(u_xlat3.xzw, u_xlat3.xzw);
        u_xlat28 = sqrt(u_xlat28);
        u_xlat3.xzw = u_xlat3.xzw / float3(u_xlat28);
        u_xlat28 = dot(u_xlat3.xw, u_xlat3.xw);
        u_xlat29 = dot(u_xlat3.xw, u_xlat2.xz);
        u_xlat2.x = dot(u_xlat2.xz, u_xlat2.xz);
        u_xlat2.x = fma((-FGlobals._Size.w), FGlobals._Size.w, u_xlat2.x);
        u_xlat2.x = u_xlat28 * u_xlat2.x;
        u_xlat2.x = fma(u_xlat29, u_xlat29, (-u_xlat2.x));
        u_xlat2.x = max(u_xlat2.x, 0.0);
        u_xlat2.x = sqrt(u_xlat2.x);
        u_xlat2.z = (-u_xlat2.x) + (-u_xlat29);
        u_xlat2.x = u_xlat2.x + (-u_xlat29);
        u_xlat2.xz = u_xlat2.xz / float2(u_xlat28);
        u_xlat29 = FGlobals._FlaskThickness.xyzx.y * FGlobals._Size.y;
        u_xlat4.x = u_xlat29 * 0.5;
        u_xlat29 = fma((-u_xlat29), 0.5, abs(u_xlat2.y));
        u_xlatb13 = 0.0<u_xlat29;
        u_xlat3.xw = u_xlat3.xw / u_xlat3.zz;
        u_xlat3.x = dot(u_xlat3.xw, u_xlat3.xw);
        u_xlat3.x = u_xlat3.x + 1.0;
        u_xlat3.x = sqrt(u_xlat3.x);
        u_xlat29 = u_xlat29 * u_xlat3.x;
        u_xlat29 = max(u_xlat29, u_xlat2.z);
        u_xlat20 = (u_xlatb13) ? u_xlat29 : u_xlat2.z;
        u_xlati29 = int((0.0<u_xlat3.z) ? 0xFFFFFFFFu : uint(0));
        u_xlati30 = int((u_xlat3.z<0.0) ? 0xFFFFFFFFu : uint(0));
        u_xlati13 = (-u_xlati29) + u_xlati30;
        u_xlat13 = float(u_xlati13);
        u_xlat11.x = fma(u_xlat13, (-u_xlat2.y), u_xlat4.x);
        u_xlatb4 = 0.0<u_xlat11.x;
        u_xlat11.x = u_xlat3.x * u_xlat11.x;
        u_xlat11.x = min(u_xlat11.x, u_xlat2.x);
        u_xlat2.x = (u_xlatb4) ? u_xlat11.x : u_xlat2.x;
        u_xlat4.x = max(u_xlat20, 0.0);
        u_xlat11.xy = input.TEXCOORD2.xy / input.TEXCOORD2.ww;
        u_xlat16_11 = _VLFrontBufferTexture.sample(sampler_VLFrontBufferTexture, u_xlat11.xy).x;
        u_xlat4.w = min(float(u_xlat16_11), u_xlat2.x);
        u_xlat2.x = fma(u_xlat3.z, u_xlat4.x, u_xlat3.y);
        u_xlat16_11 = _NoiseTex2D.sample(sampler_NoiseTex2D, input.TEXCOORD4.xz).y;
        u_xlat16_11 = u_xlat16_11 + half(-0.5);
        u_xlat20 = sin(input.TEXCOORD4.w);
        u_xlat20 = u_xlat20 * FGlobals._Turbulence.y;
        u_xlat11.x = fma(float(u_xlat16_11), FGlobals._Turbulence.x, u_xlat20);
        u_xlat11.x = u_xlat11.x * FGlobals._Size.y;
        u_xlat11.x = fma(u_xlat11.x, 0.0500000007, FGlobals._LevelPos);
        u_xlat28 = sqrt(u_xlat28);
        u_xlat3.y = u_xlat28 / u_xlat3.z;
        u_xlat28 = (-u_xlat11.x) + u_xlat2.x;
        u_xlat3.x = 1.0;
        u_xlat3.xy = float2(u_xlat28) * u_xlat3.xy;
        u_xlat28 = dot(u_xlat3.xy, u_xlat3.xy);
        u_xlat28 = sqrt(u_xlat28);
        u_xlat4.z = u_xlat28 + u_xlat4.x;
        u_xlatb28 = u_xlat11.x<u_xlat2.x;
        if(u_xlatb28){
            u_xlat20 = min(u_xlat4.w, u_xlat4.z);
            u_xlat20 = (u_xlati30 != 0) ? u_xlat20 : u_xlat4.w;
            u_xlat20 = (-u_xlat4.x) + u_xlat20;
            u_xlat3.x = float(FGlobals._SmokeRaySteps);
            u_xlat20 = u_xlat20 / u_xlat3.x;
            u_xlat16_5.xyz = FGlobals._SmokeColor.www * FGlobals._SmokeColor.xyz;
            u_xlat16_5.w = FGlobals._SmokeColor.w;
            u_xlat16_6.x = half(0.0);
            u_xlat16_6.y = half(0.0);
            u_xlat16_6.z = half(0.0);
            u_xlat16_6.w = half(0.0);
            u_xlat3.x = u_xlat2.x;
            u_xlati12 = 0x0;
            while(true){
                u_xlatb7 = u_xlati12>=FGlobals._SmokeRaySteps;
                if(u_xlatb7){break;}
                u_xlat7 = u_xlat11.x + (-u_xlat3.x);
                u_xlat7 = u_xlat7 / FGlobals._Size.y;
                u_xlat7 = u_xlat7 * FGlobals._SmokeAtten;
                u_xlat7 = u_xlat7 * 1.44269502;
                u_xlat7 = exp2(u_xlat7);
                u_xlat16_7 = half4(float4(u_xlat16_5) * float4(u_xlat7));
                u_xlat16_8.x = (-u_xlat16_6.w) + half(1.0);
                u_xlat16_6 = fma(u_xlat16_7, u_xlat16_8.xxxx, u_xlat16_6);
                u_xlat3.x = fma(u_xlat3.z, u_xlat20, u_xlat3.x);
                u_xlati12 = u_xlati12 + 0x1;
            }
        } else {
            u_xlat16_6.x = half(0.0);
            u_xlat16_6.y = half(0.0);
            u_xlat16_6.z = half(0.0);
            u_xlat16_6.w = half(0.0);
        }
        u_xlatb2 = u_xlat4.z<u_xlat4.w;
        u_xlat16_5.x = (u_xlatb2) ? half(0.100000001) : half(0.0);
        u_xlat4.y = -99999.0;
        u_xlat3.xy = (int(u_xlati30) != 0) ? u_xlat4.zw : u_xlat4.xy;
        u_xlat16_5.x = (u_xlati30 != 0) ? u_xlat16_5.x : half(0.0);
        u_xlati2 = u_xlatb2 ? u_xlati29 : int(0);
        u_xlat20 = FGlobals._FoamBottom * 0.100000001;
        u_xlat16_14 = (u_xlati2 != 0) ? half(u_xlat20) : half(0.0);
        u_xlat2.xz = (bool(u_xlatb28)) ? u_xlat3.xy : u_xlat4.xw;
        u_xlat16_4 = (bool(u_xlatb28)) ? u_xlat16_5.xxxx : half4(u_xlat16_14);
        u_xlatb29 = u_xlat2.x<u_xlat2.z;
        if(u_xlatb29){
            u_xlat20 = (-u_xlat2.x) + u_xlat2.z;
            u_xlat29 = float(FGlobals._LiquidRaySteps);
            u_xlat20 = u_xlat20 / u_xlat29;
            u_xlat2.x = fma(u_xlat3.z, u_xlat2.x, FGlobals._WorldSpaceCameraPos.xyzx.y);
            u_xlat2.x = (-u_xlat11.x) + u_xlat2.x;
            u_xlat16_5.w = FGlobals._Color1.w * FGlobals._Color1.w;
            u_xlat16_8.xyz = u_xlat16_5.www * FGlobals._Color1.xyz;
            u_xlat16_4.xyz = u_xlat16_4.www;
            u_xlat16_7.w = u_xlat16_4.w;
            u_xlat11.x = u_xlat2.x;
            u_xlati29 = 0x0;
            while(true){
                u_xlatb3 = u_xlati29>=FGlobals._LiquidRaySteps;
                if(u_xlatb3){break;}
                u_xlat3.x = u_xlat11.x / FGlobals._Size.y;
                u_xlat3.x = u_xlat3.x * FGlobals._DeepAtten;
                u_xlat3.x = u_xlat3.x * 1.44269502;
                u_xlat3.x = exp2(u_xlat3.x);
                u_xlat16_5.xyz = half3(u_xlat3.xxx * float3(u_xlat16_8.xyz));
                u_xlat16_35 = (-u_xlat16_7.w) + half(1.0);
                u_xlat16_7.xyz = u_xlat16_4.xyz;
                u_xlat16_7 = fma(u_xlat16_5, half4(u_xlat16_35), u_xlat16_7);
                u_xlat11.x = fma(u_xlat3.z, u_xlat20, u_xlat11.x);
                u_xlati29 = u_xlati29 + 0x1;
                u_xlat16_4.xyz = u_xlat16_7.xyz;
            }
            u_xlat16_4.w = u_xlat16_7.w;
        }
        u_xlat16_5.x = (-u_xlat16_6.w) + half(1.0);
        u_xlat16_5.x = clamp(u_xlat16_5.x, 0.0h, 1.0h);
        u_xlat16_2 = fma(u_xlat16_4, u_xlat16_5.xxxx, u_xlat16_6);
        u_xlat16_5.x = (-u_xlat16_4.w) + half(1.0);
        u_xlat16_5.x = clamp(u_xlat16_5.x, 0.0h, 1.0h);
        u_xlat16_3 = fma(u_xlat16_6, u_xlat16_5.xxxx, u_xlat16_4);
        u_xlat16_2 = (bool(u_xlatb28)) ? u_xlat16_2 : u_xlat16_3;
        u_xlat16_5.xyz = u_xlat16_2.xyz * FGlobals._EmissionColor.xyzx.xyz;
    } else {
        u_xlat16_5.x = half(0.0);
        u_xlat16_5.y = half(0.0);
        u_xlat16_5.z = half(0.0);
        u_xlat16_2.x = half(0.0);
        u_xlat16_2.y = half(0.0);
        u_xlat16_2.z = half(0.0);
        u_xlat16_2.w = half(0.0);
    }
    u_xlat16_6.xyz = half3(fma(u_xlat0.xyz, float3(u_xlat27), float3(FGlobals._WorldSpaceLightPos0.xyz)));
    u_xlat16_32 = dot(u_xlat16_6.xyz, u_xlat16_6.xyz);
    u_xlat16_32 = rsqrt(u_xlat16_32);
    u_xlat16_6.xyz = half3(u_xlat16_32) * u_xlat16_6.xyz;
    u_xlat16_32 = dot(input.TEXCOORD0.xyz, float3(FGlobals._WorldSpaceLightPos0.xyz));
    u_xlat16_32 = fma(u_xlat16_32, half(0.5), half(0.5));
    u_xlat16_32 = max(u_xlat16_32, half(0.0));
    u_xlat16_6.x = dot(input.TEXCOORD0.xyz, float3(u_xlat16_6.xyz));
    u_xlat16_6.x = max(u_xlat16_6.x, half(0.0));
    u_xlat16_0.x = log2(u_xlat16_6.x);
    u_xlat16_0.x = u_xlat16_0.x * FGlobals._GlossinessInt.xyzx.x;
    u_xlat16_0.x = exp2(u_xlat16_0.x);
    u_xlat16_6.x = dot(u_xlat1.xyz, (-float3(FGlobals._WorldSpaceLightPos0.xyz)));
    u_xlat16_6.x = max(u_xlat16_6.x, half(0.0));
    u_xlat16_6.x = log2(u_xlat16_6.x);
    u_xlat16_6.x = u_xlat16_6.x * FGlobals._GlossinessInt.xyzx.y;
    u_xlat16_6.x = exp2(u_xlat16_6.x);
    u_xlat16_32 = fma(u_xlat16_6.x, FGlobals._GlossinessInt.xyzx.z, u_xlat16_32);
    u_xlat16_6.xyz = u_xlat16_2.xyz * FGlobals._LightColor0.xyz;
    u_xlat16_0.xyz = u_xlat16_0.xxx * FGlobals._LightColor0.xyz;
    u_xlat16_0.xyz = fma(u_xlat16_6.xyz, half3(u_xlat16_32), u_xlat16_0.xyz);
    u_xlat16_6.xyz = fma(u_xlat16_2.xyz, input.TEXCOORD5.xyz, u_xlat16_0.xyz);
    output.SV_Target0.w = u_xlat16_2.w;
    output.SV_Target0.xyz = u_xlat16_5.xyz + u_xlat16_6.xyz;
    return output;
}
                                FGlobals�         _WorldSpaceCameraPos                         _WorldSpaceLightPos0                       _LightColor0                       _Color1                         _FoamBottom                   (   	   _LevelPos                     ,      _FlaskThickness                   0      _Size                     @      _Center                   P      _Turbulence                   `   
   _DeepAtten                    p      _SmokeColor                  x      _SmokeAtten                   �      _SmokeRaySteps                   �      _LiquidRaySteps                  �      _GlossinessInt                   �      _UpperLimit                   �      _LowerLimit                   �      _EmissionColor                   �      _Rot                 �             _VLFrontBufferTexture                    _NoiseTex2D                  FGlobals           