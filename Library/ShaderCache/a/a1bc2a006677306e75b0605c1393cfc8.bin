<Q                         DIRECTIONAL    LIQUID_VOLUME_IGNORE_GRAVITY   LIQUID_VOLUME_IRREGULAR     &  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

struct VGlobals_Type
{
    float4 _Time;
    float4 _ProjectionParams;
    half4 unity_SHAr;
    half4 unity_SHAg;
    half4 unity_SHAb;
    half4 unity_SHBr;
    half4 unity_SHBg;
    half4 unity_SHBb;
    half4 unity_SHC;
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    float4 hlslcc_mtx4x4unity_MatrixVP[4];
    float3 _FlaskThickness;
    float3 _Center;
    float4 _Turbulence;
    float4 hlslcc_mtx4x4_Rot[4];
    float _TurbulenceSpeed;
};

struct Mtl_VertexIn
{
    float4 POSITION0 [[ attribute(0) ]] ;
    float3 NORMAL0 [[ attribute(1) ]] ;
};

struct Mtl_VertexOut
{
    float4 mtl_Position [[ position ]];
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]];
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]];
    float4 TEXCOORD2 [[ user(TEXCOORD2) ]];
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]];
    float4 TEXCOORD4 [[ user(TEXCOORD4) ]];
    half3 TEXCOORD5 [[ user(TEXCOORD5) ]];
};

vertex Mtl_VertexOut xlatMtlMain(
    constant VGlobals_Type& VGlobals [[ buffer(0) ]],
    Mtl_VertexIn input [[ stage_in ]])
{
    Mtl_VertexOut output;
    float4 u_xlat0;
    half4 u_xlat16_0;
    float4 u_xlat1;
    float4 u_xlat2;
    float4 u_xlat3;
    half3 u_xlat16_4;
    half3 u_xlat16_5;
    float u_xlat18;
    u_xlat0.xyz = input.POSITION0.xyz * VGlobals._FlaskThickness.xyzx.xyz;
    u_xlat1 = u_xlat0.yyyy * VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat1 = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[0], u_xlat0.xxxx, u_xlat1);
    u_xlat1 = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[2], u_xlat0.zzzz, u_xlat1);
    u_xlat1 = u_xlat1 + VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat2 = u_xlat1.yyyy * VGlobals.hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[0], u_xlat1.xxxx, u_xlat2);
    u_xlat2 = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[2], u_xlat1.zzzz, u_xlat2);
    u_xlat1 = fma(VGlobals.hlslcc_mtx4x4unity_MatrixVP[3], u_xlat1.wwww, u_xlat2);
    output.mtl_Position = u_xlat1;
    u_xlat2.x = dot(input.NORMAL0.xyz, VGlobals.hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat2.y = dot(input.NORMAL0.xyz, VGlobals.hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat2.z = dot(input.NORMAL0.xyz, VGlobals.hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat18 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat18 = rsqrt(u_xlat18);
    u_xlat2.xyz = float3(u_xlat18) * u_xlat2.xyz;
    output.TEXCOORD0.xyz = u_xlat2.xyz;
    u_xlat3.xyz = u_xlat0.yyy * VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyw = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, u_xlat0.xxx, u_xlat3.xyz);
    u_xlat0.xyz = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[2].xyz, u_xlat0.zzz, u_xlat0.xyw);
    u_xlat0.xyz = fma(VGlobals.hlslcc_mtx4x4unity_ObjectToWorld[3].xyz, input.POSITION0.www, u_xlat0.xyz);
    output.TEXCOORD1.xyz = u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + (-VGlobals._Center.xyzx.xyz);
    u_xlat18 = u_xlat1.y * VGlobals._ProjectionParams.x;
    u_xlat3.w = u_xlat18 * 0.5;
    u_xlat3.xz = u_xlat1.xw * float2(0.5, 0.5);
    output.TEXCOORD2.zw = u_xlat1.zw;
    output.TEXCOORD2.xy = u_xlat3.zz + u_xlat3.xw;
    u_xlat1.xyz = u_xlat0.yyy * VGlobals.hlslcc_mtx4x4_Rot[1].xyz;
    u_xlat0.xyw = fma(VGlobals.hlslcc_mtx4x4_Rot[0].xyz, u_xlat0.xxx, u_xlat1.xyz);
    u_xlat0.xyz = fma(VGlobals.hlslcc_mtx4x4_Rot[2].xyz, u_xlat0.zzz, u_xlat0.xyw);
    output.TEXCOORD3.xyz = u_xlat0.xyz + VGlobals._Center.xyzx.xyz;
    u_xlat0.x = dot(input.POSITION0.xz, VGlobals._Turbulence.zw);
    output.TEXCOORD4.w = u_xlat0.x + VGlobals._TurbulenceSpeed;
    u_xlat0.x = VGlobals._Turbulence.x * 0.100000001;
    output.TEXCOORD4.xz = fma(input.POSITION0.xz, u_xlat0.xx, VGlobals._Time.xx);
    output.TEXCOORD4.y = input.POSITION0.y;
    u_xlat16_4.x = half(u_xlat2.y * u_xlat2.y);
    u_xlat16_4.x = half(fma(u_xlat2.x, u_xlat2.x, (-float(u_xlat16_4.x))));
    u_xlat16_0 = half4(u_xlat2.yzzx * u_xlat2.xyzz);
    u_xlat16_5.x = dot(VGlobals.unity_SHBr, u_xlat16_0);
    u_xlat16_5.y = dot(VGlobals.unity_SHBg, u_xlat16_0);
    u_xlat16_5.z = dot(VGlobals.unity_SHBb, u_xlat16_0);
    u_xlat16_4.xyz = fma(VGlobals.unity_SHC.xyz, u_xlat16_4.xxx, u_xlat16_5.xyz);
    u_xlat2.w = 1.0;
    u_xlat16_5.x = half(dot(float4(VGlobals.unity_SHAr), u_xlat2));
    u_xlat16_5.y = half(dot(float4(VGlobals.unity_SHAg), u_xlat2));
    u_xlat16_5.z = half(dot(float4(VGlobals.unity_SHAb), u_xlat2));
    u_xlat16_4.xyz = u_xlat16_4.xyz + u_xlat16_5.xyz;
    u_xlat16_4.xyz = max(u_xlat16_4.xyz, half3(0.0, 0.0, 0.0));
    u_xlat1.xyz = log2(float3(u_xlat16_4.xyz));
    u_xlat1.xyz = u_xlat1.xyz * float3(0.416666657, 0.416666657, 0.416666657);
    u_xlat1.xyz = exp2(u_xlat1.xyz);
    u_xlat1.xyz = fma(u_xlat1.xyz, float3(1.05499995, 1.05499995, 1.05499995), float3(-0.0549999997, -0.0549999997, -0.0549999997));
    u_xlat1.xyz = max(u_xlat1.xyz, float3(0.0, 0.0, 0.0));
    output.TEXCOORD5.xyz = half3(u_xlat1.xyz);
    return output;
}
                                           VGlobals�        _Time                            _ProjectionParams                        
   unity_SHAr                       
   unity_SHAg                   (   
   unity_SHAb                   0   
   unity_SHBr                   8   
   unity_SHBg                   @   
   unity_SHBb                   H   	   unity_SHC                    P      _FlaskThickness                         _Center                   0     _Turbulence                   @     _TurbulenceSpeed                  �     unity_ObjectToWorld                  `      unity_WorldToObject                  �      unity_MatrixVP                   �      _Rot                 P            VGlobals           