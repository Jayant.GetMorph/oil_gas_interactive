<Q                         DIRECTIONAL    LIQUID_VOLUME_IGNORE_GRAVITY   LIQUID_VOLUME_SPHERE    `L  #ifdef VERTEX
#version 100

uniform 	vec4 _Time;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec3 _FlaskThickness;
uniform 	vec3 _Center;
uniform 	vec4 _Turbulence;
uniform 	vec4 hlslcc_mtx4x4_Rot[4];
uniform 	float _TurbulenceSpeed;
attribute highp vec4 in_POSITION0;
attribute highp vec3 in_NORMAL0;
varying highp vec3 vs_TEXCOORD0;
varying highp vec3 vs_TEXCOORD1;
varying highp vec3 vs_TEXCOORD2;
varying highp vec4 vs_TEXCOORD3;
varying mediump vec3 vs_TEXCOORD4;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
vec4 u_xlat2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
float u_xlat15;
void main()
{
    u_xlat0.xyz = in_POSITION0.xyz * _FlaskThickness.xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat15 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * u_xlat1.xyz;
    vs_TEXCOORD0.xyz = u_xlat1.xyz;
    u_xlat2.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat0.xxx + u_xlat2.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + (-_Center.xyz);
    u_xlat2.xyz = u_xlat0.yyy * hlslcc_mtx4x4_Rot[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4_Rot[0].xyz * u_xlat0.xxx + u_xlat2.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4_Rot[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    vs_TEXCOORD2.xyz = u_xlat0.xyz + _Center.xyz;
    u_xlat0.x = dot(in_POSITION0.xz, _Turbulence.zw);
    vs_TEXCOORD3.w = u_xlat0.x + _TurbulenceSpeed;
    u_xlat0.x = _Turbulence.x * 0.100000001;
    vs_TEXCOORD3.xz = in_POSITION0.xz * u_xlat0.xx + _Time.xx;
    vs_TEXCOORD3.y = in_POSITION0.y;
    u_xlat16_3.x = u_xlat1.y * u_xlat1.y;
    u_xlat16_3.x = u_xlat1.x * u_xlat1.x + (-u_xlat16_3.x);
    u_xlat16_0 = u_xlat1.yzzx * u_xlat1.xyzz;
    u_xlat16_4.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_4.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_4.z = dot(unity_SHBb, u_xlat16_0);
    u_xlat16_3.xyz = unity_SHC.xyz * u_xlat16_3.xxx + u_xlat16_4.xyz;
    u_xlat1.w = 1.0;
    u_xlat16_4.x = dot(unity_SHAr, u_xlat1);
    u_xlat16_4.y = dot(unity_SHAg, u_xlat1);
    u_xlat16_4.z = dot(unity_SHAb, u_xlat1);
    u_xlat16_3.xyz = u_xlat16_3.xyz + u_xlat16_4.xyz;
    u_xlat16_3.xyz = max(u_xlat16_3.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat1.xyz = log2(u_xlat16_3.xyz);
    u_xlat1.xyz = u_xlat1.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat1.xyz = exp2(u_xlat1.xyz);
    u_xlat1.xyz = u_xlat1.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat1.xyz = max(u_xlat1.xyz, vec3(0.0, 0.0, 0.0));
    vs_TEXCOORD4.xyz = u_xlat1.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 100
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif
#if !defined(GL_EXT_shader_texture_lod)
#define texture1DLodEXT texture1D
#define texture2DLodEXT texture2D
#define texture2DProjLodEXT texture2DProj
#define texture3DLodEXT texture3D
#define textureCubeLodEXT textureCube
#endif

#ifdef GL_FRAGMENT_PRECISION_HIGH
    precision highp float;
#else
    precision mediump float;
#endif
precision highp int;
uniform 	vec4 _Time;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump vec4 _Color1;
uniform 	mediump vec4 _Color2;
uniform 	mediump vec4 _FoamColor;
uniform 	float _FoamMaxPos;
uniform 	int _FoamRaySteps;
uniform 	float _FoamDensity;
uniform 	float _FoamBottom;
uniform 	float _FoamTurbulence;
uniform 	float _LevelPos;
uniform 	vec4 _Size;
uniform 	vec3 _Center;
uniform 	float _Muddy;
uniform 	vec4 _Turbulence;
uniform 	float _DeepAtten;
uniform 	mediump vec4 _SmokeColor;
uniform 	float _SmokeAtten;
uniform 	int _SmokeRaySteps;
uniform 	float _SmokeSpeed;
uniform 	int _LiquidRaySteps;
uniform 	mediump vec3 _GlossinessInt;
uniform 	float _FoamWeight;
uniform 	vec4 _Scale;
uniform 	vec4 hlslcc_mtx4x4_Rot[4];
uniform 	float _UpperLimit;
uniform 	float _LowerLimit;
uniform 	mediump vec3 _EmissionColor;
uniform 	float _SparklingIntensity;
uniform 	float _SparklingThreshold;
uniform lowp sampler2D _NoiseTex2D;
uniform lowp sampler3D _NoiseTex;
varying highp vec3 vs_TEXCOORD0;
varying highp vec3 vs_TEXCOORD1;
varying highp vec3 vs_TEXCOORD2;
varying highp vec4 vs_TEXCOORD3;
varying mediump vec3 vs_TEXCOORD4;
#define SV_Target0 gl_FragData[0]
vec3 u_xlat0;
mediump vec3 u_xlat16_0;
vec3 u_xlat1;
vec4 u_xlat2;
mediump vec4 u_xlat16_2;
lowp float u_xlat10_2;
bool u_xlatb2;
vec3 u_xlat3;
mediump vec4 u_xlat16_3;
vec3 u_xlat4;
mediump vec4 u_xlat16_4;
vec3 u_xlat5;
vec3 u_xlat6;
vec4 u_xlat7;
mediump vec4 u_xlat16_7;
vec3 u_xlat8;
mediump vec4 u_xlat16_8;
mediump vec4 u_xlat16_9;
vec3 u_xlat10;
vec3 u_xlat11;
mediump vec4 u_xlat16_11;
mediump vec4 u_xlat16_12;
vec3 u_xlat13;
vec3 u_xlat14;
mediump vec3 u_xlat16_15;
float u_xlat18;
int u_xlati19;
vec3 u_xlat21;
bool u_xlatb21;
mediump float u_xlat16_28;
float u_xlat34;
int u_xlati34;
bool u_xlatb38;
float u_xlat48;
float u_xlat49;
bool u_xlatb49;
bool u_xlatb50;
float u_xlat51;
int u_xlati51;
bool u_xlatb51;
float u_xlat52;
lowp float u_xlat10_52;
int u_xlati52;
bool u_xlatb52;
float u_xlat53;
lowp float u_xlat10_53;
bool u_xlatb53;
mediump float u_xlat16_57;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD1.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat48 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat48 = inversesqrt(u_xlat48);
    u_xlat1.xyz = vec3(u_xlat48) * u_xlat0.xyz;
    u_xlatb49 = _UpperLimit<vs_TEXCOORD3.y;
    u_xlatb2 = vs_TEXCOORD3.y<_LowerLimit;
    u_xlatb49 = u_xlatb49 || u_xlatb2;
    if(!u_xlatb49){
        u_xlat2.xyz = _WorldSpaceCameraPos.xyz + (-_Center.xyz);
        u_xlat3.xyz = u_xlat2.yyy * hlslcc_mtx4x4_Rot[1].xyz;
        u_xlat2.xyw = hlslcc_mtx4x4_Rot[0].xyz * u_xlat2.xxx + u_xlat3.xyz;
        u_xlat2.xyz = hlslcc_mtx4x4_Rot[2].xyz * u_xlat2.zzz + u_xlat2.xyw;
        u_xlat3.xyz = u_xlat2.xyz + _Center.xyz;
        u_xlat4.xyz = (-u_xlat3.xyz) + vs_TEXCOORD2.xyz;
        u_xlat49 = dot(u_xlat4.xyz, u_xlat4.xyz);
        u_xlat49 = sqrt(u_xlat49);
        u_xlat4.xyz = u_xlat4.xyz / vec3(u_xlat49);
        u_xlat49 = dot(u_xlat4.xyz, u_xlat2.xyz);
        u_xlat2.x = dot(u_xlat2.xyz, u_xlat2.xyz);
        u_xlat2.x = (-_Size.w) * _Size.w + u_xlat2.x;
        u_xlat2.x = u_xlat49 * u_xlat49 + (-u_xlat2.x);
        u_xlat2.x = sqrt(u_xlat2.x);
        u_xlat18 = (-u_xlat49) + (-u_xlat2.x);
        u_xlat5.z = (-u_xlat49) + u_xlat2.x;
        u_xlat5.x = max(u_xlat18, 0.0);
        u_xlat49 = u_xlat4.y * u_xlat5.x + u_xlat3.y;
        u_xlat10_2 = texture2D(_NoiseTex2D, vs_TEXCOORD3.xz).y;
        u_xlat16_2.x = u_xlat10_2 + -0.5;
        u_xlat18 = sin(vs_TEXCOORD3.w);
        u_xlat18 = u_xlat18 * _Turbulence.y;
        u_xlat2.x = u_xlat16_2.x * _Turbulence.x + u_xlat18;
        u_xlat18 = _FoamTurbulence * _Size.y;
        u_xlat2.x = u_xlat2.x * u_xlat18;
        u_xlat18 = u_xlat2.x * 0.0500000007 + _LevelPos;
        u_xlat2.x = u_xlat2.x * 0.0500000007 + _FoamMaxPos;
        u_xlat34 = (-u_xlat3.y) + u_xlat18;
        u_xlat34 = u_xlat34 / u_xlat4.y;
        u_xlat34 = max(u_xlat5.x, u_xlat34);
        u_xlat6.x = min(u_xlat5.z, u_xlat34);
        u_xlat34 = (-u_xlat3.y) + u_xlat2.x;
        u_xlat34 = u_xlat34 / u_xlat4.y;
        u_xlat34 = max(u_xlat5.x, u_xlat34);
        u_xlat34 = min(u_xlat5.z, u_xlat34);
        u_xlatb50 = u_xlat18<u_xlat49;
        if(u_xlatb50){
            u_xlatb51 = u_xlat4.y<0.0;
            u_xlat52 = min(u_xlat5.z, u_xlat6.x);
            u_xlat51 = (u_xlatb51) ? u_xlat52 : u_xlat5.z;
            u_xlat51 = (-u_xlat5.x) + u_xlat51;
            u_xlat52 = float(_SmokeRaySteps);
            u_xlat51 = u_xlat51 / u_xlat52;
            u_xlat7.xyz = u_xlat4.xyz * u_xlat5.xxx + u_xlat3.xyz;
            u_xlat52 = _Time.x * _Turbulence.x;
            u_xlat52 = u_xlat52 * _Size.y;
            u_xlat8.y = u_xlat52 * _SmokeSpeed;
            u_xlat8.x = float(0.0);
            u_xlat8.z = float(0.0);
            u_xlat16_9.x = float(0.0);
            u_xlat16_9.y = float(0.0);
            u_xlat16_9.z = float(0.0);
            u_xlat16_9.w = float(0.0);
            u_xlat10.xyz = u_xlat7.xyz;
            u_xlati52 = _SmokeRaySteps;
            for(int u_xlati_while_true_0 = 0 ; u_xlati_while_true_0 < 0x7FFF ; u_xlati_while_true_0++){
                u_xlatb53 = 0>=u_xlati52;
                if(u_xlatb53){break;}
                u_xlat11.xyz = (-u_xlat8.xyz) + u_xlat10.xyz;
                u_xlat11.xyz = u_xlat11.xyz * _Scale.xxx;
                u_xlat10_53 = texture3DLodEXT(_NoiseTex, u_xlat11.xyz, 0.0).x;
                u_xlat16_11.w = u_xlat10_53 * _SmokeColor.w;
                u_xlat16_11.xyz = u_xlat16_11.www * _SmokeColor.xyz;
                u_xlat53 = u_xlat18 + (-u_xlat10.y);
                u_xlat53 = u_xlat53 / _Size.y;
                u_xlat53 = u_xlat53 * _SmokeAtten;
                u_xlat53 = u_xlat53 * 1.44269502;
                u_xlat53 = exp2(u_xlat53);
                u_xlat16_11 = vec4(u_xlat53) * u_xlat16_11;
                u_xlat16_12.x = (-u_xlat16_9.w) + 1.0;
                u_xlat16_9 = u_xlat16_11 * u_xlat16_12.xxxx + u_xlat16_9;
                u_xlati52 = u_xlati52 + -1;
                u_xlat10.xyz = u_xlat4.xyz * vec3(u_xlat51) + u_xlat10.xyz;
            }
        } else {
            u_xlat16_9.x = float(0.0);
            u_xlat16_9.y = float(0.0);
            u_xlat16_9.z = float(0.0);
            u_xlat16_9.w = float(0.0);
        }
        u_xlat7.x = min(u_xlat5.z, u_xlat34);
        u_xlati34 = int((0.0<u_xlat4.y) ? -1 : 0);
        u_xlati51 = int((u_xlat4.y<0.0) ? -1 : 0);
        u_xlati34 = (-u_xlati34) + u_xlati51;
        u_xlat34 = float(u_xlati34);
        u_xlatb52 = u_xlat2.x<u_xlat49;
        u_xlat8.x = min(u_xlat5.z, u_xlat6.x);
        u_xlat7.y = (-u_xlat34) * u_xlat8.x;
        u_xlatb49 = u_xlat49<u_xlat18;
        u_xlat53 = u_xlat34 * _FoamBottom;
        u_xlat8.y = u_xlat53 * u_xlat7.x;
        u_xlat5.y = (u_xlati51 != 0) ? u_xlat8.x : u_xlat7.x;
        u_xlat21.xz = (bool(u_xlatb49)) ? u_xlat8.xy : u_xlat5.xy;
        u_xlat21.xz = (bool(u_xlatb52)) ? u_xlat7.xy : u_xlat21.xz;
        u_xlatb49 = u_xlat21.x<u_xlat21.z;
        if(u_xlatb49){
            u_xlat49 = (-u_xlat21.x) + u_xlat21.z;
            u_xlat51 = float(_FoamRaySteps);
            u_xlat49 = u_xlat49 / u_xlat51;
            u_xlat7.xyz = u_xlat4.xyz * u_xlat21.xxx + u_xlat3.xyz;
            u_xlat51 = (-u_xlat18) + u_xlat7.y;
            u_xlat2.x = (-u_xlat18) + u_xlat2.x;
            u_xlat8.xz = _Time.xx;
            u_xlat8.y = _Size.w;
            u_xlat8.xyz = u_xlat8.xyz * _Turbulence.xxx;
            u_xlat10.xz = _Size.ww;
            u_xlat10.y = _FoamTurbulence;
            u_xlat8.xyz = u_xlat8.xyz * u_xlat10.xyz;
            u_xlat10.xz = vec2(_FoamTurbulence);
            u_xlat10.y = 0.0;
            u_xlat16_11.x = float(0.0);
            u_xlat16_11.y = float(0.0);
            u_xlat16_11.z = float(0.0);
            u_xlat16_11.w = float(0.0);
            u_xlat13.xz = u_xlat7.xz;
            u_xlat13.y = u_xlat51;
            u_xlati52 = _FoamRaySteps;
            for(int u_xlati_while_true_1 = 0 ; u_xlati_while_true_1 < 0x7FFF ; u_xlati_while_true_1++){
                u_xlatb21 = 0>=u_xlati52;
                if(u_xlatb21){break;}
                u_xlat21.x = u_xlat13.y / u_xlat2.x;
                u_xlat21.x = clamp(u_xlat21.x, 0.0, 1.0);
                u_xlat14.xyz = (-u_xlat8.xyz) * u_xlat10.xyz + u_xlat13.xyz;
                u_xlat14.xyz = u_xlat14.xyz * _Scale.yyy;
                u_xlat10_53 = texture3DLodEXT(_NoiseTex, u_xlat14.xyz, 0.0).x;
                u_xlat53 = u_xlat10_53 + _FoamDensity;
                u_xlat53 = clamp(u_xlat53, 0.0, 1.0);
                u_xlatb38 = u_xlat21.x<u_xlat53;
                u_xlat21.x = (-u_xlat21.x) + u_xlat53;
                u_xlat16_12.w = u_xlat21.x * _FoamColor.w;
                u_xlat16_12.xyz = u_xlat16_12.www * _FoamColor.xyz;
                u_xlat21.x = u_xlat13.y * _FoamWeight;
                u_xlat21.x = u_xlat21.x / u_xlat2.x;
                u_xlat21.x = clamp(u_xlat21.x, 0.0, 1.0);
                u_xlat16_12 = u_xlat21.xxxx * u_xlat16_12;
                u_xlat16_15.x = (-u_xlat16_11.w) + 1.0;
                u_xlat16_12 = u_xlat16_12 * u_xlat16_15.xxxx + u_xlat16_11;
                u_xlat16_11 = (bool(u_xlatb38)) ? u_xlat16_12 : u_xlat16_11;
                u_xlati52 = u_xlati52 + -1;
                u_xlat13.xyz = u_xlat4.xyz * vec3(u_xlat49) + u_xlat13.xyz;
            }
            u_xlat49 = _FoamDensity + 1.0;
            u_xlat7 = vec4(u_xlat49) * u_xlat16_11;
            u_xlat16_7 = u_xlat7;
        } else {
            u_xlat16_7.x = float(0.0);
            u_xlat16_7.y = float(0.0);
            u_xlat16_7.z = float(0.0);
            u_xlat16_7.w = float(0.0);
        }
        u_xlat6.y = (-u_xlat34) * u_xlat5.z;
        u_xlat2.xz = (bool(u_xlatb50)) ? u_xlat6.xy : u_xlat5.xz;
        u_xlatb49 = u_xlat2.x<u_xlat2.z;
        if(u_xlatb49){
            u_xlat49 = (-u_xlat2.x) + u_xlat2.z;
            u_xlat34 = float(_LiquidRaySteps);
            u_xlat49 = u_xlat49 / u_xlat34;
            u_xlat3.xyz = u_xlat4.xyz * u_xlat2.xxx + u_xlat3.xyz;
            u_xlat2.x = (-u_xlat18) + u_xlat3.y;
            u_xlat5.xz = _Time.xx * _Turbulence.yy;
            u_xlat18 = _Turbulence.x + _Turbulence.y;
            u_xlat5.y = _Time.x * 1.5;
            u_xlat5.xyz = vec3(u_xlat18) * u_xlat5.xyz;
            u_xlat34 = _Size.y;
            u_xlat18 = u_xlat18 * _Time.x;
            u_xlat18 = u_xlat18 * _Size.y;
            u_xlat6.y = u_xlat18 * 2.5;
            u_xlat18 = (-_Muddy) + 1.0;
            u_xlat6.x = float(0.0);
            u_xlat6.z = float(0.0);
            u_xlat16_8.x = float(0.0);
            u_xlat16_8.y = float(0.0);
            u_xlat16_8.z = float(0.0);
            u_xlat16_8.w = float(0.0);
            u_xlat10.xz = u_xlat3.xz;
            u_xlat10.y = u_xlat2.x;
            u_xlati19 = _LiquidRaySteps;
            for(int u_xlati_while_true_2 = 0 ; u_xlati_while_true_2 < 0x7FFF ; u_xlati_while_true_2++){
                u_xlatb51 = 0>=u_xlati19;
                if(u_xlatb51){break;}
                u_xlat51 = u_xlat10.y / _Size.y;
                u_xlat51 = u_xlat51 * _DeepAtten;
                u_xlat51 = u_xlat51 * 1.44269502;
                u_xlat51 = exp2(u_xlat51);
                u_xlat13.xyz = (-u_xlat5.xyz) * vec3(u_xlat34) + u_xlat10.xyz;
                u_xlat13.xyz = u_xlat13.xyz * _Scale.zzz;
                u_xlat10_52 = texture3DLodEXT(_NoiseTex, u_xlat13.xyz, 0.0).x;
                u_xlat52 = u_xlat10_52 * _Muddy + u_xlat18;
                u_xlat16_11.w = u_xlat52 * _Color1.w;
                u_xlat16_12.xyz = u_xlat16_11.www * _Color1.xyz;
                u_xlat16_11.xyz = vec3(u_xlat51) * u_xlat16_12.xyz;
                u_xlat16_12.x = (-u_xlat16_8.w) + 1.0;
                u_xlat16_11 = u_xlat16_11 * u_xlat16_12.xxxx + u_xlat16_8;
                u_xlat13.xyz = (-u_xlat6.xyz) + u_xlat10.xyz;
                u_xlat13.xyz = u_xlat13.xyz * _Scale.www;
                u_xlat10_52 = texture3DLodEXT(_NoiseTex, u_xlat13.xyz, 0.0).x;
                u_xlat53 = u_xlat10_52 + (-_SparklingThreshold);
                u_xlat53 = max(u_xlat53, 0.0);
                u_xlat13.xyz = vec3(u_xlat53) * vec3(vec3(_SparklingIntensity, _SparklingIntensity, _SparklingIntensity)) + _Color2.xyz;
                u_xlat52 = u_xlat10_52 * _Muddy + u_xlat18;
                u_xlat16_12.w = u_xlat52 * _Color2.w;
                u_xlat16_15.xyz = u_xlat16_12.www * u_xlat13.xyz;
                u_xlat16_12.xyz = vec3(u_xlat51) * u_xlat16_15.xyz;
                u_xlat16_15.x = (-u_xlat16_11.w) + 1.0;
                u_xlat16_8 = u_xlat16_12 * u_xlat16_15.xxxx + u_xlat16_11;
                u_xlati19 = u_xlati19 + -1;
                u_xlat10.xyz = u_xlat4.xyz * vec3(u_xlat49) + u_xlat10.xyz;
            }
        } else {
            u_xlat16_8.x = float(0.0);
            u_xlat16_8.y = float(0.0);
            u_xlat16_8.z = float(0.0);
            u_xlat16_8.w = float(0.0);
        }
        u_xlat16_12.x = (-u_xlat16_9.w) + 1.0;
        u_xlat16_28 = (-u_xlat16_7.w) * u_xlat16_12.x + 1.0;
        u_xlat16_3 = u_xlat16_8 * vec4(u_xlat16_28);
        u_xlat16_4 = u_xlat16_7 * u_xlat16_12.xxxx + u_xlat16_9;
        u_xlat16_3 = u_xlat16_3 * u_xlat16_12.xxxx + u_xlat16_4;
        u_xlat16_9.x = (-u_xlat16_8.w) + 1.0;
        u_xlat16_4 = u_xlat16_7 * u_xlat16_9.xxxx + u_xlat16_8;
        u_xlat16_2 = (bool(u_xlatb50)) ? u_xlat16_3 : u_xlat16_4;
        u_xlat16_9.xyz = u_xlat16_2.xyz * _EmissionColor.xyz;
    } else {
        u_xlat16_9.x = float(0.0);
        u_xlat16_9.y = float(0.0);
        u_xlat16_9.z = float(0.0);
        u_xlat16_2.x = float(0.0);
        u_xlat16_2.y = float(0.0);
        u_xlat16_2.z = float(0.0);
        u_xlat16_2.w = float(0.0);
    }
    u_xlat16_12.xyz = u_xlat0.xyz * vec3(u_xlat48) + _WorldSpaceLightPos0.xyz;
    u_xlat16_57 = dot(u_xlat16_12.xyz, u_xlat16_12.xyz);
    u_xlat16_57 = inversesqrt(u_xlat16_57);
    u_xlat16_12.xyz = vec3(u_xlat16_57) * u_xlat16_12.xyz;
    u_xlat16_57 = dot(vs_TEXCOORD0.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat16_57 = u_xlat16_57 * 0.5 + 0.5;
    u_xlat16_57 = max(u_xlat16_57, 0.0);
    u_xlat16_12.x = dot(vs_TEXCOORD0.xyz, u_xlat16_12.xyz);
    u_xlat16_12.x = max(u_xlat16_12.x, 0.0);
    u_xlat16_0.x = log2(u_xlat16_12.x);
    u_xlat16_0.x = u_xlat16_0.x * _GlossinessInt.x;
    u_xlat16_0.x = exp2(u_xlat16_0.x);
    u_xlat16_12.x = dot(u_xlat1.xyz, (-_WorldSpaceLightPos0.xyz));
    u_xlat16_12.x = max(u_xlat16_12.x, 0.0);
    u_xlat16_12.x = log2(u_xlat16_12.x);
    u_xlat16_12.x = u_xlat16_12.x * _GlossinessInt.y;
    u_xlat16_12.x = exp2(u_xlat16_12.x);
    u_xlat16_57 = u_xlat16_12.x * _GlossinessInt.z + u_xlat16_57;
    u_xlat16_12.xyz = u_xlat16_2.xyz * _LightColor0.xyz;
    u_xlat16_0.xyz = u_xlat16_0.xxx * _LightColor0.xyz;
    u_xlat16_0.xyz = u_xlat16_12.xyz * vec3(u_xlat16_57) + u_xlat16_0.xyz;
    u_xlat16_12.xyz = u_xlat16_2.xyz * vs_TEXCOORD4.xyz + u_xlat16_0.xyz;
    SV_Target0.w = u_xlat16_2.w;
    SV_Target0.xyz = u_xlat16_9.xyz + u_xlat16_12.xyz;
    return;
}

#endif
                              